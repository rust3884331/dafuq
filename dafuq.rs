
use std::io;
use std::process::Command;

fn main() {

main_menu();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn black_bean_and_chicken_soup(){
    clear_screen();
println!("
Black Bean and Chicken Soup




This is so tasty, and healthy too! We love black beans. Prep time does not include soaking and
cooking the beans, but does include cooking the chicken. You can, of course, substitute canned
beans, just drain them. The soup can also be frozen in a sturdy plastic container, or a ziploc
bag. To serve, defrost and heat in microwave or on stovetop.




    Ingredients


1 lb black beans, soaked, cooked, and drained

1 lb boneless skinless chicken breast, cut in bite size pieces

1⁄2 teaspoon cumin powder

1⁄2 teaspoon garlic powder

1⁄4 teaspoon black pepper

2 large onions, diced

2 (14 1/2 ounce) cans diced tomatoes, one can drained only

2 cups corn

2 (14 ounce) cans chicken broth (I use 99% fat free)

2 tablespoons cumin

2 tablespoons garlic powder

2 teaspoons black pepper




    Directions


Sprinkle chicken pieces with 1/2 tsp cumin, 1/2 tsp garlic powder, and 1/4 tsp black pepper.

Cook on medium high heat in small about of oil, until chicken is cooked through and lightly
browned, or about 5-7 minutes.

Combine all ingredients and cooked chicken in crockpot.

Stir well, and taste to make sure spices are how you like them.

Cook on high heat one hour, then low for 3-4 hours, or for however long you need it to cook.

It will be done when you are ready for it.




- Amanda Beth



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn chinese_fried_rice(){
    clear_screen();
println!("
Chinese Fried Rice




This Chinese fried rice has the flavor those other recipes are missing. Tastes like takeout. I want to
dedicate this dish to Bergy, whose recipe “AM & B's Indonesian Mehoon” has inspired this dish.
Make sure you season your rice with salt before it cooks. Add some butter to the cooking water, as
well. Other seasonings should be added before you cook, as well, so it has time to get inside the
rice. If you like sesame flavor, add 1 tsp. of it after you add the green onions, but do not use it as a
cooking oil because it easily burns.




    Ingredients


3⁄4 cup finely chopped onion

2 1⁄2 tablespoons oil

1 egg, lightly beaten (or more eggs if you like)

3 drops soy sauce

3 drops sesame oil

8 ounces cooked lean boneless pork or 8 ounces chicken, chopped

1⁄2 cup finely chopped carrot (very small)

1⁄2 cup frozen peas, thawed

4 cups cold cooked rice, grains separated (preferably medium grain)

4 green onions, chopped

2 cups bean sprouts

2 tablespoons light soy sauce (add more if you like)




    Directions


Heat 1 tbsp oil in wok; add chopped onions and stir-fry until onions turn a nice brown color, about
8-10 minutes; remove from wok.

Allow wok to cool slightly.

Mix egg with 3 drops of soy and 3 drops of sesame oil; set aside.

Add 1/2 tbsp oil to wok, swirling to coat surfaces; add egg mixture; working quickly, swirl egg until
egg sets against wok; when egg puffs, flip egg and cook other side briefly; remove from wok, and
chop into small pieces.

Heat 1 tbsp oil in wok; add selected meat to wok, along with carrots, peas, and cooked onion; stir-
fry for 2 minutes.

Add rice, green onions, and bean sprouts, tossing to mix well; stir-fry for 3 minutes.

Add 2 tbsp of light soy sauce and chopped egg to rice mixture and fold in; stir-fry for 1 minute
more; serve.

Set out additional soy sauce on the table, if desired.




- PalatablePastime



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn irish_pub_beef_stew(){
    clear_screen();
println!("
Irish Pub Beef Stew




Ingredients


     1 1⁄2 lbs beef, cut into chunks (splurge on the cut if you can)

     1⁄4 cup butter

     1 (10 1/2 ounce) can tomato soup

     1 (10 1/2 ounce) can water

     4 carrots, cut into chunks

     4 large potatoes, cut into chunks (I don't peel carrots or spuds but you can if you like)

     2 stalks celery, cut into chunks

     4 onions, cut into chunks

     2 teaspoons salt

     1 teaspoon black pepper

     1⁄4 cup fresh parsley, chopped fine

     1⁄4 cup good quality cooking sherry

     2 bay leaves




Directions


Preheat oven to 300F degrees.

In a heavy skillet brown the beef in the butter over medium high heat.

Add the soup and water and stir well.

Add the rest of the ingredients and cook for about 5 minutes, stirring once.

Transfer to a cast iron dutch oven or oven proof pot and cook in the oven,
covered for 5 hours, stirring occasionally.

Remove from oven, remove bay leaves and serve with Irish Soda Bread and butter.




- Diana Adcock



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn chicken_noodle_soup(){
    clear_screen();
println!("
Chicken Noodle Soup




This chicken noodle soup is great on a cool day.




Ingredients


     2 teaspoons butter

     1 cup sliced celery

     1 cup chopped carrot

     1⁄2 cup chopped onion

     1⁄2 teaspoon thyme

     1 teaspoon poultry seasoning

     2 (32 ounce) containers and 1 can chicken broth (14 ounce)

     2 teaspoons chicken bouillon

     8 ounces egg noodles

     2 cups cooked chicken (3 frozen breasts)

     parsley




Directions


Melt butter in large pot.

Sauté the celery, carrot and onion for 5 to 10 minutes.

Add thyme, poultry seasoning, chicken broth and bouilion.

Bring to a boil.

Add noodles and chicken and cook on low for 20 minutes.

Sprinkle with parsley.




- MizzNezz



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn comforting_beef_enchilada_soup(){
    clear_screen();
println!("
Comforting Beef Enchilada Soup




Like my Tortilla Soup II recipe but modified for people that like beef instead. Personally, I like the
chicken version much better.




    Ingredients


8 ounces carrots, diced

8 ounces celery

8 ounces onions

1⁄2 teaspoon garlic powder

salt and pepper

2 tablespoons corn oil

4 (15 ounce) cans beef broth

1 (15 ounce) can petite diced tomatoes

1 (10 ounce) can Rotel diced tomatoes

1 (1 1/4-1 1/2 ounce) packet taco seasoning

1 (10 count) package corn tortillas, cut into small pieces

12 ounces shredded beef brisket (or shredded grilled beef fajita meat)

1 cup milk

corn tortilla chips, broken into small pieces




    Directions


Saute carrots, onions, celery in corn oil, garlic powder, salt and pepper until tender.

Add beef broth and bring to boil.

Add tomatoes, Rotel, taco seasoning, and shredded beef.

Cut tortillas into small pieces and add to broth mixture.

Let boil for 20 minutes or until tortillas are thoroughly incorporated into soup, stirring occasionally
to keep from sticking.

Reduce heat and simmer for additional 10 minutes.

Add milk and simmer for additional 10 minutes.

If thicker soup is desired, add more diced tortillas and let incorporate into soup.

Garnish with shredded cheese and broken tortilla chips.

Substitutions: 1 cup Masa Harina (masa flour) for 1 12 ct package of yellow corn tortillas.
Gradually add masa flour mixing into broth. If thicker soup is desired, add more masa flour.

Can also add two seeded/sliced jalapeños and one sprig of fresh cilantro.




- Eddie Shipman



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn grilled_pizza(){
    clear_screen();
println!("
Grilled Pizza




This was so much fun!! and so good!! Can't wait to do it again. Stretching the dough instead of
rolling it makes it nicely uneven, creating crispy textures. We topped with sliced tomatoes, sliced
mushrooms, tender crisp asparagus and parmesan cheese, sprinkled with oregano and thyme. We
just used one dough ball. from the local newspaper.




    Ingredients

1 (1 lb) package frozen bread dough


    Toppings of your choice

prosciutto
sliced tomatoes
fresh asparagus
shaved parmesan cheese
onion
herbs (etc)


    For the seasoning

1 cup extra virgin olive oil
2 teaspoons ground oregano
1 teaspoon onion powder
3⁄4 teaspoon ground black pepper
1⁄2 teaspoon curry powder
1⁄2 teaspoon salt
1⁄2 teaspoon garlic powder
1⁄2 teaspoon red pepper flakes
1⁄4 teaspoon ground cumin
1⁄4 teaspoon cayenne pepper
chopped tomato (optional)
chopped onion (optional)
chopped fresh basil (optional)




    Directions


to make the seasoning: In a small ssaucepan combine all the seasoning ingredients. Whisk over low
heat just until warm and oil becomes fragrant. Remove from heat and let rest for 2 hours at room
temperature.

The Pizza bread: Lightly flour the back of a 15 x 10 inch baking sheet and one of the pizza dough
balls. Stretch out the dough to the size of the baking sheet. Place the stretched dough on the back of
the baking sheet.

Stir the seasoned oil and liberally brush the surface of the dough. Turn the baking sheet (like a big
spatula) with the dough directly onto cooking grate. Remove the baking sheet and liberally brush
the top side of the dough with the seasoned oil.

Grill over direct medium heat until the underside of the dough is marked, 1 to 3 minutes. Don't
worry if the crust bubbles, it will deflate when turned over.

Slide the dough onto the baking sheet and flip the ungrilled side onto the grill. Brush the top with
more oil and continue to grill for 3 to 4 minutes.

Remove from grill and cool slightly on a cooling rack. Add toppings, return to grill, close lid and
grill to desired doneness (cheese melts etc.) Repeat grilling procedure for 2 remaining balls of
dough.




- Derf2440



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn buttermilk_waffles(){
    clear_screen();
println!("
Buttermilk Waffles




Good waffle recipes are surprisingly hard to find. Most of them say to use “waffle mix”, or call for
sugar to be dumped in. I consider this to be nothing short of blasphemy, so I decided to create my
own recipe. It's very light and fluffy.

This recipe makes approximately fourteen 10 cm * 10 cm waffles.




    Ingredients


1.5 Cups flour
1 Teaspoon baking powder
1 Teaspoon baking soda
Pinch of salt
1.5 Cups buttermilk
4 Eggs, separated
0.5 Cups oil or applesauce
2 Teaspoons vanilla extract




    Preparation


In a large bowl, mix the flour, baking powder, baking soda, and salt together. Add the buttermilk,
egg yolks, oil (or applesauce), and vanilla.

In a separate bowl, beat the egg whites until stiff peaks form.

Beat the first batter mixture until smooth. Some cross contamination from the egg whites is
acceptable.

Gently stir the stiff egg whites into the rest of the batter. Do not overmix.




By Spatula Tzar



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn chicken_tortilla_soup_ii(){
    clear_screen();
println!("
Chicken Tortilla Soup II




I'm getting tired of seeing the so-called tortilla soup recipes running around that are nothing but a
glorified chicken soup with tortillas added. Here's the real McCoy. Similar to the recipe at Houston's
restaurant (modified, of course, to suit my tastes).




    Ingredients


8 ounces carrots, diced

8 ounces celery

8 ounces onions, diced

1⁄2 teaspoon garlic powder or 1 garlic clove, diced

1⁄8 teaspoon salt

1⁄4 teaspoon pepper

1 tablespoon corn oil

4 (15 ounce) cans chicken broth

1 (15 ounce) can tomatoes, diced (optional)

1 (10 ounce) can Rotel tomatoes & chilies, diced

1 (1 1/4-1 1/2 ounce) packet taco seasoning (I use McCormicks)

10 (8 inch) corn, tortillas (NOT FLOUR, cut into small pieces, about 1-in x1-in)

12 ounces chicken meat, poached, diced

1 cup milk or 1 cup sour cream

12 ounces monterey jack cheese or 12 ounces Mexican blend cheese, shredded

corn tortilla chips, broken into small pieces for garnish




    Directions


Saute carrots, onions, celery in corn oil, garlic, salt and pepper until tender.

Add chicken broth and bring to boil.

Add tomatoes, Rotel, taco seasoning, and chicken.

Cut Tortillas into small pieces and add to broth mixture.

Let boil for 45 minutes or until tortillas are thoroughly incorporated into soup stirring occasionally
to keep from sticking.

Reduce heat and add 8 oz. cheese. Simmer for additional 10 minutes.

Add milk and simmer for additional 10 minutes.

If thicker soup is desired, add more diced tortillas and let incorporate into soup.

Garnish with shredded cheese and broken tortilla chips.

Substitutions: 1 cup Masa Harina (Masa Flour) for 1 10 ct. package of corn tortillas.

Mix masa with 1 cup cold water, then add masa flour mix into soup. If thicker soup is desired, add
more masa/water mix.

If you don't want to use the seasoning packet, substitute 1/2 teaspoons cumin + 1 tsp, chili powder.

You can also use grilled chicken fajita meat for poached diced chicken.




NOTE: If you use FLOUR tortillas, as some people have, you WILL end up with a “TORTILLA
      MESS”, this recipe is for CORN tortillas. Don't go blaming me and giving me a bad review because
      you used the wrong stuff.




- Eddie Shipman



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn beef_and_broccoli_stir_fry(){
    clear_screen();
println!("
Beef and Broccoli Stir-Fry




I clipped this recipe from Taste of Home magazine several years ago and have found it to be the
best-tasting easy beef and broccoli stir-fry. It is credited to Ruth Stahl. I often use charcoal chuck
steak, which is very tender and lean. I also like that it doesn't call for any unusual ingredients.




    Ingredients


3 tablespoons cornstarch, divided

1⁄2 cup water, plus

2 tablespoons water, divided

1⁄2 teaspoon garlic powder

1 lb boneless round steak or 1 lb charcoal chuck steak, cut into thin 3-inch strips

2 tablespoons vegetable oil, divided

4 cups broccoli florets

1 small onion, cut into wedges

1⁄3 cup reduced sodium soy sauce

2 tablespoons brown sugar

1 teaspoon ground ginger

hot cooked rice, for serving

toasted sesame seeds, for serving (optional)




    Directions


In a bowl, combine 2 tablespoons cornstarch, 2 tablespoons water and garlic powder until smooth.

Add beef and toss.

In a large skillet or wok over medium-high heat, stir-fry beef in 1 tablespoon oil until beef reaches
desired doneness; remove and keep warm.

Stir-fry onion in remaining oil for 4-5 minutes until softened. Add the broccoli and cook for 3
minutes until the broccoli is tender but still crisp. Return beef to pan.

Combine soy sauce, brown sugar, ginger and remaining 1 tablespoon cornstarch and 1/2 cup water
until smooth; add to the pan.

Cook and stir for 2 minutes.

Serve over rice and garnish with toasted sesame seeds (optional).




- Chris from Kansas



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn the_best_chili_ever(){
    clear_screen();
println!("
The Best Chili Ever




This is the best chili recipe I have ever tried. I'm not sure where the
recipe originated, but it is amazing! Sometimes, I don't bother adding all
four cans of the kidney beans and it still turns out wonderful. Once
anyone tastes this chili, they will be begging for the recipe!! Enjoy!




Ingredients


     2 teaspoons oil

     2 onions, chopped

     3 cloves garlic, minced

     1 lb lean ground beef

     3⁄4 lb beef sirloin, cubed

     1 (14 1/2 ounce) can diced tomatoes

     1 can dark beer

     1 cup strong coffee

     2 (6 ounce) cans tomato paste

     1 can beef broth

     1⁄2 cup brown sugar

     3 1⁄2 tablespoons chili sauce

     1 tablespoon cumin

     1 tablespoon cocoa

     1 teaspoon oregano

     1 teaspoon cayenne

     1 teaspoon coriander

     1 teaspoon salt

     4 (15 ounce) cans kidney beans

     4 chili peppers, chopped




Directions



    Heat oil.

    Cook onions, garlic and meat until brown.

    Add tomatoes, beer, coffee, tomato paste and beef broth.

    Add spices Stir in 2 cans of kidney beans and peppers.

    Reduce heat and simmer for 1 1/2 hours.

    Add 2 remaining cans of kidney beans and simmer for another 30 minutes.




- AmandaAOates



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn scott_hibb_whiskey_grilled_baby_back_ribs(){
    clear_screen();
println!("
Scott Hibb's Whiskey Grilled Baby Back Ribs




This is really an amazing baby back ribs recipe! I found it about a year ago at another recipe site
and have been making it ever since.




    Ingredients


2 (2 lb) baby back rib racks

fresh coarse ground black pepper

1 tablespoon ground red chili pepper

2 1⁄4 tablespoons vegetable oil

1⁄2 cup minced onion

1 1⁄2 cups water

1⁄2 cup tomato paste

1⁄2 cup white vinegar

1⁄2 cup brown sugar

2 1⁄2 tablespoons honey

2 tablespoons Worcestershire sauce

2 teaspoons salt

1⁄4 teaspoon fresh coarse ground black pepper

1 1⁄4 teaspoons liquid smoke flavoring

2 teaspoons whiskey

2 teaspoons garlic powder

1⁄4 teaspoon paprika

1⁄2 teaspoon onion powder

1 tablespoon dark molasses

1⁄2 tablespoon ground red chili pepper




    Directions


Preheat oven to 300°F (150°C).

Cut each full rack of ribs in half, so that you have 4 half racks.

Sprinkle salt and pepper (more pepper than salt), and 1 tablespoon chili pepper over meat.

Wrap each half rack in aluminum foil.

Bake for 2 1/2 hours.

Meanwhile, heat oil in a medium saucepan over medium heat.

Cook and stir the onions in oil for 5 minutes.

Stir in water, tomato paste, vinegar, brown sugar, honey, and Worcestershire sauce.

Season with 2 teaspoons salt, 1/4 teaspoon black pepper, liquid smoke, whiskey, garlic powder,
paprika, onion powder, dark molasses, and 1/2 tablespoon ground chili pepper.

Bring mixture to a boil, then reduce heat.

Simmer for 1 1/4 hours, uncovered, or until sauce thickens.

Remove from heat, and set sauce aside.

Preheat an outdoor grill for high heat.

Remove the ribs from the oven, and let stand 10 minutes.

Remove the racks from the foil, and place on the grill.

Grill the ribs for 3 to 4 minutes on each side.

Brush sauce on the ribs while they're grilling, just before you serve them (adding it too early will
burn it).




- Chippie1



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn brownies(){
    clear_screen();
println!("
Brownies




These brownies are not cake-like. They are more moist and gooey. I always undercook them a bit
because we like them very moist.




    Ingredients


1⁄2 cup vegetable oil
1 cup sugar
1 teaspoon vanilla
2 large eggs
1⁄4 teaspoon baking powder
1⁄3 cup cocoa powder
1⁄4 teaspoon salt
1⁄2 cup flour




    Directions


Preheat oven to 350 degrees Fahrenheit or 180 degrees Celsius.

Mix oil and sugar until well blended.

Add eggs and vanilla; stir just until blended.

Mix all dry ingredients in a separate bowl.

Stir dry ingredients into the oil/sugar mixture.

Pour into greased 9 x 9 square pan.

Bake for 20 minutes or until sides just starts to pull away from the pan.

Cool completely before cutting.


Note: I usually double the recipe and bake in a 9 x 13 pan. If you double the recipe, you will need to
      cook longer than 20 minutes.




- Juenessa



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn otellos_penne_bolognese(){
    clear_screen();
println!("
Otello's Penne Bolognese




Ingredients


olive oil

500g beef mince

one large onion

2 cups of tomato sauce

2 tablespoons of sugar

penne pasta




Directions


heat oil in a large frying pan. add onion and cook, stirring until
softened. add the beef mince keep stirring until browned.

add the sauce and bring to a simmer, then reduce to low heat.

while the sauce is simmering bring a large saucepan of water to boil for the pasta.

once the penne pasta is cooked strain the pasta then mix well with the sauce.

enjoy.



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn otellos_beans_and_noodles(){
    clear_screen();
println!("
Otello’s 4 Beans & noodles




Ingredients


1 Medium sized onion, diced,

4 medium sized potatoes pealed chopped Into 8 chunks.

400g tin of Four beans

400g tin of chick peas

1 1/2 cups of baby peas

Salt to taste.

500g elbow pasta.

1/4 cup of olive oil.




Directions


using a large saucepan, add the onion, potato, and the chick peas, four beans, then
add 750ml of water, bring to boil then reduce to low heat to a simmer till potatoes are cooked.

bring back to full boil, add pasta, when the liquid starts boiling again, add peas.

once pasta is cooked, add the olive oil.

enjoy.



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn pizza(){
    clear_screen();
println!("
Pizza



Make your own pizza topped with green peppers, mushrooms, or other vegetables.



Ingredients

    1/4 mini baguette or Italian bread (split lengthwise, or 2 split English muffins)

    1/2 cup pizza sauce

    1/2 cup mozzarella or cheddar cheese (part-skim, shredded)

    1/4 cup green pepper (chopped)

    1/4 cup mushrooms (fresh or canned, sliced)

    vegetable toppings (others, as desired, optional)

    Italian seasoning (optional)



Directions

Toast the bread or English muffin until slightly brown.

Top bread or muffin with pizza sauce, vegetables and low-fat cheese.

Sprinkle with Italian seasonings as desired.

Return bread to toaster oven (or regular oven preheated to 350 °F).

Heat until cheese melts.



 Source:

Pumpkin Post and Banana Beat Newsletters
University of Massachusetts Extension
Nutrition Education Program



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn mexican_chicken_chili_soup(){
    clear_screen();
println!("
Mexican Chicken Chili Soup




This is Top Secret Recipes version of The Soup Nazi's Mexican Chicken Chili. The secret to this
soup seems to be the long simmering time. If you like, you can substitute turkey breast for the
chicken to make turkey chili.




    Ingredients


1 lb chicken breast fillet (4 fillets)

1 tablespoon olive oil

10 cups water

2 cups chicken stock

1⁄2 cup tomato sauce

1 potato, peeled & chopped

1 small onion, diced

1 cup frozen corn

1⁄2 carrot, sliced

1 stalk celery, diced

1 cup canned diced tomato

1 (15 ounce) can red kidney beans, plus liquid

1⁄4 cup diced canned pimiento

1 jalapeno, diced

1⁄4 cup chopped Italian parsley

1 garlic clove, minced

1 1⁄2 teaspoons chili powder

1 teaspoon cumin

1⁄4 teaspoon salt

1 dash cayenne pepper

1 dash basil

1 dash oregano



    On the side

sour cream

1 pinch chopped Italian parsley

grated cheddar cheese (optional)




    Directions


Sauté the chicken breasts in the olive oil in a large pot over medium/high heat.

Cook the chicken on both side until done-- about 7-10 minutes per side.

Cool the chicken until it can be handled.

Do not rinse the pot.

Shred the chicken by hand into bite-sizes pieces and place the pieces back into the pot.

Add the remaining ingredients to the pot and turn heat to high.

Bring mixture to a boil, then reduce heat and simmer for 4-5 hours.

Stir mixture often so that many of the chicken pieces shred into much smaller bits.

Chili should reduce substantially to thicken and darken (less orange, more brown) when done.

Combine some chopped Italian parsley with sour cream and serve it on the side for topping the
chili, if desired.




- dale7793



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn spaghetti_sauce(){
    clear_screen();
println!("
Spaghetti Sauce




This is a recipe I have developed over a number of years. This is the only spaghetti sauce my family
will request! Very easy to make. I hope your family enjoys it as much as mine does!




    Ingredients


1 1⁄2 lbs ground beef

2 tablespoons olive oil

1 medium onion, chopped

2 cloves garlic, minced (to taste)

2 bay leaves

1 teaspoon oregano

1 teaspoon dried basil

1 teaspoon italian seasoning

1 teaspoon salt (, or to taste)

ground pepper

1 (6 ounce) can tomato paste

2 (16 ounce) cans tomato sauce

1 (28 ounce) can diced tomatoes

8 ounces fresh mushrooms, sliced and sauteed in butter (optional)

parmesan cheese, freshly grated (optional)




    Directions


Brown the ground beef, onion and garlic in olive oil with bay leaves, oregano, basil, Italian
Seasoning, salt and pepper.

Add tomato paste, tomato sauce and diced tomatoes.

Stir well and bring to a simmer over medium heat.

Cover and simmer for 1 1/2 hours.

Use sauce to top your cooked spaghetti.

Top with sauteed mushroom.

Pass the Parmesan.




- Bev I Am



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn anzac_slice(){
    clear_screen();
println!("
ANZAC slice




    Ingredients

1 1/4 cups plain flour
1 1/4 cups rolled oats
1 cup firmly packed brown sugar
1 cup shredded coconut
150g butter, chopped
2 tablespoons golden syrup
1/2 teaspoon bicarbonate of soda
2 tablespoons boiling water




    Directions

Preheat oven to 180°C/160°C fan-forced. Grease and line a 3cm-deep, 19cm x 29cm (base) slice
pan with baking paper, allowing a 2cm overhang at long ends.

Combine flour, oats, sugar and coconut in a large bowl. Make a well in the centre.

Place butter and syrup in a saucepan over low heat. Cook, stirring occasionally, for 8 to 10 minutes
or until smooth. Combine bicarbonate of soda and boiling water in a jug. Remove butter mixture
from heat. Stir in bicarbonate of soda mixture. Add to flour mixture. Stir to combine.

Transfer to prepared pan. Using the back of a spoon, press mixture evenly into pan. Bake for 25 to
30 minutes or until golden. Cool in pan. Cut into squares. Serve.




- Brett Ede



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn any_day_is_a_picnic_chicken_salad(){
    clear_screen();
println!("
Any Day's a Picnic Chicken Salad





Onion and pickle relish spice up a traditional chicken salad.



Ingredients

    2 1/2 cups chicken breast (cooked, diced)
    1/2 cup celery (chopped)
    1/4 cup onion (chopped)
    3 packages pickle relish (2/3 tablespoon)
    1/2 cup mayonnaise (light)



Directions

    Combine all ingredients.
    Refrigerate until ready to serve.
    Use within 1-2 days. Chicken salad does not freeze well.



How to use:

    Make chicken salad sandwiches.
    Make a pasta salad by mixing with 2 cups cooked pasta.
    Kids will love this salad served in a tomato or a cucumber boat.



 Source:

University of Wisconsin Cooperative Extension Service
A Family Living Program.


╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn bean_and_rice_burritos(){
    clear_screen();
println!("
Bean and Rice Burritos




These baked burritos are a great way to use leftover cooked rice. Try them with brown rice for a
whole grain boost.




    Ingredients


2 cups rice (cooked)
1 onion (small, chopped)
2 cups kidney beans (cooked, or one 15 ounce can, drained)
8 flour tortillas (10 inch)
1/2 cup salsa
1/2 cup cheese (shredded)




    Directions


Preheat the oven to 300 °F.

Peel the onion, and chop it into small pieces.

Drain the liquid from the cooked (or canned) kidney beans.

Mix the rice, chopped onion, and beans in a bowl.

Put each tortilla on a flat surface.

Put 1/2 cup of the rice and bean mix in the middle of each tortilla.

Fold the sides of the tortilla to hold the rice and beans.

Put each filled tortilla (burrito) in the baking pan.

Bake for 15 minutes.

While the burritos are baking, grate 1/2 cup cheese.

Pour the salsa over the baked burritos. Add cheese.

Serve the burritos warm.




Source:

Pennsylvania Nutrition Education Network



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn lasagna(){
    clear_screen();
println!("
Lasagna




My family's favorite. This is a very meaty lasagna. We prefer it with cottage cheese, but you can
easily substitute ricotta.




    Ingredients


1 1⁄2 lbs lean ground beef

1⁄2 lb Italian sausage or 1/2 lb ground veal

1 large onion, chopped

2 -3 garlic cloves, minced

1 teaspoon salt

1 teaspoon fresh coarse ground black pepper

1 tablespoon dried parsley flakes

1 tablespoon dried oregano

1 tablespoon dried basil

2 (14 1/2 ounce) cans whole tomatoes, undrained and chopped (or canned chopped tomatoes)

12 ounces tomato paste

24 ounces ricotta cheese

2 eggs, beaten

1⁄2 teaspoon pepper

2 tablespoons parsley

1⁄2 cup grated parmesan cheese

1 lb mozzarella cheese, divided

12 -15 lasagna noodles




    Directions


Brown ground meat, onion and garlic.

Add salt, pepper, parsley, oregano, basil, chopped tomatoes with juice, tomato paste; stirring until
well mixed.

Cover and simmer 1 hour (or longer,but watch for getting too dry).

Cook lasagna noodles according to package directions; drain and set aside.

Spray a 13 x 9 baking pan with cooking spray.

Combine riccota cheese, eggs, pepper, 2 tablespoons parsley, Parmesan cheese and 1/2 1lb of
mozzarella cheese; In a lasagna pan, layer noodles, meat sauce, and cheese mixture; repeat.

Top off with layer of noodles; sprinkle evenly with remaining mozzarella cheese; make sure to
cover noodles completely.

Bake at 375F for 40-60 minutes, or until cheese mixture is thoroughly melted. (I cover w/ foil for
about 40 minutes, then uncover for 15-20 minutes.

Let sit for 15-20 minutes before cutting and serving.




- ratherbeswimmin



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn italian_cannelloni(){
    clear_screen();
println!("
Italian Cannelloni




I've had this recipe for 20 years. It was given to me by my Brother in laws mother (Italian). I have
on occassion substitued the ground beef and pork for ground chicken and/or turkey. Everyone in my
family drools when they know I'm making it.




    Ingredients


2 tablespoons vegetable oil or 2 tablespoons olive oil
1⁄4 cup finely chopped onion
1 teaspoon finely chopped garlic (about 2 cloves)
1 1⁄2 lbs ground beef
1⁄2 lb ground pork
1⁄2 cup grated parmesan cheese
2 tablespoons cream or 2 tablespoons milk
2 eggs, slightly beaten
1 teaspoon oregano
1 dash pepper
200g cannelloni tubes or 200g manicotti, can be used
796ml spaghetti sauce or 796ml homemade spaghetti sauce
2 tablespoons butter or 2 tablespoons margarine




    Directions


In oil in a large frypan, cook and stir onion, garlic until the onion is tender.

Add beef and pork and continue cooking until beef is brown. Remove from heat and drain excess oil.

Mix in half the parmesan cheese, cream, eggs and seasonings.

When cool enough to handle, stuff uncooked cannelloni noodles with mixture. Spread a thin layer
of sauce on the bottom of a 3.5 litre pan. Arrange stuffed cannelloni in a single layer and pour
remaining sauce over, being certain to cover all the pasta.

Sprinkle with the remaining parmesan and dot with butter.

Cover with foil and bake at 375 F for 55 -60 minutes or until tender when pierced with a fork.
(during last 10 minutes shredded mozzeralla can be sprinkled on top and the foil removed if desired.).




- tigra



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn whole_wheat_pancakes(){
    clear_screen();
println!("
Whole Wheat Pancakes



These are SO delicious - the honey & whole wheat give them a wonderful
flavor. Hope you'll enjoy them as much as we do!



Ingredients


     1 cup whole wheat flour

     2 teaspoons baking powder

     1⁄2 teaspoon salt

     1 tablespoon honey

     3 tablespoons oil

     1 cup buttermilk

     2 large eggs




Directions


Stir honey& oil together in a bowl.

Add milk & eggs & beat well.

Mix dry ingredients into the liquids until flour is moistened.




- WJKing



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn traditional_spaghetti_bolognese(){
    clear_screen();
println!("
Traditional Spaghetti Bolognese




I got this recipe from an italian friend on a trip to the spanish isle of mallorca quite a few years ago
and cannot find another to touch it.




    Ingredients


1⁄2 ounce butter

1⁄2 tablespoon olive oil

1⁄2 carrot

1 stalk celery

1⁄2 onion

2 ounces streaky bacon

1⁄2 lb hamburger or 1/2 lb ground beef

1 (7 ounce) can chopped tomatoes

1 bay leaves

1⁄2 to taste salt

1⁄2 to taste fresh ground black pepper

1 clove chopped garlic

2 ounces mushrooms

1⁄8 pint beef stock

1⁄2 glass red wine

1 tablespoon double cream

3⁄8 tablespoon tomato puree

1⁄2 to taste thyme

1⁄2 to taste oregano




    Directions


Gently melt the butter & oil in a large pan which can be covered.

Add chopped carrot, onion, celery, bacon & bay leaves, gently cook until golden.

Add minced steak & garlic, season well with salt & pepper, cook until meat is no longer pink.

Add wine,cook until liquid reduces a little, add mushrooms thyme& oregano.

Blend the tomato puree with the beef stock,then add to pan along with the tinned tomatoes, stir well
then cover and cook on lowest possible heat for a couple of hours. The secret to this dish is long,
slow cooking as to allow the flavors to meld.

As this dish slowly simmers you will need to add more liquid; use either wine or a little water.I find
a little wine does best.

After two hours or so remove from heat, remove bay leaves, add 2 tbsp of cream, stir well, serve
with hot pasta or spaghetti with fresh baked garlic bread and grated cheese.




- fireball



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn lentil_veggie_soup_crock_pot(){
    clear_screen();
println!("
Lentil-Veggie Soup (Crock Pot)




This soup is easy to put together with basic food items that most of us have around. I found the
recipe in Better Homes and Gardens Biggest Book of Slow Cooker Recipes, and I did some
adjustments with the vegetables and herbs used. I was surprised at how good this very simple soup
turned out.




    Ingredients


1 cup dry lentils
1 1⁄2 cups carrots, chopped
1 1⁄2 cups celery, chopped
1 1⁄2 cups onions, chopped
3 garlic cloves, minced
1 teaspoon dried basil
1 teaspoon dried oregano
1⁄2 teaspoon dried thyme
1 tablespoon dried parsley
2 bay leaves
3 1⁄2 cups vegetable broth (2 cans)
1 1⁄2 cups water
1 (14 1/2 ounce) can diced tomatoes
fresh ground black pepper, to taste




    Directions


Rinse lentils.

Place all ingredients except the pepper into a 4-6 quart slow cooker.

Cover and cook on low for at least 12 hours, or high for at least 5 hours.

Season with pepper and remove the bay leaves before serving.




- Vino Girl



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn grilled_chicken_and_avocado_quinoa_pilaf(){
    clear_screen();
println!("
Grilled Chicken and Avocado Quinoa Pilaf




This pilaf recipe pairs avocados and bell peppers with red quinoa and grilled chicken, for a
satisfying, colorful meal.




    Ingredients


2 tablespoons fresh or bottled lemon juice
1/4 cup fresh basil
3/4 teaspoon ground black pepper (divided)
1 avocado (cut into chunks)
1 tablespoon olive oil (divided)
1/4 teaspoon salt
2 small boneless, skinless chicken breasts (about 1 lb)
1 large red bell pepper
1/2 medium onion (chopped)
1 clove garlic (minced)
3 cups water
3 teaspoons sodium-free chicken bouillon
1 1/2 cups red quinoa (uncooked/dry)




    Directions


Heat grill.

Peel and cut avocado into chunks; place in a medium bowl.

Mix lemon juice, basil, an 1/2 tsp black pepper. Drizzle over avocado chunks, toss, and set aside.

Cut chicken breasts in half crosswise.

Mix 1/2 tbsp olive oil, salt, and remaining black pepper. Brush mixture on chicken and red bell
pepper.

Grill chicken and pepper until done. Set chicken breasts aside. Cut pepper into thin strips.

While chicken and peppers are grilling, heat remaining olive oil in a large pan, add garlic and onion,
and cook until tender, about 5 minutes.

Add water, bouillon, and quinoa to pan; bring to boil, cover, reduce heat, and simmer until liquid is
absorbed and quinoa is cooked (about 15-20 minutes).

Place quinoa pilaf in a large bowl and add chicken, red peppers, and avocado. Toss gently.




Notes:  Serving Suggestions: Serve with non-fat milk and orange slices.




Source: Produce For Better Health Foundation



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn belgian_waffles(){
    clear_screen();
println!("
Belgian Waffles




This belgian waffle recipe is delicious! Top with your favorite fruit.




    Ingredients


2 cups all-purpose flour
2 teaspoons baking powder
2 tablespoons confectioners' sugar
1 tablespoon vegetable oil
2 cups milk
3 eggs, separated
2 teaspoons vanilla
1 pinch salt
whipped cream




    Directions


Combine the flour, baking powder, confectioners sugar, oil, milk and egg yolks.

Beat the egg whites and salt until they stand in soft peaks, mix in the vanilla at this time and fold
into the batter (do not over mix).

Pour 1/8 of the mixture into a hot waffle iron and bake for about 2 minutes.

Repeat with the remaining batter.

Top with your favorite fruit and whipped cream and serve hot.




- Charlotte J



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn slow_cooker_chicken_noodle_soup(){
    clear_screen();
println!("
Slow Cooker Chicken Noodle Soup




This is a classic chicken noodle soup recipe from a recent issue of Good Housekeeping that I'd like
to try. Update: I have made this soup and really like it. I did use 3-4 skinless chicken breasts in
place of the whole chicken, and I substituted 8 cups of canned chicken broth for the water. If you do
this, you might want to add a little less salt unless you use a low-sodium chicken broth. I'm not sure
why some reviewers say that their egg noodles dissolve, unless they are using the very fine egg
noodles. I use a wide noodle and they are just fine after 20 minutes in the Crock-Pot.




    Ingredients


8 cups water or 8 cups canned chicken broth
1 cup carrot, cut into 1/4-inch slices
1 cup celery, cut into 1/4-inch slices
1 cup onion, chopped
1 garlic clove, minced
2 bay leaves
1⁄2 teaspoon dried thyme
4 teaspoons salt (to taste)
1⁄2 teaspoon fresh ground black pepper, to taste
1 (3 1/2 lb) roasting chickens
3 cups wide egg noodles, uncooked




    Directions


In 4-1/2 to 6-quart slow-cooker bowl, combine water, carrots, celery, onion, garlic, bay leaves,
thyme, 4 t salt, 1/2 t pepper.

Place whole chicken on top of vegetables.

Cover slow cooker with lid and cook as manufacturer directs on low setting 8 to 10 hours or on high
4-5 hours.

Transfer chicken to cutting board. Discard bay leaves. Add noodles to slow-cooker; cover with lid
and cook (on low or high) 20 minutes.

While noodles cook, remove and discard skin, fat and bones from chicken; shred meat.

Skim fat from soup and discard. Return chicken to soup to serve.




- CookingONTheSide



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn fruit_salad_with_crystallized_ginger(){
    clear_screen();
println!("
Fruit Salad with Crystallized Ginger




Ingredients


2 cups Granny Smith apples

2 cups papaya cubes

1 cup kiwi slices

1-1/2 cups poppy seed dressing

1/4 cup crystallized ginger

1 cup raspberries

1/4 cup seedless grapes

2 tablespoons lime juice

Fresh mint leaves




Directions


toss ingredients in large bowl. ready to serve!

by Casey Ryback - Under Siege 2: Dark Territory



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn stuffed_green_peppers(){
    clear_screen();
println!("
Stuffed Green Peppers




These tasty stuffed peppers make a delicious and filling entree. Use brown
rice to increase your whole grains.




Ingredients

    4 green pepper (large, washed)
    1 pound turkey, ground, 85% lean
    1 cup rice, uncooked
    1/2 cup onion (peeled and chopped)
    1 1/2 cups tomato sauce, unsalted
    ground black pepper (to taste)




Directions

1. Cut around the stem of the green peppers. Remove the seeds and the pulpy part of the peppers.

2. Wash, and then cook green peppers in boiling water for five minutes. Drain well.

3. In saucepan, brown turkey. Add rice, onion, 1/2 cup tomato sauce and black pepper.

4. Stuff each pepper with the mixture and place in casserole dish.

5. Pour the remaining tomato sauce over the green peppers.

6. Cover and bake for 30 minutes at 350 degrees.




 Source:

From Pyramid to the Plate: Healthy Eating by Timing, Combining, and Planning
Adopted from: Eating Right is Basic
Michigan State University Extension



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn quick_easy_pot_roast(){
    clear_screen();
println!("
Quick + Easy Pot Roast




Make pot roast fit for an elegant dinner party for a weeknight meal. This healthy dish is ready in
about an hour.




    Ingredients


2 1⁄2 lbs beef chuck roast
1 teaspoon kosher salt
1⁄4 teaspoon fresh ground black pepper
1 garlic clove, roughly chopped
1 teaspoon Worcestershire sauce
2 teaspoons dried thyme
1 tablespoon tomato paste
1 cup beef broth
1 lb small potato
3⁄4 lb carrot, peeled and roughly chopped
1⁄4 cup chopped fresh parsley




    Directions


Set Instant Pot to sauté mode, add beef and sear for 2 to 3 minutes per side.

Season with salt and pepper, then add garlic, Worcestershire sauce, thyme, tomato paste and beef
broth.

Top beef with potatoes and carrots.

Cover and lock the lid in place. Place the vent to the sealing position and set to manual on high
pressure for 40 minutes. When the cook time is done, allow the pressure to naturally release for 15
minutes.

Release remaining pressure and remove the lid. Using a slotted spoon, transfer carrots and potatoes
to a serving dish. Transfer pot roast to same dish and garnish with parsley.

If a gravy is desired, set the pot to saute mode and bring cooking liquid to a simmer. Mix a
tablespoon of cornstarch with 3 tablespoons of cooking liquid. Add the cornstarch mixture back to
the simmering liquid; cook and stir until thickened. Spoon some of the gravy over the pot roast
before serving.

Serves: 6; Calories: 342; Total Fat: 8 grams; Saturated Fat: 3 grams; Total Carbohydrate: 19 grams;
Sugars: 4 grams; Protein: 45 grams; Sodium: 538 milligrams; Cholesterol: 123 milligrams; Fiber: 4
gram.




- DanaAngeloWhite



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    recipes_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn brick_rocket_stove() {
    clear_screen();
println!("
Brick Rocket Stove




this brick rocket stove is a highly efficient cooking stove that uses a small amount
of wood to generate a lot of heat, making it ideal for outdoor cooking.

this stove is a four layer chimney column design.

First you will need 15 bricks and a suitable base to build on, bare earth or a large
square paver is good. you just need a level ground to build this stove on. each layer
should have the bricks overlapping each other for stability.


layer 1
    three and a half bricks
    with the opening faceing
    to the right. the half
    brick needs to go to the
    the back right corner.

layer 2
    three and a half bricks
    with the opening faceing
    forward. the half brick
    needs to go to the front
    Leth corner.

layer 3
    four full bricks.

layer 4
    four full bricks. for a
    pot stand on top of the
    stove turn two bricks
    on their side.


 PJ????????????????7PY7???????G:             .!!!!!!!!!!!!!!.                 !7777777777777.
 #^                 P7        B:             :P            P:                .&@5Y555555Y5&@^
 #^                 P7        B:             :P            5:                .&#          B@:
 #^                 P!        B:         ... ^P............P^ .............. :&#.         B@:
 #J!77777777!!!!!!!!GY!!!!!!!7B:      7#BGGGGB#GGGGGGGGGGGG#BGGGGGGGGGGGGGGGGB@#.         B@:
 #7~~~~~~~?B^^^^^^^^:^^^^^^^^^:       ?@5.:::::::::::::::::::::::::::::::::::^&#.         B@:
 #^       ~G                          ?@5                                    .&#.         B@:
 #^       ~G                          ?@5                                    .&#.         B@:
 #^       ~G                          ?@J                                    .&#.         B@:
 #^       ~&J?????????????????P~      ?@P~~~~~~~~~~~~~~~~~^^~~~~~~~~~~~~~~~~~!&&!~~~~~~~~~#@7~~~~~~:
 #^       ~@^                 G!      ?@#PPPPPGPPGGGGGGGPG&@GPGGGGGGGGGGGGGGGGGPGGGGGGGGGGGGGGGGG#@J
 #^       ~@:                 P!      ?@J                 #&:                                    ?@J
 #^       ~@:                 P!      ?@J                .#&:                                    J@J
 #?????????@7?????????????????B!      ?@J                .#&:                                    5@J
                                      ?@J                 #&.                                    J@J
                                      ?@GJJJJJJJJJJJJJJJJY&@YJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJG@J
 J?????????Y??????????????????J.      ?@G????????????????????????????????????J@@J????????????????P@J
 #~.......!&:................:#:      ?@Y                                    .#&.                ?@J
 #^       ~#.                 B:      ?@P                                    .&&.                ?@J
 #^       ~#                  B:      ?@Y                                    .&&.                ?@J
 #^       ~&^^^^^^^^::^^^^^^^^B:      ?@Y                                    .&&.                ?@J
 #^       ~B!!!!!!!!5GJJJJJJJY#:      ?@&GGGGGGGGGGGGGGGGGBBBBBBBBBBBBBBBBBBGB@@BGGGGGGGGGGGGGGGG#@J
 #^       ~G        JJ        B:      ?@Y:^^^^^^^^^^^^^^^^#@YJJJJJJJJJJJJJJJJJ&&~^^^^^^^^^^^^^^^^5@J
 #^       ~G        JY        B:      ?@J                 #@?7777777777777777?&&.                ?@J
 #^       ~G        JY        B:      ?@J                .#@?7777777777777777?&&.                Y@J
 #Y7?????7YG        JY        B:      ?@J                 #@?7777777777777777?&&.                Y@J
 #~.......!G        JY        B:      ?@5:^^^^^^^^^^^^^^^~&@YJJJJJJJJJJJJJJJJY@&~^^^^^^^^^^^^^^^^Y@J
 #^       ~G        JY        B:      !BGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGB7
 #^       ~G        JY        B:
 #!.:::::.7G        J5 ......:#:
 777777777?!        ~J7???????J.


 #5J?????????????????????????7B
 #^                 P!       :B.
 #^                 P7       :B.      ^7777777777777777777777777777777777777777^::::::::::::::::::::
 #^                 P7       :B.      J@B555555555555555555555555555555555555B@Y                   !
 #Y???????J5????????B!       :B.      J@?                                    ?@J                   !
 #^       !G        P7       :B.      J@?                                    ?@Y                   !
 #^       ~G        P7       :B.      J@?                                    ?@#GGGGGGGGGGGGGGGGGB#7
 #^       !G        P!       :B       J@?                                    ?@#^:^^^^^^^^^^^^^^:Y@?
 #^       ^P~~~~~~~^P?^~~~~~~!G.      J@?                                    ?@#.                J@?
 #^       :B!~~~~~~~~~~~~~~~~!B:      J@?                                    ?@#.                J@?
 #^       :B                  B:      J@?                                    ?@#                 J@?
 #^       :B.                 B:      J@Y^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^Y@&~^^^^^^^^^^^^^^^^5@?
 #^       :B.                .B:      J@#GGGGGGGGGGGGGGGGG&@GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGP#@?
 5J???????75??????????????????P:      J@?                .&&.                                    Y@?
                                      J@?                .#&.                                    5@?
                                      J@?                .#&.                                    5@?
 ^????????^:                          J@?                .#&.                                    J@?
 #?       7G                          J@GJJJJJJJJJJJJJJJJY&&YJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJB@?
 #^       ~#7?????????????????J       J@BJJJJJJJJJJJJJJJJY&@GPPPPPPPPPPPPPPPPB@G?JJJJJJJJJJJJJJJ?G@?
 #^       ~G                 ^#.      J@?                .#&7!77777777777777!5@J                 J@?
 #^       ~G                 :G       J@Y                .#&?77777777777777775@J                 J@?
 #^       ~#???????7?Y????????G:      J@5                .#&?77777777777777775@J                 J@?
 #^       ~G        JY       .B:      J@J                .#&?77777777777777775@J                 J@?
 #^       ~G        JY        B:      J@#GGGGGGGGGGGGGGGGG&@BBBBBBBBBBBBBBBBB#@#PGGGGGGGGGGGGGGGP#@?
 #^       ~G        JY        B:      J@Y^^^^^^^^^^^^^^^^~&&~^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^5@?
 G!???????7G????????Y?        B:      J@?                .#&.                                    5@?
 B?                !G!        B:      J@?                .#&.                                    5@?
 #^                 P7        B:      J@?                .#&.                                    J@?
 B?!!!!!!!!!!!!!!!!~B!        B:      J@Y:^^^^^^^^^^^^^^^~&&~^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^P@?
                    G7       .B:      7BGGGGGGGGGGGGGGGGGGBBGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGB7
                    J?????????P:



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
pyro_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn the_hobo_stove() {
    clear_screen();
println!("
The Hobo Stove


A hobo stove is a style of improvised heat-producing and cooking device used in
survival situations, by backpackers, hobos, tramps and homeless people. Hobo
stoves can be functional to boil water for purification purposes during a power
outage and in other survival situations, and can be used for outdoor cooking.

-From Wikipedia



this design works amazingly well despite the small size of the stove. it acts
like a chimney, as heat rises air is sucked through the perforated elevated floor
supplying oxygen directly under the fire.







Materials and Tools:


    Tin snips

    large tin-can

    metal lid from the
    same tin or the floor
    from another tin-can
    of the same size.

    Safety gloves





1    Start by preparing the can. Using the tin snips, make four or five one-inch V
     shaped cuts on the side of your tin-can at the bottom, then press them in at 90
     degrees. this is going to be the tabs to hold up the elevated floor with
     perforated holes, as well as the air intake holes.

    _________elevated floor______________________________________
                                    ╲            ╱
                                     ╲          ╱
          one inch between            ╲        ╱
        the between the floor          ╲      ╱
        and the elevated floor.         ╲    ╱
                                         ╲  ╱
    _____________floor____________________╲╱_____________________




2    so the fire can breathe with a frying pan on the stove you will need to create a
     stove trivet. this resembles the merlons and crenels you see on medieval castles.
     cut out crenels along the top of your tin-can, the merlons need to be about two
     inches apart from each other.
    _____________           __________________            ________________
                !           !    merlons     !           !
                !  crenels  !   2 inches     !           !
                !___________!                !___________!



3   preparing the elevated floor. drill or punkser holes the size of fingers on the
    lid, then place the lid on the triangle tabs. this is what allows direct air
    flow under the fire greatly improving efficiency.




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
pyro_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn the_cat_alien_conspiracy(){
    clear_screen();
println!("
The cat alien conspiracy




Don't let your cat see you reading this about aliens.

Since as far as we know the beginning of man, aliens have been using cats to try to stop us from
progressing to the reasonably advanced race that human beings have become. They do this because
they aren't allowed to directly kill us, however they don't want us to catch up to them or even
become as advanced as their race.

The aliens originally took the approach of placing large cats like tigers, lions, jaguars, etc. Here to
kill off our ancestors to slow us down before we even had a chance to start. This back fired on them,
though it did kill off a lot of them that had intelligence however not the muscle needed to actually
use the weapons that they were trying to invent. They did not kill all of them and we managed to
push forward.

Then the aliens left for a fairly long time however thinking that the original large cats would wipe
all of us out, and just left us for dead. Later while checking in on us they noticed that a large tower
was being built and saw that we were advancing quicker than they were comfortable with, so they
destroyed that building made us speak different languages and sent us all in different directions.
Once again they threw a monkey wrench in to our species development but this time they blamed it
on “God”.

When the aliens sent us to each of these different areas they stripped us of our tools. Problem is they
forgot to wipe our memories of how to make those tools and start over again. This language change
and being divided set us back a good deal however it did not make recovery impossible and the
separate civilizations were able to get back to where they were technology wise at different
speeds.Those poor primitive tribes of today have barely fully recovered. However this time they
visited a few times to check on us and saw that we recovered a little yet were still struggling, not to
mention realizing that the language barrier that they created was working better than they ever
imagined. Satisfied with their work their visits became increasingly far apart as their confidence
grew.

Later on one day while passing through the area the aliens decided to take a short detour over to
earth just to see what these pesky little humans were up to. They noticed an advanced civilization
had created huge things that we have come to know as pyramids. This really caught their eye,
especially since with the massive size of them and the fact that they had no idea how the civilization
could make such things with the relatively primitive tools that they had, not to mention in the
middle of an area as inhospitable as a desert. They came to the realization that the large cats failed
their mission and that they needed a new approach.

At this point they decided the only way to find out how all of this happened was by placing spies.
This is when they left us with a smaller more intelligent form of cat to watch, learn, and hopefully
even do somethings to sabotage the humans technological growth. They gave them specific
instructions, to act cute, go in to the areas that the humans lived in and allow them to think that they
had domesticated them like they had domesticated dogs long ago. They equipped them with
telepathy abilities so that they could both communicate the reports to the aliens, which try to be
invisible to us however with some of us they fail and appear to be ghosts.

The “house cats” soon discovered that these telepathy abilities (with the aid of toxoplasma gondii),
could be used as a form of mind control to make the humans believe that they were cute and worth
keeping. As opposed to just being pompous, egotistical, picky, things that that did nothing more
than eat, sleep, completely ignore us, and refuse to be trained, while really not helping us at all. Not
to mention using sunbeams that we now know can be used for solar power.

As they got used to this royal treatment while spying on the people that pulled off the miracle of
those pyramids. They reported that these humans had managed to do it by a use of math and science
that was amazingly advanced despite everything the aliens has tried to do to stunt this relatively
young species technological advancement, and told them that it was all done in the name of their
religion which the aliens knew from their own experience to be a big step in the development of
culture and civilization. That with the reports of large libraries of knowledge scared the aliens.

They knew this was bad, they realized that a good amount of these humans were learning to sense
their presence even though they were invisible and were even calling them spirits which they had
incorporated in to their religion. However they saw that the humans noticed that when a cat was
around these “spirits” left very quickly, and generally didn't leave until a cat was brought in to the
area. The aliens told the cat spies to use this to their advantage to make these humans believe that
they were protecting them from the spirits.

This worked beautifully all skepticism that the humans ever had of the cats went right out the
window, They believed them to be guardians sent to them by the gods (haha close but not close
enough to the truth) the most advanced race of the time had now started making statues to worship
these spies, even went as far as to make a unimaginably huge monument known as the sphinx in
honor of these well disguised spies. Which they believed to now have the greatest guardian power
of all cats. When the cats displayed their love for it, the Egyptians believed that it had just received
their blessing.

These people truly loving the cats however having way to much of a “good” thing becomes a bad
thing, started selling them to other people in other areas through barter and trade. This was great for
them because they were making wealth off of these guardians, and worked even better for the aliens
because now their spies were being spread across the planet, very easily infiltrating the planet as
technology and the ability to travel advanced.

Jumping along to modern day, cats have now gotten in to every continent and I think every country.
Chinese apparently are eating them while keeping others as “pets” but hey we all know every war
has its casualties. Also we know that they see dogs (the humans long time, perfectly loyal,
completely loving, never will go out of style companion) as their natural enemy. However they
manage to get along when they see that they are both in the humans house to stay and neither is
going any where.

To this day they continue to attempt to find out everything that we know, and stop us from learning
more. When you are on the computer they try to stop you from using it while they are clearly
reading what is said on the monitor themselves. When you read a book they read it as well. While
you are watching tv if it sounds to them like you might be learning something they either watch it as
well or try to distract you, either by climbing in your lap or laying on top of the television possibly
with an attempt to cover part of the screen. The aliens maintain their cover of being ghosts, cats to a
degree maintain their attempts to act like they are protecting us from them.

How many times have you “accidentally” tripped or almost tripped over your cat? How many times
was that in the dark where you know they can see perfectly well, or in an area like near the top of
stairs? April 23, 2010 -- According to a CDC report, pets cause fall injuries that send more than
86,000 people to emergency rooms in the USA every year, guess how many of those are cats. The
cat's alien spy mind control ability to make us keep them because of their cute factor has been fine
tuned by them to perfection, though some people see through it and proclaim that they don't like
cats.

On July 16, 1945 the cats started flooding the aliens with urgent reports that the humans were
making and testing very dangerous weapons that they called “atomic bombs”. Less than a month
later (August 15, 1945) every cat spy in the world flooded the aliens with a 2nd wave of reports that
clearly stated that “the humans have fully functional atomic bombs and have just demonstrated that
they aren't afraid to use them.” When that wave of reports came in the aliens freaked out, and
wasted no time in getting to earth as fast as they possibly could.

They were in such a rush to get here, that when they arrived on July 7, 1947 they forgot to use their
breaks in time to slow down for a safe landing. This resulted in their alien ufo crash landing in
Roswell New Mexico, causing their invisible/ghost cover to be damaged and no longer work. This
was quickly cleaned up with all traces of proof covered up by the humans that they were easily able
to use there mind control to make them bend to their will, and try to stop the knowledge from
spreading. For a small few this attempt failed while the rest remain oblivious.

You are probably wondering how does Steve Leighton know this, and how did the aliens manage to
get back home undetected. The aliens with a little power of persuasion got themselves on to the next
rocket that was being launched to explore the moon. This time remembering to clear all of the
memories of it from the humans at NASA that saw them.

As for how I know this about the cats, one of those aliens mated with one of my ancestors, as others
did with some of the best that this world has to offer to create a form of new species to eventually
take over the earth. We appear human to all forms of current technology as a form of stealth. Of
others like me, very few are able to hear what the cats are saying when making their reports
however I am able to and I feel it is only fair that all of you know whats really going on. I will stop
here and leave it open to all questions for two reasons.

If you believe this BS, you can send 30 dollars to me, and I will let the aliens know to leave you
alone, and cat's will like you more.
cash.app/$catalienconspiracy

Remember just because some one calls it an alien conspiracy, does not mean that the alien
conspiracy is true.



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    sfs_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn torlak_and_the_tale_of_the_space_mushrooms(){
    clear_screen();
println!("
                                           .-'~~~-.
                                         .'o  oOOOo`.
                                        :~~~-.oOo   o`.
                                         `. ╲ ~-.  oOOo.
                                           `.; / ~.  OO:
                                           .'  ;-- `.o.'
                                          ,'  ; ~~--'~
                                            ;  ;
                    _______╲│╱ __________╲╲;_╲╲╱╱___╲│╱ ________




Torlak and the Tale of the Space Mushrooms




Torlak of Nanub was running from the feds one cold space day. He sold drugs you see, to be
specific it was space dust, the hottest shit on the planet of Blaznor. Drugs are bad, even to space
aliens, so they must be stopped. At least this is what the government wants you to believe, both
theirs and ours.

While on the run Torlak stopped at a small dinner on Rizorf to get some chow. He ordered a half
plate of space eggs, covered in franks red-hot sauce, with an extra-large side order of hash browns
and toast. Running from the feds sure makes you hungry and he needed a man sized dinner. the eggs
were a bit runny for his taste but he gulped them down anyway. After paying and convincing the
waitress to give him head in the bathroom in lieu of her tip he was on his merry way unaware of the
events that would unfold in the near future.

He used his fake face to make it past the first intergalactic check point when he noticed a slight
rumble in his third stomach. He figured it was his nerves and paid it no attention. After rounding the
horse Nebula the rumble had moved to his second stomach and he felt severe sharp pain in his
fourth. “space shits!” he cried, “just what I need now, FUCK!”

Quickly Torlak left the bridge and headed to his toilet. He flung open the door and parked his
cheeks on the seat and got ready to unleash a toxic dump the likes of which have never been seen by
the galaxy. right before he could pinch off the first loaf he noticed the blinking light over the shit
tank level indicator. “FULL! And no time to empty the shit tanks, I haft a dump my load now!”

Torlak pulled his jeans back up and tried to hold off the flood of turd that was imminent.
Desperately looking for somewhere to drop his load he searched high and low for some kind of
container. Under his seat he found a 6 month old brown bag lunch he forgot to eat. Yellow and
orange with fungus no one would want to eat it but the bag would work just fine for what Torlak
had in mind.

Torlak dropped his pants and put the bag to his ass. With every pound that drained from his body he
felt 100 times better. Ten pounds lighter and pain free Torlak was left with a problem on his hands:
what to do with a huge bag of alien shit. Torlak dons his space suit and opens the main door to his
cockpit. With the cold blackness of space exposed he musters every memory of collage baseball and
throws the bag as hard as he can out the door. with good riddance he closes the door and caries on
his daring run from the police.

Torlak cuts west towards Simplat but his bag of shit makes no notice. With nothing but the rules of
physics governing its voyage it has no choice but to go straight until something else happens to it on
its way. That something just happened to be 30 light years later and a planet called Betagon 7.
Known for being the worst possible place to incubate life, the planet was left to weapons testing and
a colony for Mormons that people got tired of dealing with. No atmoshphere proves difficult to any
complex life from trying to take up residence on Betagon 7 but proves quite helpful to our bag of
unpleasantness. it falls towards the planet in rapid decent and splatters on the ground covering it in
many square feet of brown sludgy goo with a frozen sandwick in the middle.

Slowly the sandwich melts and mixes with the goo. Spores from the fungus get their precious heat
and light and begin to grow. Multiplying rapidly they follow the instructions encoded in their DNA
and organize. First a stem appears, then a bell. Then another shroom pops up and yet another. Over
the next 5 days a total of 6 shrooms manage to make it. to this day they sit as a tribute to Torlak, his
horribly undercooked breakfast and drugs. Remember kids, not all drugs are bad. So if you happen
to run across a nice phat rail of space dust, grab yourself a straw and snort that shit up!






╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    sfs_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn i_think_my_neighbor_is_an_alien(){
    clear_screen();
println!("
I Think My Neighbor Is An Alien




Guys, my neighbors really freak me out. Sometimes at night, they will pull in to their garage and
close the door with what looks like a female human. Then, during the night, they will leave, and I
will never see the female human being again. I think that maybe they are aliens and the “female
human” is their disguised ancestor.

I say this because shortly after they pull into the garage, I hear the car doors close and then I hear a
bunch of sounds like something hitting the car, and I hear screaming. So I think the “female human”
disguised alien is one of their ancestors who can't adapt to the earth's atmosphere. After about
fifteen minutes of the thumping noise, I hear the car doors close again and then they leave and the
car comes back and parks for the night.

I am really kind of freaked out about this. My neighbor is about 30, and “married” to another
humanoid who is strangely absent during these rituals. He always walks around with a t-shirt on
that has some strange scrawlings on it that look like alien language, he says it is something to do
with a group of guys he hung out with in college but I think he's trying to lie to me.

Anyone know how I can find out if these are really aliens??



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    sfs_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn robbanks(){
    clear_screen();
println!("
HOW TO ROB A BANK

and now...The Daredevil of Anarchy Inc. somewhat proudly presents:

      HOW TO ROB A BANK -=- A How-To TextFile (c) 1984 Anarchy Inc.
      --- -- --- - ----     Uploaded by: The Yakuza
                            Nabbed by: The Mayor

Well, now.  You say that you want to go and rob a bank, eh? You say that
you need easy money, eh?  This entertaining little text file will give you
information and tips about how to easily rob a bank, and get away with it.

First off, you'll need a bank(obviously).  Well, I would suggest something
famous, like Wells Fargo, or Bank of the West.  At least you're certain
you'll get in the newspaper.  For about four weeks, stake out the place,
without attracting attention to yourself.  In other words, don't open an
account there.

Next, you'll need a gun.  I would hardly recommend a small pistol, or a
shotgun.  Machine guns and armed missiles are not recommended, as they
usually end up making up quite a mess.  (Remember, if you ARE caught, you
don't want a vandlism count, do you?)

Finally, before you begin, you'll need a partner.  Choose somebody you
know well, but not too well.  If worst comes to worst, you might have to
shoot him, take him for hostage, or turn him in.  Pick somebody dull-
witted, like Little Al, or Matt Ackeret.
                                        (In other words, somebody you
                                         won't miss too much.)

Now, you're ready to get started.  But you'll need a “get-away” car...I
recommend a Buick, or a van.  VW's and Mack Trucks just won't do.  Get
something with a lot of pickup, like BSBAL the Wise's station “the boat”
wagon.  You might want to remove the lincense plate numbers, so the police
won't have any information about you and your party.

What?  Did I say the word “police”?  Well, I'm not talking about Sting and
friends.  I'm talking “The Blue Knight”/“Dirty Harry” type buggers.  They
can get nasty, with those little guns, and nightsticks. They can be rude
too.

Inside the bank, you'll have to rob it quick, as people tend to scream
when others with Ski Masks enter...I would also recommend dressing all in
black.  There will be security cameras there...Nasty things. Get rid of
them.  Also, there might be a security guard or two in there.  I would
suggest shooting them, as they make lousy hostages, and make sure you kill
them.  Remember, if you can't stand the sight of blood all over the neat
little carpets they keep, don't bother robbing banks. Stick to something
like Credit Card fraud, or fone phreaking.

Now, when you first enter the bank, there will be some fool shouting “Oh
my God!  Oh my God!” all over the place.  Reply with some snappy phrase
like: “He can't help you now...” and then shoot him/her. They were giving
you a headache, wern't they?

While standing there with gun in hand, make it very clear to people that
you will shoot them.  You WILL, won't you?  Demonstrate this fact by
shooting several innocent by-standers, and potted plants. You might even
take out a desk while you're at it.  Don't you love this feeling of power?

Money.  That's what you're here for, right?  Well, if you arn't, you've
just blown away several people and a plant for nothing.  You might as well
just leave the place. Money is obviously kept in drawers, where tellers
can make change and such. That's what you're after.  Go to the farthest
teller from the door.  That's where they place all “Tellers in
training”...They're usually pushovers...

Another problem comes to mind.  Bait money.  What the f--- is bait money,
you might ask?  Well, when the stupid teller hands you all the money from
the drawers, one of the little slots that the money is in, trips a silent
alarm.  Not fun.  Well, the only thing it I would suggest is to pick and
choose.  Good luck, as you really can't tell when a silent alarm goes off.

Next problem.  Let's get the hell out of this place, shall we? Okay, let's
go!  I would suggest running like hell to the outside, and once in the
car, finding the car's speed limit in the parking lot.  Look out for speed
bumps...

You're off!  You've made it!  Now, you are onto the road of becoming a
hardened criminal!  Congratulations...Wait...What's that? You're reading
this in prison?  Gosh, I forgot to tell you about those cruel policemen,
and the OTHER security guards.  Oops.  Oh well, enjoy the prison life...

...This text file was not written from personal experience
...The Daredevil, Anarchy Inc., and all members within, are not in any way
responsible for actions that people might take against banks and such. We
do not supply lawyers, or post bail.  If you were jailed because of this
text file, well, that's your problem, not ours.

...Friendly tip of the day: Try practicing on 7-11's and Burger King
before moving up to banks.  It gets you psyched up for your job.  We do
not recommend taking hostages, because I might be at a bank someday, when
some idiot runs in with a shotgun and...

(c) 1984 Anarchy Inc.  All rights reserved.  Have a nice day!

(I hear the food's pretty good in prison...Good luck keeping an even number
 of fingers...)8/353-1553



X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
 Another file downloaded from:                     The NIRVANAnet(tm) Seven

 & the Temple of the Screaming Electron   Taipan Enigma        510/935-5845
 Burn This Flag                           Zardoz               408/363-9766
 realitycheck                             Poindexter Fortran   510/527-1662
 Lies Unlimited                           Mick Freen           801/278-2699
 The New Dork Sublime                     Biffnix              415/864-DORK
 The Shrine                               Rif Raf              206/794-6674
 Planet Mirth                             Simon Jester         510/786-6560

                          “Raw Data for Raw Nerves”
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    sfs_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn the_caffeine_conspiracy(){
    clear_screen();
println!("
The caffeine conspiracy




A cocaine molecule is C17 H21 NO4, when written in a less cryptic form, that makes cocaine | C17
H21 N1 O4 at the molecular level. In comparison, a single caffeine molecule is C8 H10 N4 O2. The
96.5% similarity between caffeine and cocaine is relatively well hidden in plain sight. That is until
you look at the molecular composition of two caffeine molecules is | C16 H20 N8 O4 (caffeine
multiplied by 2).

1 cocaine molecule. | C17 H21 N1 O4
2 caffeine molecules| C16 H20 N8 O4
1 caffeine molecule. | C 8 H10 N4 O2

Exhaled air is about 4.0% CO2, and 18% O2 with normal breathing the 78% N2 remains unchanged
between inhaling and exhaling. With the air we breath being about 78% nitrogen (N2), that small
change in the amount of N2 exhaled would (and or does) go completely unnoticed.

At this point the very close similarity (95.83%) between caffeine and cocaine becomes scarily clear.
For the people that consume large Amounts of coffee, or to many energy drinks. That morning
headache you get before your morning coffee or energy drink helps it go away, is a form of physical
addiction to caffeine.

Withdrawal symptoms for both drugs can include migraines, fatigue, dizziness, irritability,
sleepiness, slowing of activity, lack of pleasure, anxiety, craving, agitation, delayed depression,
uncotrollable shaking. These withdrawal symptoms depending on how much of either of these
drugs a person has been using and how long they have been using it for can last up to one week.

By 1903 due to a wave of anti-narcotics legislations, [then-manager of Coca-Cola Asa Griggs]
replaced the cocaine in coca cola with caffeine.

Caffeine is in coffee, black tea, green tea, oolong tea (wu-yi tea) most 'energy' drinks, colas, and
many sodas/pop. It is also hiding in “decaffeinated” coffee and tea, chocolate, chuppa-chups,
guarana, and some drugs.

A 6 oz cup of:

Percolated coffee has about 120 mg of caffeine.
Black tea has about 70 mg of caffeine.
Green tea about 35 mg of caffeine.
Leading colas 45 mg of caffeine.
Mountain dew 54 mg of caffeine.
Brewed decaf has 5 mg of caffeine.
Milk chocolate has 6 mg per ounce.
Baking chocolate has 35 mg per ounce.


An estimated 149.2 million 60 kg bags of coffee were consumed worldwide in the calendar year of
2014. With a 2.3% Average annual growth rate in global coffee consumption since 2011 (source:
ICO). With 19,694,400,000 lbs. of coffee beans consumed in 2014 some body must know how
closely related to cocaine it is, and some body is making a fortune off of it.




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    sfs_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn decarboxylate_cannabis(){
    clear_screen();
println!("
Decarboxylate Cannabis




Ingredients


Cannabis Flowers




Directions


Preheat oven to 240• F (116•C)

Roughly grind whatever cannabis you have on hand. A gram or an ounce, any
amount can be decarbed at the same time, provided it fits in your oven.

Place the roughly ground up cannabis in a pyrex baking dish. Metal sheet pans
also work. However, the higher sides of the baking dish make it easier to manage.

Cover with tin foil.

Place in the preheated oven, and bake for 60 minutes. If you had exceptionally
moist weed beforehand, you could extend the baking time.

Remove the tinfoil, and your cannabis is now fully transformed into a potent
and medicinally activated product.





=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn cannabis_canola_oil(){
    clear_screen();
println!("
Cannabis Canola Oil




Ingredients


    Cannabis Flowers

    Canola Oil




Directions


Decarboxylate your weed by spreading your cannabis evenly on an oven tray And coat
with aluminum foil. Place in the oven on 240F/115C for 45 minutes, and then let it cool.

Grind your cannabis or chop it fine

Add cannabis to a glass bowl and add 1 cup of canola oil.

Create a double boiler by placing the bowl on top of a pot with water on medium heat.

Stir occasionally and keep it warm but not boiling for about 2 hours.

Strain through Cheesecloth & let it cool.


Notes

14 g Cannabis / 237 ml Canola Oil OR 0.5 Oz Cannabis / 1 cp Coconut Oil



=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn cooking_with_pot(){
    clear_screen();
println!("
Cooking with Pot


     Many people after cleaning their grass throw away the seeds,
stems, and twigs.  I would highly recommend that you save these, as
there are many recipes for these odds and ends.  A tasty hot drink
that resembles tea can be made very simply by tying up all the
waste from your stash into a muslin ball or a piece of cheesecloth.

Use the quantity you have on hand, as the quantity will determine
the strength and potency. Now, drop the cheesecloth containing the
grass into a kettle of water, and bring the water to a boil.  Allow
the kettle to boil for a few minutes, and then remove it from the
flame and let it steep for another five minutes with the grass
still inside.  After this, the drink is ready.  Just add sugar and
lemon to taste.

     If you decide against growing pot, and want to eat your seeds,
there is an interesting recipe for “seed pancakes”.  It is prepared
by lightly toasting a quarter cup of seeds in a large frying pan.
Now, take the seeds from the frying pan and add them to a mixture
of one cup of pancake mix, one egg, a quarter cup of milk, and one
tablespoon of butter.  Beat this mixture until it is smooth and
creamy.  Heat a frying pan with a small amount of butter, then pour
in pancake batter.  Turn the pancakes as they start to look done,
or when the edges begin to turn brown.  Repeat procedure until all
batter is used.  Serve pancakes with butter, maple syrup, and
honey.

     For a stimulating drink (sounds like all the rest of the
cookbooks) place eight ounces milk, a few spoonfuls of sugar, a
tablespoon of malted milk, half a banana, and half a tablespoon
grass, and three betel muts in a blender.  Keep the blender working
full speed for a few minutes, then strain and serve.

     If you like candy, it's very simple to make some using pot.
Take a quarter cup of powdered grass and add water until it equals
a full cup.  Mix this with four cups sugar and two and a half cups
corn syrup.  Now heat in a large pot to 310 degrees, and add red
food coloring and mint flavoring.  Remove the pot from the stove,
and allow the mixture to cool a little, before pouring it onto wax
paper.  When the candy's cool, cut it into squares and eat.

     One of the most common recipes for cooking with pot is
spaghetti.  This recipe doesn't take too much special preparation:
Just when you add your oregane, add at the same time a quarter cup
grass, and allow it to simmer with the sauce.  Be sure to use well
cleaned grass, unless you can get into eating twigs and stems.
Another way of serving pot with spaghetti is to grind it up very
fine and mix it with some ground cheese.  Then sprinkle the cheese-
pot mixture over the sauce just before eating.

     Dessert is probably the most important stage of the meal,
since it will be the last thing your guests remember before they
pass out all over your table.  For an interesting dessert, grind a
quarter ounce of grass very finely, and add enough water so it
forms a paste.  Now separately dissolve one and a half cups sugar
into two cups milk.  Add to this your pot paste and one lemon rind
grated.  Beat in a half cup heavy cream, until the mixture is firm
and thick.  Now pour the mixture into ice cube trays and freeze.
Just before you're ready to serve, rebeat the frozen mush until it
becomes light and fluffy.

     The following are some additional recipes for cooking with
pot:

==================================================================
                         Acapulco Green

3 ripe avocados                    3 tablespoons wine vinegar
1/2 cup chopped onions             2 teaspoons chili powder
1/2 cup chopped grass

Mix the vinegar, grass, and chili powder together and let the
mixture stand for one hour.  Then add avocados and onions and mash
all otgether.  It can be served with tacos or as a dip.

==================================================================
                            Pot Soup

1 can condensed beef broth         1/2 can water
3 tablespoons grass                3 tablespoons chopped watercress
3 tablespoons lemon juice

Combine all ingredients in a saucepan and bring to a boil over
medium heat.  Place in refrigerator for two to three hours, reheat,
and serve.

==================================================================
                     Pork and Beans and Pot

1 can (1lb, 13 oz) can             1/2 cup light molasses
  pork and beans                   1/2 teaspoon hickory salt
1/2 half cup grass                 3 pineapple rings
4 slices bacon

Mix together in a casserole, cover top with pineapple and bacon,
bake at 350 degrees for about 45 minutes.  Serves about six.

==================================================================
                          The Meat Ball

1 lb. hamburger                    1/4 cup bread crumbs
1/4 cup chopped onions             3 tablespoons grass
1 can cream of mushroom soup       3 tablespoons India relish

Mix it all up and shape into meatballs.  Brown in frying pan and
drain.  Place in a casserole with soup and 1/2 cup water, cover and
cook over low heat for about thrity minutes.  Feeds about 4 people.

==================================================================
                         Spaghetti Sauce

1 can (6 oz.) tomato paste         1 can (6 oz.) water
2 tablespoons olive oil            1/2 clove minced garlic
1/2 cup chopped onions             1 bay leaf
1/2 cup chopped grass              1 pinch thyme
1 pinch pepper                     1/2 teaspoon salt

Mix in a large pot, cover and simmer with frequent stirring for two
hours.  Serve over spaghetti.

==================================================================
                            Pot Loaf

1 packet onion soup mix            2 lbs. ground beef
1 (16 oz) can whole peeled         1 egg
  tomatoes                         4 slices bread, crumbed
1/2 cup chopped grass

Mix all ingredients and shape into a loaf.  Bake for one hour in
400-degree oven.  Serves about six.

==================================================================
                         Chili Bean Pot

2 lbs. pinto beans                 1/2 clove garlic
1 lb. bacon, cut into two inch     1 cup chopped grass
  sections                         1/2 cup mushrooms
2 cups read wine                   4 tablespoons chili powder

Soak beans overnight in water.  In a large pot, pour boiling water
over beans and simmer for at least an hour, adding more water to
keep beans covered.  Now add all other ingredients and conitnue to
simmer for another three hours.  Salt to taste.  Serves about ten.

==================================================================
                          Bird Stuffing

5 cups rye bread crumbs            1/3 cup chopped onions
2 tablespoons poultry seasoning    3 tablespoons melted butter
1/2 cup each raisins and almonds   1/2 cup chopped grass
1/2 cup celery                     2 tablespoons red wine

Mix it all together, then stuff it in.

==================================================================
                            Apple Pot

4 apples (cored)                   4 cherries
1/2 cup brown sugar                1/3 cup chopped grass
1/4 cup water                      2 tablespoons cinnamon

Powder the grass in a blender, then mix grass with sugar water.
Stuff apple cores with this paste.  Sprinkle apples with cinnamon,
and top with a cherry.  Bake for 25 minutes at 350 degrees.

==================================================================
                          Pot Brownies

1/2 cup flour                      1 egg (beaten)
3 tablespoons shortening           1 tablespoon water
2 tablespoons honey                1/2 cup grass
1 pinch salt                       1 square melted chocolate
1/4 teaspoon baking powder         1 teaspoon vanilla
1/2 cup sugar                      1/2 cup chopped nuts
2 tablespoons corn syrup

Sift flour, baking powder, and salt together.  Mix shortening,
sugar, honey, syrup and egg.  Then blend in chocolate and other
ingredients, mix well.  Spread in an eight-inch pan and bake for 20
minutes at 350 degrees.  If you are too lazy to cook, just add the
grass to any ready to make brownie mix.

==================================================================
                          Banana Bread

1/2 cup shortening                 1 cup mashed bananas
2 eggs                             2 cups sifted flour
1 teaspoon lemon juice             1/2 cup chopped grass
3 teaspoons baking powder          1/2 teaspoon salt
1 cup sugar                        1 cup chopped nuts

Mix the shortening and sugar, beat eggs, and add to mixture.
Separately mix bananas with lemon juice and add to the first
mixture.  Sift flour, salt, and baking powder together, then mix
all ingredients together.  Bake for 1 1/4 hours at 375 degrees.

==================================================================
                       Sesame Seed Cookies

3 oz. ground roast sesame seeds    1/4 cup honey
3 tablespoons ground almonds       1/2 teaspoon ground ginger
1/4 teaspoon nutmeg                1/4 teaspoon cinnamon
1/4 oz. grass

Toast the grass until slighly brown and then crush it in a mortar.
Mix crushed grass with all other ingredients, in a skillet.  Place
skillet over low flame and add 1 tablespoon of salt butter.  Allow
it to cook.  When cool, roll mixture into little balls and dip them
into the sesame seeds.

     If you happen to in the country at a place where pot is being
grown, here's one of the greates recipes you can try.  Pick a
medium-sized leaf off the marihuana plant and dip it into a cup of
drawn butter, add salt and eat.

==================================================================
I hope you enjoy these recipes which were taken from The Anarchist
Cookbook, by William Powell and no longer available at finer
bookstores anywhere.

Bon appetit!
Absalom


X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
 Another file downloaded from:                     The NIRVANAnet(tm) Seven

 & the Temple of the Screaming Electron   Taipan Enigma        510/935-5845
 Burn This Flag                           Zardoz               408/363-9766
 realitycheck                             Poindexter Fortran   510/527-1662
 Lies Unlimited                           Mick Freen           801/278-2699
 The New Dork Sublime                     Biffnix              415/864-DORK
 The Shrine                               Rif Raf              206/794-6674
 Planet Mirth                             Simon Jester         510/786-6560

                          “Raw Data for Raw Nerves”
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn cannabis_butter(){
    clear_screen();
println!("
Cannabis Butter




Ingredients


    1/2 oz or 14 grams Cannabis Flowers

    1 Cup Butter (unsalted or salted)




Directions


Decarboxylate your weed by spreading your cannabis evenly on an
oven tray And coat with aluminum foil.

Place in the oven on 240F/115C for 45 minutes, and then let it cool.

Grind your dry cannabis or chop it till fine.

Place the cannabis in a glass bowl. Create a double boiler by having
a pot of water on the stove on medium to high heat. And placing the
glass bowl with cannabis on it

Add butter and let it simmer for 60 minutes minimum, 3 hours maximum Stirring often.

Strain your butter trough a cheesecloth.

Cool Down




Notes

1 Whole cup of cannabis butter contains: 1400 MG THC / 1 Tablespoon Contains 87.5 MG THC




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn weed_french_toast(){
    clear_screen();
println!("
Weed French Toast




Equipment & Ingredients




    Equipment


9x13 baking dish
whisk
Measuring cups and spoons
Bowl




    Ingredients


1 whole French baguette
4 tbsp cannabutter
1 cup milk
1/4 cup sugar
3 tbsp maple syrup
1/2 tsp salt
powdered sugar




    Directions


Lightly butter the baking dish. Cut the baguette crosswise at an angle to make 8 pieces. Each slice
should be at least ¾ inches thick. Keep aside.

In a small bowl cream the two kinds of butters till they are thoroughly combined. Now spread this
butter on one side of each of the 8 slices. Now arrange these 8 slices, butter side up, into the greased
baking dish.

In a separate mixing bowl add the eggs, milk, maple syrup, sugar, vanilla and salt. Whisk the
ingredients till they blend. Pour this mixture over the bread and allow the slices to soak it up by
pressing it down on it with a spoon. Cover the dish and refrigerate for a few hours.

Preheat the oven to 350°F. Once heated place the baking tray in the oven and bake for 45 minutes or
until the tops turn golden brown. Dust them with powdered sugar and enjoy the power.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn cannabis_pancakes(){
    clear_screen();
println!("
Cannabis Pancakes




Equipment & Ingredients

Equipment
    1 mixing bowl
    1 whisk
    measuring cups & spoons
    1 non-stick frying pan


Ingredients

    2 tbsp cannabis butter
    1 & 1/4 cup all purpose flour
    1 cup milk
    1 pinch salt
    2 large eggs




Directions

Combine all ingredients into a bowl and whisk until there are no more lumps
and the mixture is a smooth batter. Set a frying pan over a medium heat then
carefully wipe it with an oiled kitchen paper tissue.

Once the pan is hot and oiled, pour in your desired amount of pancake batter.
Cook for 1 minute on each side, or until golden. Serve immediately or keep
warm in the oven on low heat until the rest are finished cooking.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn weed_spaghetti_bolognese(){
    clear_screen();
println!("
Weed Spaghetti Bolognese




Equipment & Ingredients

Equipment

    2 Large saucepan
    skillet
    Measuring cups and spoons
    Cutting board & sharp knife
    strainer for pasta

Ingredients

    1 1/2 tbsp cannabis oil
    2 cloves garlic minced
    1 onion finely chopped
    1 lb ground beef
    1/2 cup red wine dry
    2 beef bouillion cubes
    28 oz can of crushed tomatoes
    2 tbsp tomato paste
    2 tsp white sugar
    2 tsp Worcestershire sauce
    2 dried bay leaves
    2 sprigs thyme
    13 oz package spaghetti




Directions

Heat cannabis oil in a large pot or deep skillet over medium high heat. Add onion
and garlic, cook for 3 minutes or until light golden and softened.

Turn heat up to high and add beef. Cook, breaking it up as your go, until browned.

Add red wine. Bring to simmer and cook for 1 minute, scraping the bottom of the
pot, until the alcohol smell is gone.

Add remaining ingredients except salt and pepper. Stir, bring to a simmer then turn
down to medium so it bubbles gently. Cook for 20 – 30 minutes (no lid), adding
water if the sauce gets too thick for your taste. Stir occasionally.

Cook pasta according to package directions, strain and either mix in together
with sauce, OR scoop pasta on plate, and add as much or as little bolognese
sauce as you like.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn one_pot_taco_weed_spaghetti(){
    clear_screen();
println!("
One Pot Taco Weed Spaghetti




Equipment & Ingredients

Equipment
    1 strainer
    1 large saucepan or pot to boil pasta
    1 cheese grater
    1 sharp knife
    1 cutting board

Ingredients
    1 roma tomato
    1 lb ground beef
    2 cups pasta (spaghetti)
    1 pkg storebought taco seasoning
    1 small can of cream of chicken soup
    2 cups grated cheddar cheese
    1/3 cup cilantro more or less if you like
    1 onion chopped finely
    3-4 tbsp cannabutter



Directions

Fill a pot with water abd bring to a boil on high.

In the meantime, chop up all veggies finely. Set aside

Pour pasta into boiling water with a pinch of salt. Let
cook for 10 minutes approx. Follow package directions.

Add cannabutter to a frying pan on medium -high and once
pan is hot, add ground beef and onions. Stir often and
once meat is browned, add onions and tomatoes. Add taco
seasoning package. Cook for a few minutes more.

Strain pasta and add it back into the same pot, add in
meat mixture and mix well.

Serve immediately with loads of cheese and cilantto.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn weed_brownies(){
    clear_screen();
println!("
Weed Brownies




    Ingredients


1/2 cup cannabutter

1 cup granulated sugar

1/3 cup natural cocoa powder

3/4 cup all-purpose flour

1/2 cup chopped caramel or white/milk/dark chocolate, or even a mix if you’re feeling
adventurous

Two large eggs

One teaspoon of vanilla extract

1/4 teaspoon kosher salt

1/2 teaspoon baking powder

Ingredients - Chocolate Ganache Glaze

3 ounces semisweet chocolate

1/4 cup heavy cream

1/2 cup chopped pecans




    Directions


Preheat the oven to 350 .℉

Grease or line an 8×8 inch pan with baking paper or parchment paper.

In a medium saucepan over low heat, melt the cannabutter.

When the butter has melted, remove the saucepan from the heat.

Pour the butter into a mixing bowl

Add the eggs, vanilla, and sugar. Mix until smooth.

Sift the flour, cocoa, salt, and baking powder into the butter mixture.

With a spatula, gently fold in the dry ingredients until everything is incorporated.

Pour the brownie mixture into the pan. Spread evenly.

Bake the brownies for 30 minutes. Check that the brownies are entirely baked through by sticking a
toothpick in the center; the brownies are done if it comes out clean.

Let the brownies cool.

Place the chopped semisweet chocolate in a bowl.

In a small saucepan, pour the heavy cream.

Bring the cream to a boil and pour in the semisweet chocolate. Stir until the chocolate has melted.
The mixture should be smooth when you remove it from the heat.

Spread the glaze over the brownies and sprinkle the chopped nuts over the glaze.

Cut the cooled brownies into nine squares. Serve and enjoy the gooey, chocolatey weed brownies
warm, or let them cool completely.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn quick_beef_weed_stir_fry(){
    clear_screen();
println!("
Quick Beef & Weed Stir Fry




Equipment & Ingredients

Equipment

    Wok or skillet
    wooden spoon
    sharp knife
    cutting board

Ingredients

    2 tbsp cannabis oil
    1 lb beef sirloin cut into 1-2 inch strips
    1 1/2 cups fresh broccoli
    1 red bell pepper cut into thin match sticks
    2 carrots thinly sliced
    1 green onion chopped finely
    1 tsp minced garlic
    2 tbsp soy sauce
    2 tbsp toasted sesame seeds




Directions

Heat cannabis oil in a large wok or skillet over medium-high heat; cook
and stir beef until browned, 3 to 4 minutes. Move beef to the side of
the wok and add broccoli, bell pepper, carrots, green onion, and garlic
to the center of the wok. Cook and stir vegetables for 2 minutes.

Stir beef into vegetables and season with soy sauce and sesame seeds.
Continue to cook and stir until vegetables are tender, about 2 more minutes.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn sausage_stuffed_peppers_with_weed(){
    clear_screen();
println!("
Sausage Stuffed Peppers With Weed




Equipment & Ingredients




    Equipment


sharp knife
large mixing bowl
Large baking sheet or large oven-safe deep dish big enough for 6 bell peppers.
parchment paper
Large skillet




    Ingredients


6 Bell Peppers
1 tbsp olive oil
4 tbsp cannabis butter
1/2 cup onion finely chopped
1 lb sausage
1 cup mushrooms
1 tbsp fresh oregano
1 tbso fresh parsley
1/2 cup chicken broth
2 cups shredded cheese
1 1/2 cups brown or white rice




    Directions


Prep the peppers: Slice 1/4 inch off the top of the peppers like removing a lid. Remove the core and
seeds as best you can.Line peppers cut side up in a greased baking pan large enough to fit all 6.
Preheat oven to 375°F

Heat olive oil and onion in a large skillet over medium heat. Cook for 4 minutes until onions are
soft, then stir in the sausage, mushrooms, garlic, oregano, parsley, basil, salt, and pepper. Now add
the cannabutter.

Cook and break up the sausage with your spoon/spatula as best you can until the sausage is almost
fully cooked through, for about 6 minutes.While it's cooking, you can get the rice started.

Stir in the chicken broth to the skillet with sausage. Stir in 1 cup of shredded cheese and all of the
cooked rice. Spoon filling into each pepper, filling all the way to the top. Place on your
cookie/baking sheet or large oven-safe dish so they stand up and don't fall over.

You can pour a little of water on the bottom of the pan, around the peppers, OR in an oven-proof
dish next to the peppers so they stay moist. If you choose the latter, you can fill it up as full as you
like.

Bake, covered, for 35 minutes or until the peppers are nearly fork tender. Remove from the oven,
and sprinkle the rest of the cheese on top of each pepper. Return to the oven, uncovered, and bake
for 5 more minutes.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn cannabis_winter_soup(){
    clear_screen();
println!("
Cannabis Winter Soup




Equipment & Ingredients

Equipment

    Soup pot
    Measuring cups and spoons
    sharp knife
    cutting board
    hand mixer

Ingredients

    1 tbsp regular butter
    2 tbsp cannabis butter
    1 zuchinni
    2 potatoes
    3 cups chicken stock
    1/2 cup milk or heavy cream
    2 tbsp oil
    1 dash salt & pepper each




Directions

First, melt the butter and set aside. Next, boil the potatoes and the
zucchini. Once boiled, mix with a blender or electric whisk until it’s
creamy and smooth. Then, add chicken stock to the zucchini and potatoes,
as well as the milk or cream and melted butter – mix well. Lastly, once
the resulting mixture has a thick, smooth texture, add in two spoonfuls
of oil, salt and pepper.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn christmas_cannabis_cookies(){
    clear_screen();
println!("
Christmas Cannabis Cookies




Equipment & Ingredients




    Equipment

mixing bowls
Cookie sheets
Wire rack
Non stick cooking spray




    Ingredients

2 cups red and green christmas cookies M&M's are great
1 1/2 cups graham crackers
1 1/2 cups shredded coconut
3/4 cup all-purpose flour
1/2 cup cannabutter softened
1 can sweetened condensed milk 14 oz
2 tsp baking powder




Directions

In a medium mixing bowl, combine graham cracker crumbs, all-purpose flour and baking powder.

In a separate large mixing bowl, combine condensed milk and softened cannabis butter; beat until
smooth. Stir in graham cracker mixture, mixing thoroughly. Stir in coconut flakes.

Refrigerate and chill dough for 45 minutes until it hardens. Then mix in the chocolates.

Preheat the oven to 375 degrees Fahrenheit (190 degrees Celsius).

Drop dough by rounded teaspoonfuls onto greased cookie sheets. Leave at least 2-inches of space in
between each cookie.

Bake in the preheated oven for 7 to 10 minutes, or until lightly browned. Allow cookies to cool on
cookie sheets for 5 minutes before removing to a wire rack to cool completely.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn the_denver_weed_omelette(){
    clear_screen();
println!("
The Denver Weed Omelette




Equipment & Ingredients

Equipment

    mixing bowl
    measuring cups & spoons
    whisk
    non stick pan

Ingredients

    3 large eggs
    1 tbsp cannabis butter or cannabis oil
    1/4 cup diced smoked ham
    2 tbsp diced onion
    2 tbsp diced green or red peppers
    1 dash salt & pepper each
    1/3 cup shredded cheddar cheese




Directions

Beat eggs in a small bowl until just combined; do not overbeat.

Melt cannabis butter in a skillet over medium-high heat. Add ham, onion,
and bell pepper; season with salt and pepper. Cook and stir until onions
soften and ham begins to caramelize, about 5 minutes.

Reduce heat to medium-low and pour in eggs. Mix briefly with a spatula
while shaking the pan to ensure ingredients are evenly distributed.
Quickly run the spatula along edges of omelet. Sprinkle Cheddar cheese
and cayenne pepper over omelet. Cayenne optional.

Cook, shaking the pan occasionally, until top is still wet but not
runny, about 5 minutes. Use a spatula to fold omelet in half and
transfer it to a plate.




=================FROM INTERNAHIONAL HIGHLIFE=====================

⠀⠀⠀⠀https://internationalhighlife.com/

⠀⠀⠀⠀https://www.youtube.com/channel/UC20Z339VAGN-nm-R9p8JtkA

⠀⠀⠀⠀https://www.instagram.com/international_highlife/

     https://www.facebook.com/internationalhighlife

⠀⠀⠀⠀https://twitter.com/intl_highlife⠀⠀⠀⠀⠀⠀⠀

=================================================================




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    weed_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn bloodninja_j_gurli13(){
    clear_screen();
println!("
Bloodninja j_gurli13




    Bloodninja: Ok baby, we got to hurry, I don't know how long I can keep it ready for you.

j_gurli13: thats ok. ok i'm a japanese schoolgirl, what r u.

    Bloodninja: A Rhinocerus. Well, hung like one, thats for sure.

j_gurli13: haha, ok lets go.

j_gurli13: i put my hand through ur hair, and kiss u on the neck.

    Bloodninja: I stomp the ground, and snort, to alert you that you are in my breeding territory.

j_gurli13: haha, ok, u know that turns me on.

j_gurli13: i start unbuttoning ur shirt.

    Bloodninja: Rhinoceruses don't wear shirts.

j_gurli13: No, ur not really a Rhinocerus silly, it's just part of the game.

    Bloodninja: Rhinoceruses don't play games. They fucking charge your ass.

j_gurli13: stop, cmon be serious.

    Bloodninja: It doesn't get any more serious than a Rhinocerus about to charge your ass.

    Bloodninja: I stomp my feet, the dust stirs around my tough skinned feet.

j_gurli13: thats it.

    Bloodninja: Nostrils flaring, I lower my head. My horn, like some phallic symbol of
                my potent virility, is the last thing you see as skulls collide and mine
                remains the victor. You are now a bloody red ragdoll suspended in the
                air on my mighty horn.

    Bloodninja: Fuck am I hard now.



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn bloodninja_sarah19fca(){
    clear_screen();
println!("
Bloodninja Sarah19fca




    Bloodninja: I lick your earlobe, and undo your watch.

Sarah19fca: mmmm, okay.

    Bloodninja: I take yo pants off, grunting like a troll.

Sarah19fca: Yeah I like it rough.

    Bloodninja: I smack you thick booty.

Sarah19fca: Oh yeah, that feels good.

    Bloodninja: Smack, Smack, yeeeaahhh.

    Bloodninja: I make some toast and eat it off your
                ass. Land O' Lakes butter all in your crack. Mmmm.

Sarah19fca: you like that?

    Bloodninja: I peel some bananas.

Sarah19fca: Oh, what are you gonna do with those?

    Bloodninja: get me peanuts. Peanuts from the ballpark.

Sarah19fca: Peanuts?

    Bloodninja: Ken Griffey Jr. Yeaaaaahhh.

Sarah19fca: What are you talking about?

    Bloodninja: I'm spent, I jump down into the alley and
                smoke a fatty. I throw rocks at the cats.

Sarah19fca: This is stupid.

    Bloodninja: Stone Cold Steve Austin gives me some beer.

    Bloodninja: Wanna Wrestle Stone Cold?

    Bloodninja: Yeeaahhhh.

Sarah19fca: /ignore

    Bloodninja: Its cool stone cold she was a bitch anyway.

    Bloodninja: We get on harleys and ride into the sunset.



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn bloodninja_mommymelissa(){
    clear_screen();
println!("
Bloodninja MommyMelissa




    Bloodninja: Wanna cyber?

MommyMelissa: Sure, you into vegetables?

    Bloodninja: What like gardening an shit?

MommyMelissa: Yeah, something like that.

    Bloodninja: Nuthin turns me on more, check this out

    Bloodninja: You bend over to harvest your radishes.

(pause)

MommyMelissa: is that it?

    Bloodninja: You water your tomato patch.

    Bloodninja: Are you ready for my fresh produce?

MommyMelissa: I was thinking of like, sexual acts INVOLVING
              vegetables... Can you make it a little more sexy for me?

(pause)

    Bloodninja: I touch you on your lettuce, you massage
                my spinach... Sexily.

    Bloodninja: I ride your buttocks, like they were
                amber waves of grains.

MommyMelissa: Grain doesn't really turn me on... I was thinking
              more along the lines of carrots and zucchinis.

    Bloodninja: my zucchinis carresses your carrots.

    Bloodninja: Damn baby your right, this shit is HOT.

MommyMelissa: ...

    Bloodninja: My turnips listen for the soft cry of your
                love. My insides turn to celery as I unleash
                my warm and sticky cauliflower of love.

MommyMelissa: What the fuck is this madlibs? I'm outta here.

    Bloodninja: Yah, well I already unleashed my
                cauliflower, all over your olives, and up
                in your eyes. Now you can't see. Bitch.

MommyMelissa: whatever.



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn bloodninja_britneyspears14(){
    clear_screen();
println!("
Bloodninja BritneySpears14




    Bloodninja: Baby, I been havin a tough night so treat me nice aight?

BritneySpears14: Aight.

    Bloodninja: Slip out of those pants baby, yeah.

BritneySpears14: I slip out of my pants, just for you, Bloodninja.

    Bloodninja: Oh yeah, aight. Aight, I put on my robe and wizard hat.

BritneySpears14: Oh, I like to play dress up.

    Bloodninja: Me too baby.

BritneySpears14: I kiss you softly on your chest.

    Bloodninja: I cast Lvl 3 Eroticism. You turn into a real beautiful woman.

BritneySpears14: Hey...

    Bloodninja: I meditate to regain my mana, before casting Lvl 8 Penis of the Infinite.

BritneySpears14: Funny I still don't see it.

    Bloodninja: I spend my mana reserves to cast Mighty of the Beyondness.

BritneySpears14: You are the worst cyber partner ever. This is ridiculous.

    Bloodninja: Don't fuck with me biznitch, I'm the mightiest sorcerer of the lands.

    Bloodninja: I steal yo soul and cast Lightning Lvl 1,000,000 Your body explodes
                into a fine bloody mist, because you are only a Lvl 2 Druid.

BritneySpears14: Don't ever message me again you piece.

    Bloodninja: Robots are trying to drill my brain but my lightning shield
                inflicts DOA attack, leaving the robots as flaming piles of metal.

    Bloodninja: King Arthur congratulates me for destroying Dr. Robotnik's evil army
                of Robot Socialist Republics. The cold war ends. Reagan steals my
                accomplishments and makes like it was cause of him.

    Bloodninja: You still there baby? I think it's getting hard now.

    Bloodninja: Baby?



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn jdogg_qt_pie(){
    clear_screen();
println!("
Jdogg QT-Pie


    Jdogg: Hey

QT-Pie: Hey

    Jdogg: whats goin on

QT-Pie: Nothing. Who are you?

    Jdogg: Jdogg. Wanna cyber?

QT-Pie: what does that mean?

    Jdogg: what are you wearing?

QT-Pie: T-shirt. Jeans.

    Jdogg: Garter belt?

QT-Pie: Ummm...no.

    Jdogg: Are we gonna cyber or not?

QT-Pie: uh, okay.

    Jdogg: Sweet, I start by rubbing your ass all around. You love this.

    Jdogg: You're wet already. I can smell your pussy stink from here.

QT-Pie: WHAT?!

    Jdogg: I execute standing position 12 from the Kama Sutra. Passion
           fills the room. Your head is close to the ceiling fan.

    Jdogg: You leave everything to Jdogg.

    Jdogg: I am completely inside of you. You are my dick puppet.
           I put on a little play.

QT-Pie: This is weird. I should go.

    Jdogg: I drop you on the ground, and lay a stripe down your back.

QT-Pie: A stripe?

    Jdogg: I need a sandwich.

QT-Pie: You're a freak.

    Jdogg: I was great. You loved it.




╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn sweet17_bloodninja(){
    clear_screen();
println!("
sweet17 Bloodninja




sweet17: Hi

    bloodninja: hello

    bloodninja: who is this?

sweet17: just a someone?

    bloodninja: A someone I know?

sweet17: nope

    bloodninja: Then why the hell are you bothering me?

sweet17: well sorrrrrry

sweet17: I just wanted to chat with you

    bloodninja: why?

sweet17: nevermind your an jerk

    bloodninja: Hey wait a minute

sweet17: yes?

    bloodninja: look I’m sorry. I’m just a little paranoid

sweet17: paranoid?

    bloodninja: yes

sweet17: of what?

sweet17: me?

    bloodninja: No. I’m in hiding.

sweet17: LOL

    bloodninja: Don’t fucking laugh at me!

    bloodninja: This shit is serious!

sweet17: What are you hiding from?

    bloodninja: The cops.

sweet17: gimme a fucking break

    bloodninja: I’m serious.

sweet17: I don’t get it

    bloodninja: The cops are after me.

sweet17: For what?

    bloodninja: I’m wanted in three states

sweet17: For???

    bloodninja: It’s kindof embarrasing.

    bloodninja: I had sex with a turkey.

    bloodninja: Hello?

sweet17: You are fucking sick.

    bloodninja: Send me your picture.

sweet17: why?

    bloodninja: so I know you aren’t one of them.

sweet17: One of what?

    bloodninja: The cops.

sweet17: I’m not a cop i told you

    bloodninja: Then send me your picture.

sweet17: hold on

    bloodninja: Hurry up.

    bloodninja: Are you there?

    bloodninja: fuck you, cop!

sweet17: Hey sorry

sweet17: I had to do something for my mom.

    bloodninja: I thought you were trying to find a picture to send to me.

    bloodninja: When really you were notifying the authorities.

    bloodninja: Weren’t you!?

sweet17: thats not it

    bloodninja: Then what?

sweet17: I don’t want to send you the picture cause I’m not pretty

    bloodninja: Most cops aren’t

sweet17: IM NOT A FUCKING COP YOU DICKSHIT!

    bloodninja: Then send me the picture.

sweet17: fine. What’s your e-mail?

    bloodninja: Just send it through here.

sweet17: alright *PIC*

sweet17: Did you get it?

    bloodninja: Hold on. I’m looking.

sweet17: That was me back in may

sweet17: I’ve lost weight since then.

    bloodninja: I hope so

sweet17: what?!?

sweet17: that hurt my feelings.

    bloodninja: Did it?

sweet17: Yes. I’m not that much smaller than that now.

    bloodninja: Will it make you feel better if I send you my picture?

sweet17: yes

    bloodninja: Alright let me find it.

sweet17: kks

    bloodninja: Okay here it is. *PIC*

sweet17: this isn’t you.

    bloodninja: I’ll be damned if it ain’t!

sweet17: You don’t look like that.

    bloodninja: How the hell do you know?

sweet17: cause your profile has another picture.

    bloodninja: The profile pic is a fake.

    bloodninja: I use it to hide from the cops.

sweet17: You look like the Farm Fresh guy lol

    bloodninja: Well, you look like you ATE the Farm Fresh guy….

    bloodninja: Not to mention all the groceries.

sweet17: Go fuck yourself

    bloodninja: I was going to until I saw that picture

    bloodninja: Now my unit won’t get hard for a week.

sweet17: I shouldn’t have sent you that picture.

sweet17: You’ve done nothing but slam me.

sweet17: you hurt me.

    bloodninja: And calling me the Farm Fresh guy doesn’t hurt me?

sweet17: I thought you were bullcrapping me!

    bloodninja: Why would I do that?

sweet17: I can’t believe that cops are after you

    bloodninja: I can’t believe Santa lets you sit on his lap..

sweet17: FUCK YOU!!!

    bloodninja: You’d break both of his legs.

sweet17: You’re a fucking wanker!

sweet17: I’ve been teased my whole life because of my weight

sweet17: and you make fun of me when you don’t even know me

    bloodninja: Ok. I’m sorry.

sweet17: No you aren’t

    bloodninja: You’re right. I’m not.

bloodninja: HAARRRRR!

sweet17: I’m done with you

    bloodninja: Aww. I’m sorry.

sweet17: I’m putting you on ignore

    bloodninja: Wait a sec

    bloodninja: We got off on the wrong foot.

    bloodninja: Wanna start over?

sweet17: No

    bloodninja: I’ll eat your kitty

sweet17: You’ll what?

    bloodninja: You heard me.

    bloodninja: I said I’d eat your kitty.

sweet17: I thought you said you couldn’t get it hard after seeing my picture

    bloodninja: Do I need a hard-on to eat your kitty?

sweet17: I’d like to know that the man eating me out is excited yes

    bloodninja: Well I’m not like most men.

    bloodninja: I get excited in different ways.

sweet17: Like what?

    bloodninja: Do you really wanna know?

sweet17: I don’t know

    bloodninja: You have to tell me yes or no.

sweet17: I’m afraid to

    bloodninja: Why?

sweet17: cause

    bloodninja: cause why?

sweet17: well lets see

sweet17: you say you have sex with turkeys. You call me fat. then you wanna eat me out

sweet17: doesn’t that seem strange to you?

    bloodninja: Nope

sweet17: well its strange to me

    bloodninja: Fine. I won’t do it if you don’t want me to

sweet17: I didn’t say that

    bloodninja: So is that a yes?

sweet17: I guess so.

    bloodninja: Ok. I need your help getting excited though.

    bloodninja: Are you willing?

sweet17: What do you need me to do?

    bloodninja: I need you talk like a pirate.

sweet17: ???

    bloodninja: When I start to go limp… you say “HARRRR!!!”

    bloodninja: ok?

    bloodninja: Hello?

sweet17: You can’t be serious

    bloodninja: Oh yes I am!

    bloodninja: It’s my fantasy.

sweet17: this is retarded

    bloodninja: Do you want it or not?

sweet17: Yes I want it.

    bloodninja: Then you’ll do it for me?

sweet17: sure

    bloodninja: Ok. Here we go.

    bloodninja: I gently remove your panties and being to massage your thighs.

    bloodninja: You get really juicy thinking about my tounge brushing up against them

    bloodninja: I softly begin to tounge your wet kitty.

    bloodninja: I run my tounge up and down your smooth cunt.

sweet17: mmmm yeah

    bloodninja: uh oh …going limp.

sweet17: Har

    bloodninja: You gotta do better than that!

    bloodninja: Your picture was really bad.

sweet17: HARRRRRRRRRRRR

    bloodninja: Ahhhh. Much better. I feel your kitty get more moist with every stroke.

    bloodninja: I softly suck on your clit bringing it in and out of my mouth.

    bloodninja: Your juices run down my chin as your scent makes its way to my nose.

    bloodninja: I begin to feel empowered by your femininity.

sweet17: mmmmmm you are good

    bloodninja: I feel your thighs tighten as I fuck harder

    bloodninja: going limp

sweet17: HARRRRRRR

    bloodninja: Mmmm I grab your swelling buttocks in my hands.

    bloodninja: You begin to sway back and forth.

    bloodninja: going limp

sweet17: this is stupid

    bloodninja: …still limp

    bloodninja: Do it!

sweet17: HARRRRRRRRRRRRR

    bloodninja: I turn you around to lick your asshole.

    bloodninja: I pry apart that battleship you call your ass.

    bloodninja: I see poo nuggets hanging from the hair around your ass.

sweet17: WTF?!?!?

    bloodninja: They stink really bad.

sweet17: OMG STOP!!!

    bloodninja: I start to get fed up with your ugly ass

    bloodninja: I tear off your wooden peg leg.

    bloodninja: I ram it up your ass.

sweet17: YOURE A FUCKING PYSCHO!!

    bloodninja: Then I pour hot carmel over your head.

    bloodninja: And turn you into a fucking candy apple…

    bloodninja: I kick you in the face!

sweet17: FUCK YOU DICKHEAD!!

    bloodninja: The celluloid from your cheeks hits the side of the cabin…

    bloodninja: Your parrot flys away.

    bloodninja: …going limp again.

    bloodninja: Hello?

    bloodninja: Say it!

    bloodninja: HAARRRRRR!!!!!



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn bloodninja_dirtykate(){
    clear_screen();
println!("
Bloodninja DirtyKate




    Bloodninja: Wanna cyber?

DirtyKate: K, but don't tell anybody ;-)

DirtyKate: Who are you?

    Bloodninja: I've got blond hair, blue eyes, I work out a lot

    Bloodninja: And I have a part time job delivering for Papa John's in my Geo Storm.

DirtyKate: You sound sexy.. I bet you want me in the back of your car..

    Bloodninja: Maybe some other time. You should call up Papa John's and make an order

DirtyKate: Haha! OK

DirtyKate: Hello! I'd like an extra-EXTRA large pizza just dripping with sauce.

    Bloodninja: Well, first they would say, “Hello, this is Papa John's, how may
                I help you”, then they tell you the specials, and then you would
                make your order. So that's an X-Large. What toppings do you want?

DirtyKate: I want everything, baby!

    Bloodninja: Is this a delivery?

DirtyKate: Umm...Yes

DirtyKate: So you're bringing the pizza to my house now? Cause I'm home alone...
           and I think I'll take a shower...

    Bloodninja: Good. It will take about fifteen minutes to cook, and then I'll
                drive to your house.

**pause**

DirtyKate:I'm almost finished with my shower... Hurry up!

    Bloodninja: You can't hurry good pizza.

    Bloodninja: I'm on my way now though

**pause**

DirtyKate: So you're at my front door now.

    Bloodninja: How did you know?

    Bloodninja: I knock but you can't hear me cause you're in the shower.
                So I let myself in, and walk inside. I put the pizza down on your coffee table.

    Bloodninja: Are you ready to get nasty, baby? I'm as hot as a pizza oven

DirtyKate: ooohh yeah. I step out of the shower and I'm all wet and cold. Warm me up baby

    Bloodninja: So you're still in the bathroom?

DirtyKate: Yeah, I'm wrapping a towel around myself.

    Bloodninja: I can no longer resist the pizza. I open the box and unzip my pants with my other
                hand. As I penetrate the gooey cheese, I moan in ecstacy. The mushrooms and
                Italian sausage are rough, but the sauce is deliciously soothing. I blow my
                load in seconds. As you leave the bathroom, I exit through the front door....

DirtyKate: What the fuck?

DirtyKate: You perverted piece of shit

DirtyKate: Fuck



╔══════════════════════╗
║(B) Go Back           ║
║                      ║
║(M) Main Menu         ║
║                      ║
║(E) Exit              ║
╚══════════════════════╝");
    messages_2();
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn main_menu(){
    clear_screen();

    println!("
                     ▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄
                    ▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
                    ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌
                    ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌
                    ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░▌       ▐░▌
                    ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌
                    ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄█░▌
                    ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░░░░░░░░░░░▌
                    ▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌          ▐░█▄▄▄▄▄▄▄█░▌ ▀▀▀▀▀▀█░█▀▀
                    ▐░░░░░░░░░░▌ ▐░▌       ▐░▌▐░▌          ▐░░░░░░░░░░░▌        ▐░▌
                     ▀▀▀▀▀▀▀▀▀▀   ▀         ▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀          ▀



            Wanna make a car dealer uncomfortable, just say... “Tell me if you can hear this?”
            Then get in the trunk and start screaming.

    ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗
    ║                                                                                               ║
    ║  (1) My son asked why I speak so softly in the house.                                         ║
    ║      I said I was afraid the NSA was listening. He laughed, I laughed, Alexa laughed.         ║
    ║                                                                                               ║
    ║                                                                                               ║
    ║  (2) people tell me I have a unique way of lighting up a room...                              ║
    ║      it's called arson and those people are called witnesses.                                 ║
    ║                                                                                               ║
    ║                                                                                               ║
    ║  (3) Either my cooking has improved or.... My family's immune systems have strengthened.      ║
    ║                                                                                               ║
    ║                                                                                               ║
    ║  (4) Marijuana is bad mkay? it smells really bad. so you shouldn't shouldn't smoke it mkay?   ║
    ║                                                                                               ║
    ║                                                                                               ║
    ║  (5) Constipations.                                                                           ║
    ║                                                                                               ║
    ║                                                                                               ║
    ║  (6) get me out of here.                                                                      ║
    ║                                                                                               ║
    ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝

                            ENTER ANY NUMBER 1 TO 6 FROM THE ABOVE MENU");


let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "1" => sfs(),
    "2" => pyro(),
    "3" => recipes(),
    "4" => weed(),
    "5" => messages(),
    "6" => exit(),
    _ => main(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn messages(){
    clear_screen();
println!("
    __   __  _______  _______  _______  _______  _______  _______  _______
   |  |_|  ||       ||       ||       ||   _   ||       ||       ||       |
   |       ||    ___||  _____||  _____||  |_|  ||    ___||    ___||  _____|
   |       ||   |___ | |_____ | |_____ |       ||   | __ |   |___ | |_____
   |       ||    ___||_____  ||_____  ||       ||   ||  ||    ___||_____  |
   | ||_|| ||   |___  _____| | _____| ||   _   ||   |_| ||   |___  _____| |
   |_|   |_||_______||_______||_______||__| |__||_______||_______||_______|


      There is always a little “go fuck yourself” in every “whatever”

   ╔═════════════════════════════════╗
   ║                                 ║
   ║ (1) Bloodninja BritneySpears14  ║
   ║                                 ║
   ║ (2) Bloodninja DirtyKate        ║
   ║                                 ║
   ║ (3) sweet17 Bloodninja          ║
   ║                                 ║      “No thanks, I'm a vegetarian”
   ║ (4) Bloodninja MommyMelissa     ║    is a fun thing to say when someone
   ║                                 ║       tries to hand you their baby.
   ║ (5) Jdogg QT-Pie                ║
   ║                                 ║
   ║ (6) Bloodninja j_gurli13        ║
   ║                                 ║
   ║ (7) Bloodninja Sarah19fca       ║
   ║                                 ║
   ╚═════════════════════════════════╝

    ENTER ANY NUMBER (1) TO (7) FROM
     THE ABOVE MENU. (E) TO EXIT
      OR (B) BACK TO MAIN MENU");

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "1" => bloodninja_britneyspears14(),
    "2" => bloodninja_dirtykate(),
    "3" => sweet17_bloodninja(),
    "4" => bloodninja_mommymelissa(),
    "5" => jdogg_qt_pie(),
    "6" => bloodninja_j_gurli13(),
    "7" => bloodninja_sarah19fca(),
    "b" => main_menu(),
    "e" => exit(),
    _ => messages(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn messages_2(){

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "b" => messages(),
    "m" => main_menu(),
    "e" => exit(),
    _ => messages(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn pyro(){
    clear_screen();
println!("
      ____  __ __  ____   ___
     |    ╲|  |  ||    ╲ /   ╲
     |  o  )  |  ||  D  )     |
     |   _/|  ~  ||    /|  O  |
     |  |  |___, ||    ╲|     |
     |  |  |     ||  .  ╲     |
     |__|  |____/ |__|╲ _╲ ___/

         __  __ __    ___  _____
        /  ]|  |  |  /  _]|     |
       /  / |  |  | /  [_ |   __|
      /  /  |  _  ||    _]|  |_
     /   ╲ _|  |  ||   [_ |   _]
     ╲      |  |  ||     ||  |
      ╲ ____|__|__||_____||__|


     if you want to find a needle
   in a haystack, burn the haystack.

    ╔════════════════════════════╗
    ║                            ║
    ║   (1) Brick Rocket Stove   ║
    ║                            ║
    ║   (2) The Hobo Stove       ║
    ║                            ║
    ╚════════════════════════════╝

   ENTER ANY NUMBER (1) TO (2) FROM
     THE ABOVE MENU. (E) TO EXIT
      OR (B) BACK TO MAIN MENU");

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "1" => brick_rocket_stove(),
    "2" => the_hobo_stove(),
    "b" => main_menu(),
    "e" => exit(),
    _ => pyro(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn pyro_2(){

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "b" => pyro(),
    "m" => main_menu(),
    "e" => exit(),
    _ => pyro(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn recipes(){
    clear_screen();
println!("
                    ▄████████    ▄████████  ▄████████  ▄█     ▄███████▄    ▄████████    ▄████████
                   ███    ███   ███    ███ ███    ███ ███    ███    ███   ███    ███   ███    ███
                   ███    ███   ███    █▀  ███    █▀  ███▌   ███    ███   ███    █▀    ███    █▀
                  ▄███▄▄▄▄██▀  ▄███▄▄▄     ███        ███▌   ███    ███  ▄███▄▄▄       ███
                 ▀▀███▀▀▀▀▀   ▀▀███▀▀▀     ███        ███▌ ▀█████████▀  ▀▀███▀▀▀     ▀███████████
                 ▀███████████   ███    █▄  ███    █▄  ███    ███          ███    █▄           ███
                   ███    ███   ███    ███ ███    ███ ███    ███          ███    ███    ▄█    ███
                   ███    ███   ██████████ ████████▀  █▀    ▄████▀        ██████████  ▄████████▀
                   ███    ███


                  Dad cooks a deer and doesn't tell the kids what it is, he gives one clue.
                  it's what your mother calls me. the boy yells, it's a fucking dick, don't eat it..!!

    ╔══════════════════════════════════════╦══════════════════════════════════════╦══════════════════════════════════════╗
    ║                                      ║                                      ║                                      ║
    ║ (1)  Any Day's a Picnic              ║ (12) Comforting Beef                 ║ (23) Grilled Chicken and             ║
    ║      Chicken Salad                   ║      Enchilada Soup                  ║      Avocado Quinoa Pilaf            ║
    ║                                      ║                                      ║                                      ║
    ║ (2)  ANZAC slice                     ║ (13) Grilled Pizza                   ║ (24) Fruit Salad with                ║
    ║                                      ║                                      ║      Crystallized Ginger             ║
    ║ (3)  Bean and Rice Burritos          ║ (14) Irish Pub Beef Stew             ║                                      ║
    ║                                      ║                                      ║ (25) Pizza                           ║
    ║ (4)  Beef and Broccoli Stir-Fry      ║ (15) Italian Cannelloni              ║                                      ║
    ║                                      ║                                      ║ (26) Lentil-Veggie                   ║
    ║ (5)  Belgian Waffles                 ║ (16) Lasagna                         ║      Soup (Crock Pot)                ║
    ║                                      ║                                      ║                                      ║
    ║ (6)  Black Bean and Chicken Soup     ║ (17) Otello's Penne Bolognese        ║ (27) Spaghetti Sauce                 ║
    ║                                      ║                                      ║                                      ║
    ║ (7)  Brownies                        ║ (18) Otello’s 4 Beans & noodles      ║ (28) Slow Cooker Chicken             ║
    ║                                      ║                                      ║      Noodle Soup                     ║
    ║ (8)  Buttermilk Waffles              ║ (19) The Best Chili Ever             ║                                      ║
    ║                                      ║                                      ║ (29) Scott Hibb's Whiskey            ║
    ║ (9)  Chicken Noodle Soup             ║ (20) Stuffed Green Peppers           ║      Grilled Baby Back Ribs          ║
    ║                                      ║                                      ║                                      ║
    ║ (10) Chicken Tortilla Soup II        ║ (21) Mexican Chicken Chili Soup      ║ (30) Traditional Spaghetti Bolognese ║
    ║                                      ║                                      ║                                      ║
    ║ (11) Chinese Fried Rice              ║ (22) Quick + Easy Pot Roast          ║ (31) Whole Wheat Pancakes            ║
    ║                                      ║                                      ║                                      ║
    ╚══════════════════════════════════════╩══════════════════════════════════════╩══════════════════════════════════════╝

                ENTER ANY NUMBER (1) TO (31) FROM THE ABOVE MENU. (E) TO EXIT OR (B) BACK TO MAIN MENU");

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "1" => any_day_is_a_picnic_chicken_salad(),
    "2" => anzac_slice(),
    "3" => bean_and_rice_burritos(),
    "4" => beef_and_broccoli_stir_fry(),
    "5" => belgian_waffles(),
    "6" => black_bean_and_chicken_soup(),
    "7" => brownies(),
    "8" => buttermilk_waffles(),
    "9" => chicken_noodle_soup(),
    "10" => chicken_tortilla_soup_ii(),
    "11" => chinese_fried_rice(),
    "12" => comforting_beef_enchilada_soup(),
    "13" => grilled_pizza(),
    "14" => irish_pub_beef_stew(),
    "15" => italian_cannelloni(),
    "16" => lasagna(),
    "17" => otellos_penne_bolognese(),
    "18" => otellos_beans_and_noodles(),
    "19" => the_best_chili_ever(),
    "20" => stuffed_green_peppers(),
    "21" => mexican_chicken_chili_soup(),
    "22" => quick_easy_pot_roast(),
    "23" => grilled_chicken_and_avocado_quinoa_pilaf(),
    "24" => fruit_salad_with_crystallized_ginger(),
    "25" => pizza(),
    "26" => lentil_veggie_soup_crock_pot(),
    "27" => spaghetti_sauce(),
    "28" => slow_cooker_chicken_noodle_soup(),
    "29" => scott_hibb_whiskey_grilled_baby_back_ribs(),
    "30" => traditional_spaghetti_bolognese(),
    "31" => whole_wheat_pancakes(),
    "b" => main_menu(),
    "e" => exit(),
    _ => recipes(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn recipes_2(){

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "b" => recipes(),
    "m" => main_menu(),
    "e" => exit(),
    _ => recipes(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn sfs(){
    clear_screen();
println!("
                    .▄▄ ·       • ▌ ▄ ·. ▄▄▄ .
                    ▐█ ▀. ▪     ·██ ▐███▪▀▄.▀·
                    ▄▀▀▀█▄ ▄█▀▄ ▐█ ▌▐▌▐█·▐▀▀▪▄
                    ▐█▄▪▐█▐█▌.▐▌██ ██▌▐█▌▐█▄▄▌
                     ▀▀▀▀  ▀█▄▀▪▀▀  █▪▀▀▀ ▀▀▀
                   ·▄▄▄▄• ▄▌ ▄▄· ▄ •▄ ▪   ▐ ▄
                   ▐▄▄·█▪██▌▐█ ▌▪█▌▄▌▪██ •█▌▐█
                   ██▪ █▌▐█▌██ ▄▄▐▀▀▄·▐█·▐█▐▐▌
                   ██▌.▐█▄█▌▐███▌▐█.█▌▐█▌██▐█▌
                   ▀▀▀  ▀▀▀ ·▀▀▀ ·▀  ▀▀▀▀▀▀ █▪
                      .▄▄ ·  ▄ .▄▪  ▄▄▄▄▄
                      ▐█ ▀. ██▪▐███ •██
                      ▄▀▀▀█▄██▀▐█▐█· ▐█.▪
                      ▐█▄▪▐███▌▐▀▐█▌ ▐█▌·
                       ▀▀▀▀ ▀▀▀ ·▀▀▀ ▀▀▀


  I picked up a hitchhiker last night. he seemed surprised that I'd
  pick up a stranger and asked “thanks, but why would you pick me
  up? how do you know I'm not a serial killer?” I told him the
  chances of two serial killers in one car would be astronomical.

    ╔════════════════════════════════════════════════════════╗
    ║                                                        ║
    ║ (1) I Think My Neighbor Is An Alien                    ║
    ║                                                        ║
    ║ (2) HOW TO ROB A BANK                                  ║
    ║                                                        ║
    ║ (3) The caffeine conspiracy                            ║
    ║                                                        ║
    ║ (4) The cat alien conspiracy                           ║
    ║                                                        ║
    ║ (5) Torlak and the Tale of the Space Mushrooms         ║
    ║                                                        ║
    ╚════════════════════════════════════════════════════════╝

         ENTER ANY NUMBER (1) TO (5) FROM THE ABOVE MENU.
             (E) TO EXIT OR (B) BACK TO MAIN MENU");

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "1" => i_think_my_neighbor_is_an_alien(),
    "2" => robbanks(),
    "3" => the_caffeine_conspiracy(),
    "4" => the_cat_alien_conspiracy(),
    "5" => torlak_and_the_tale_of_the_space_mushrooms(),
    "b" => main_menu(),
    "e" => exit(),
    _ => sfs(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn sfs_2(){

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "b" => sfs(),
    "m" => main_menu(),
    "e" => exit(),
    _ => sfs(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn weed(){
    clear_screen();
println!("
                    ███▄ ▄███▓ ▄▄▄       ██▀███   ██▓ ▄▄▄██▀▀▀█    ██  ▄▄▄       ███▄    █  ▄▄▄
                   ▓██▒▀█▀ ██▒▒████▄    ▓██ ▒ ██▒▓██▒   ▒██   ██  ▓██▒▒████▄     ██ ▀█   █ ▒████▄
                   ▓██    ▓██░▒██  ▀█▄  ▓██ ░▄█ ▒▒██▒   ░██  ▓██  ▒██░▒██  ▀█▄  ▓██  ▀█ ██▒▒██  ▀█▄
                   ▒██    ▒██ ░██▄▄▄▄██ ▒██▀▀█▄  ░██░▓██▄██▓ ▓▓█  ░██░░██▄▄▄▄██ ▓██▒  ▐▌██▒░██▄▄▄▄██
                   ▒██▒   ░██▒ ▓█   ▓██▒░██▓ ▒██▒░██░ ▓███▒  ▒▒█████▓  ▓█   ▓██▒▒██░   ▓██░ ▓█   ▓██▒
                   ░ ▒░   ░  ░ ▒▒   ▓▒█░░ ▒▓ ░▒▓░░▓   ▒▓▒▒░  ░▒▓▒ ▒ ▒  ▒▒   ▓▒█░░ ▒░   ▒ ▒  ▒▒   ▓▒█░
                   ░  ░      ░  ▒   ▒▒ ░  ░▒ ░ ▒░ ▒ ░ ▒ ░▒░  ░░▒░ ░ ░   ▒   ▒▒ ░░ ░░   ░ ▒░  ▒   ▒▒ ░
                   ░      ░     ░   ▒     ░░   ░  ▒ ░ ░ ░ ░   ░░░ ░ ░   ░   ▒      ░   ░ ░   ░   ▒
                          ░         ░  ░   ░      ░   ░   ░     ░           ░  ░         ░       ░  ░



            ...... And when the cop told me I smelled like marijuana, I told him he smelled like bacon....

                    I don't think he thought it was very funny, which is why I'm calling you collect.

    ╔════════════════════════════╦═════════════════════════════════════════╦═════════════════════════════════════════╗
    ║                            ║                                         ║                                         ║
    ║ (1) Cooking with Pot       ║ (5) Cannabis Pancakes                   ║ (10) Weed French Toast                  ║
    ║                            ║                                         ║                                         ║
    ║ (2) Cannabis Canola Oil    ║ (6) One Pot Taco Weed Spaghetti         ║ (11) Weed Spaghetti Bolognese           ║
    ║                            ║                                         ║                                         ║
    ║ (3) Cannabis Butter        ║ (7) Christmas Cannabis Cookies          ║ (12) The Denver Weed Omelette           ║
    ║                            ║                                         ║                                         ║
    ║ (4) Decarboxylate Cannabis ║ (8) Quick Beef & Weed Stir Fry          ║ (13) Sausage Stuffed Peppers With Weed  ║
    ║                            ║                                         ║                                         ║
    ║                            ║ (9) Cannabis Winter Soup                ║ (14) Weed Brownies                      ║
    ║                            ║                                         ║                                         ║
    ╚════════════════════════════╩═════════════════════════════════════════╩═════════════════════════════════════════╝

                ENTER ANY NUMBER (1) TO (14) FROM THE ABOVE MENU. (E) TO EXIT OR (B) BACK TO MAIN MENU");

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "1" => cooking_with_pot(),
    "2" => cannabis_canola_oil(),
    "3" => cannabis_butter(),
    "4" => decarboxylate_cannabis(),
    "5" => cannabis_pancakes(),
    "6" => one_pot_taco_weed_spaghetti(),
    "7" => christmas_cannabis_cookies(),
    "8" => quick_beef_weed_stir_fry(),
    "9" => cannabis_winter_soup(),
    "10" => weed_french_toast(),
    "11" => weed_spaghetti_bolognese(),
    "12" => the_denver_weed_omelette(),
    "13" => sausage_stuffed_peppers_with_weed(),
    "14" => weed_brownies(),
    "b" => main_menu(),
    "e" => exit(),
    _ => weed(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn weed_2(){

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

match input.trim() {
    "b" => weed(),
    "m" => main_menu(),
    "e" => exit(),
    _ => weed(),
}
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn clear_screen() {
    let output = if cfg!(target_os = "windows") {
        Command::new("cmd")
            .args(&["/C", "cls"])
            .output()
            .expect("failed to execute process")
    } else {
        Command::new("sh")
            .arg("-c")
            .arg("clear")
            .output()
            .expect("failed to execute process")
    };
    println!("{}", String::from_utf8_lossy(&output.stdout));
}
//88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
fn exit(){
clear_screen();
    println!("exiting");
}
