
use std::{thread, time::Duration};
use std::io::{self, Write};

pub fn file() {

    crate::clear_screen();

    let frames: Vec<&str> = ANIMATION_FRAMES.split("[br]").collect();

    for frame in frames {
        io::stdout().flush().expect("failed to flush stdout");
        println!("{}", frame);
        let sleep_duration = Duration::from_millis(200);
            thread::sleep(sleep_duration);
        crate::clear_screen();
    }

    println!("{}", FRAME);

    println!("
    ╔══════════════════════╗
    ║      (R) Replay      ║
    ╠══════════════════════╣
    ║(B) Go Back           ║
    ║                      ║
    ║(M) Main Menu         ║
    ║                      ║
    ║(E) Exit              ║
    ╚══════════════════════╝");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "b" => super::menu::movies_menu(),
        "m" => crate::main_menu(),
        "e" => crate::exit(),
        _ => file(),
    }
}

const ANIMATION_FRAMES: &str = r#"
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    ||            \ ,\|/           ||
    ||             \O(_)___________||
    ||             /`/|\           ||
    ||            / : \            ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    ||        ,\|/\ : /            ||
    ||        O(_)_\:/_____________||
    ||        `/|\ /:\             ||
    ||            / : \            ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    ||     ,\|/   \ : /            ||
    ||     O(_)____\:/_____________||
    ||     `/|\    /:\             ||
    ||            / : \            ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    ||,\|/        \ : /            ||
    ||O(_)_________\:/_____________||
    ||`/|\         /:\             ||
    ||            / : \            ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    || \|/,       \ : /            ||
    ||_(_)O________\:/_____________||
    || /|\`        /:\             ||
    ||            / : \            ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    ||       \|/, \ : /            ||
    ||_______(_)O__\:/_____________||
    ||       /|\`  /:\             ||
    ||            / : \            ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \  |  /           ||
    ||            \ : /            ||
    ||_____________\:/_____________||
    ||          \  /_\(O)/_        ||
    ||           \/_:/(_)\         ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \ ,\|/  /          ||
    ||           \O(_)_/           ||
    ||            `/|\/\           ||
    ||_____________\:/__\__________||
    ||          \  /:\  /          ||
    ||           \/_:_\/           ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \__|__/           ||
    ||           /\ : /\           ||
    ||_____\(_)//__\:/__\__________||
    ||     /(O)\\  /:\  /          ||
    ||           \/_:_\/           ||
    ||           /  |  \           ||
    ||          /   :   \          ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    |    /         ||
    ||          \   :   /          ||
    ||           \__|__/           ||
    ||           /\ : /\           ||
    ||__________/__\:/__\__________||
    ||      \   \  /:\  /          ||
    ||       \.' \/_:_\/           ||
    ||        \  /  |  \|/,        ||
    ||         \/_._:_.(_)O        ||
    ||         /    |  /|\`        ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \    | _\(O)/_      ||
    ||          \   :  /(_)\       ||
    ||           \__|__/  \        ||
    ||           /\ : /\ .'\       ||
    ||__________/__\:/__\___\______||
    ||      \   \  /:\  /   /      ||
    ||       \.' \/_:_\/ './       ||
    ||        \  /  |  \  /        ||
    ||         \/_._:_._\/         ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \__ _|_ _ /         ||
    ||        _/\ ` : ` /\         ||
    ||     _\(_)/\__|__/  \        ||
    ||      /(O)\/\ : /\ .'\       ||
    ||__________/__\:/__\___\______||
    ||      \   \  /:\  /   /      ||
    ||       \.' \/_:_\/ './       ||
    ||        \  /  |  \  /        ||
    ||         \/_._:_._\/         ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \__ _|_ _ /         ||
    ||         /\ ` : ` /\         ||
    ||        /  \__|__/  \        ||
    ||       /'. /\ : /\ .'\       ||
    ||______/___/__\:/__\___\______||
    ||  \   \   \  /:\  /   /      ||
    ||   \_  \.' \/_:_\/ './       ||
    || _\(_)/_\  /  |  \  /        ||
    ||  /(O)\  \/_._:_._\/         ||
    ||         /    |    \         ||
    ||        /     :     \        ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \__ _|_ _ /         ||
    ||         /\ ` : ` /\         ||
    ||        /  \__|__/  \        ||
    ||       /'. /\ : /\ .'\       ||
    ||______/___/__\:/__\___\______||
    ||  \   \   \  /:\  /   /      ||
    ||   \   \.' \/_:_\/ './       ||
    ||    | .'\  /  |  \  /        ||
    ||    \'   \/_._:_._\/         ||
    ||     \   /    \|/, \         ||
    ||      `\/.---.(_)O  \        ||
    ||       /      /|\`   \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||        \     :     /        ||
    ||         \__ _|_ _ /         ||
    ||         /\ ` : ` /\         ||
    ||        /  \__|__/  \        ||
    ||       /'. /\ : /\ .'\       ||
    ||______/___/__\:/__\___\__\(O)/_
    ||  \   \   \  /:\  /   /  /(_)\|
    ||   \   \.' \/_:_\/ './    /  ||
    ||    | .'\  /  |  \  /'.  |   ||
    ||    \'   \/_._:_._\/   `/    ||
    ||     \   /    |    \   /     ||
    ||      `\/.---.:.---.\/`      ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |,\|/  /       ||
    ||        \     :O(_)'/\       ||
    ||         \__ _|`/|\/  `\     ||
    ||         /\ ` : ` /\   .\    ||
    ||        /  \__|__/  \.'  |   ||
    ||       /'. /\ : /\ .'\   \   ||
    ||______/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||   \   \.' \/_:_\/ './   /   ||
    ||    | .'\  /  |  \  /'.  |   ||
    ||    \'   \/_._:_._\/   `/    ||
    ||     \   /    |    \   /     ||
    ||      `\/.---.:.---.\/`      ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||       /\'---':'---'/\       ||
    ||     /`  \__ _|_ __/  `\     ||
    ||   _/.   /\ ` : ` /\   .\    ||
    || \(_)/'./  \__|__/  \.'  |   ||
    || /(O)\ /'. /\ : /\ .'\   \   ||
    ||______/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||   \   \.' \/_:_\/ './   /   ||
    ||    | .'\  /  |  \  /'.  |   ||
    ||    \'   \/_._:_._\/   `/    ||
    ||     \   /    |    \   /     ||
    ||      `\/.---.:.---.\/`      ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||       /\'---':'---'/\       ||
    ||     /`  \__ _|_ __/  `\     ||
    ||    /.   /\ ` : ` /\   .\    ||
    ||   |  './  \__|__/  \.'  |   ||
    ||   /   /'. /\ : /\ .'\   \   ||
    ||__/___/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /   ||
    || |  | .'\  /  |  \  /'.  |   ||
    || |  \'   \/_._:_._\/   `/    ||
    |\(_)/ \   /    |    \   /     ||
    |/(O)\  `\/.---.:.---.\/`      ||
    ||       /      |      \       ||
    ||      /       :       \      ||
    ||     /        |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||       /\'---':'---'/\       ||
    ||     /`  \__ _|_ __/  `\     ||
    ||    /.   /\ ` : ` /\   .\    ||
    ||   |  './  \__|__/  \.'  |   ||
    ||   /   /'. /\ : /\ .'\   \   ||
    ||__/___/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /   ||
    || |  | .'\  /  |  \  /'.  |   ||
    || |  \'   \/_._:_._\/   `/    ||
    ||/ .' \   /    |    \   /     ||
    ||.:_   `\/.---.:.---.\/`      ||
    ||   '.  / \|/, |      \       ||
    ||     \/.'(_)O :       \      ||
    ||     /   /|\` |        \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||       /\'---':'---'/\       ||
    ||     /`  \__ _|_ __/  `\     ||
    ||    /.   /\ ` : ` /\   .\    ||
    ||   |  './  \__|__/  \.'  |   ||
    ||   /   /'. /\ : /\ .'\   \   ||
    ||__/___/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /   ||
    || |  | .'\  /  |  \  /'.  |   ||
    || |  \'   \/_._:_._\/   `/    ||
    ||/ .' \   /    |    \   _\(O)/_|
    ||.:_   `\/.---.:.---.\/` /(_)\||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \        |        /     ||
    ||      \       :       /      ||
    ||       \      |      /       ||
    ||       /\'---':'---'/\       ||
    ||     /`  \__ _|_ __/  `\_\(O)/_
    ||    /.   /\ ` : ` /\   .\/(_)\|
    ||   |  './  \__|__/  \.'  | | ||
    ||   /   /'. /\ : /\ .'\   \  \||
    ||__/___/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'.  | | ||
    || |  \'   \/_._:_._\/   `/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \        ,\|/       /    ||
    ||     \       O(_)      /     ||
    ||      \      `/|\.__.'/\     ||
    ||       \      |      /  '.__ ||
    ||       /\'---':'---'/\     .'||
    ||     /`  \__ _|_ __/  `\ .' /||
    ||    /.   /\ ` : ` /\   .\  | ||
    ||   |  './  \__|__/  \.'  | | ||
    ||   /   /'. /\ : /\ .'\   \  \||
    ||__/___/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'.  | | ||
    || |  \'   \/_._:_._\/   `/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||   _ /\'.__.' : '.__.'/\     ||
    ||_\(_)/_\      |      /  '.__ ||
    || /(O)\ /\'---':'---'/\     .'||
    ||     /`  \__ _|_ __/  `\ .' /||
    ||    /.   /\ ` : ` /\   .\  | ||
    ||   |  './  \__|__/  \.'  | | ||
    ||   /   /'. /\ : /\ .'\   \  \||
    ||__/___/___/__\:/__\___\___\__||
    ||  \   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'.  | | ||
    || |  \'   \/_._:_._\/   `/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||     /\'.__.' : '.__.'/\     ||
    || __.'  \      |      /  '.__ ||
    ||'.     /\'---':'---'/\     .'||
    ||\ '. /`  \__ _|_ __/  `\ .' /||
    || |  /.   /\ ` : ` /\   .\  | ||
    || | |  './  \__|__/  \.'  | | ||
    ||/  /   /'. /\ : /\ .'\   \  \||
    \(_)/___/___/__\:/__\___\___\__||
    /(O)\   \   \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'.  | | ||
    || |  \'   \/_._:_._\/   `/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||     /\'.__.' : '.__.'/\     ||
    || __.'  \      |      /  '.__ ||
    ||'.     /\'---':'---'/\     .'||
    ||\ '. /`  \__ _|_ __/  `\ .' /||
    || |  /.   /\ ` : ` /\   .\  | ||
    || | |  './  \__|__/  \.'  | | ||
    ||/  / \|/'. /\ : /\ .'\   \  \||
    ||__/__(_)O_/__\:/__\___\___\__||
    ||  \  /|\` \  /:\  /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'.  | | ||
    || |  \'   \/_._:_._\/   `/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||     /\'.__.' : '.__.'/\     ||
    || __.'  \      |      /  '.__ ||
    ||'.     /\'---':'---'/\     .'||
    ||\ '. /`  \__ _|_ __/  `\ .' /||
    || |  /.   /\ ` : ` /\   .\  | ||
    || | |  './  \__|__/  \.'  | | ||
    ||/  /   /'. /\\|/,\ .'\   \  \||
    ||__/___/___/__(_)O_\___\___\__||
    ||  \   \   \  /|\` /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'.  | | ||
    || |  \'   \/_._:_._\/   `/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||     /\'.__.' : '.__.'/\     ||
    || __.'  \      |      /  '.__ ||
    ||'.     /\`---':'---'/\     .'||
    ||\ '. /`  \__ _|_ __/  `\ .' /||
    || |  /.   /\ ` : ` /\   .\  | ||
    || |  | './  \ _|_ /  \.' |  | ||
    ||/  /   /'. /\ : /\ .'\   \  \||
    ||__/___/___/_\(_)/_\___\___\__||
    ||  \   \   \ /(O)\ /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'. |  | ||
    || |  \'   \/_._:_._\/   '/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|
[br]
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||     /\'.__.' : '.__.'/\     ||
    || __.'  \      |      /  '.__ ||
    ||'.     /\`---':'---'/\     .'||
    ||\ '. /`  \__ _|_ __/  `\ .' /||
    || |  /.   /\ ` : ` /\   .\  | ||
    || |  | './  \ _|_ /  \.' |  | ||
    ||/  /   /'. /\ : /\ .'\   \  \||
    ||__/___/___/_\(_)/_\___\___\__||
    ||  \   \   \ /(O)\ /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'. |  | ||
    || |  \'   \/_._:_._\/   '/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|

"#;
const FRAME: &str = r#"
     _______________________________
    |\_____________________________/|
    ||   \          |          /   ||
    ||    \         |         /    ||
    ||     \       /|\       /     ||
    ||     /\'.__.' : '.__.'/\     ||
    || __.'  \      |      /  '.__ ||
    ||'.     /\`---':'---'/\     .'||
    ||\ '. /`  \__ _|_ __/  `\ .' /||
    || |  /.   /\ ` : ` /\   .\  | ||
    || |  | './  \ _|_ /  \.' |  | ||
    ||/  /   /'. /\ : /\ .'\   \  \||
    ||__/___/___/_\(_)/_\___\___\__||
    ||  \   \   \ /(O)\ /   /   /  ||
    ||\  \   \.' \/_:_\/ './   /  /||
    || |  | .'\  /  |  \  /'. |  | ||
    || |  \'   \/_._:_._\/   '/  | ||
    ||/ .' \   /    |    \   / '. \||
    ||.:_   `\/.---.:.---.\/`   _:.||
    ||   '.  / __   |   __ \  .'   ||
    ||     \/.'  '. : .'  '.\/     ||
    ||     /       \|/       \     ||
    ||____/_________|_________\____||
    |/_____________________________\|"#;