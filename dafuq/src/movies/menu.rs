
use std::io;

pub fn movies_menu() {
    crate::clear_screen();

    {
        let x = [r#""#, r#"        @@@@@@@@@@    @@@@@@   @@@  @@@  @@@  @@@@@@@@   @@@@@@"#, r#"        @@@@@@@@@@@  @@@@@@@@  @@@  @@@  @@@  @@@@@@@@  @@@@@@@"#, r#"        @@! @@! @@!  @@!  @@@  @@!  @@@  @@!  @@!       !@@"#, r#"        !@! !@! !@!  !@!  @!@  !@!  @!@  !@!  !@!       !@!"#, r#"        @!! !!@ @!@  @!@  !@!  @!@  !@!  !!@  @!!!:!    !!@@!!"#, r#"        !@!   ! !@!  !@!  !!!  !@!  !!!  !!!  !!!!!:     !!@!!!"#, r#"        !!:     !!:  !!:  !!!  :!:  !!:  !!:  !!:            !:!"#, r#"        :!:     :!:  :!:  !:!   ::!!:!   :!:  :!:           !:!"#, r#"        :::     ::   ::::: ::    ::::     ::   :: ::::  :::: ::"#, r#"        :      :     : :  :      :      :    : :: ::   :: : :"#, r#""#, r#"    What’s the difference between a pickpocket and a peeping tom?..."#, r#"    A pickpocket snatches watches, the peeping tom watches snatches."#, r#""#, r#"        ╔═════════════════════════════════════════════════════╗"#, r#"        ║ (1) Baseball - by 0_joan_stark                      ║"#, r#"        ║                                                     ║"#, r#"        ║ (2) Beach Scene - by 0_joan_stark                   ║"#, r#"        ║                                                     ║"#, r#"        ║ (3) Cat Love - by 0_joan_stark                      ║"#, r#"        ║                                                     ║"#, r#"        ║ (4) Diving - by 0_joan_stark                        ║"#, r#"        ║                                                     ║"#, r#"        ║ (5) Golf - by 0_joan_stark                          ║"#, r#"        ║                                                     ║"#, r#"        ║ (6) Spider - by 0_joan_stark                        ║"#, r#"        ║                                                     ║"#, r#"        ║ (7) Swimming - by 0_joan_stark                      ║"#, r#"        ║                                                     ║"#, r#"        ║ (8) Flame War - by Karlsson                         ║"#, r#"        ║                                                     ║"#, r#"        ║ (9) Aliens - by Krogg                               ║"#, r#"        ║                                                     ║"#, r#"        ║ (10) Fire - by Krogg                                ║"#, r#"        ║                                                     ║"#, r#"        ║ (11) Grind - by Krogg                               ║"#, r#"        ║                                                     ║"#, r#"        ║ (12) Globe - by misc                                ║"#, r#"        ║                                                     ║"#, r#"        ║ (13) Plane - by misc                                ║"#, r#"        ║                                                     ║"#, r#"        ║ (14) Train - by misc                                ║"#, r#"        ║                                                     ║"#, r#"        ║ (15) Under Sea - by misc                            ║"#, r#"        ║                                                     ║"#, r#"        ║ (16) Monkey                                         ║"#, r#"        ╚═════════════════════════════════════════════════════╝"#, r#""#, r#"           ENTER ANY NUMBER (1) TO (16) FROM THE ABOVE MENU."#, r#"                (E) TO EXIT OR (B) BACK TO MAIN MENU"#];

        for y in x.iter() {
            println!("  {}", y);
        }
    }

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "1" => super::baseball::file(),
        "2" => super::beach_scene::file(),
        "3" => super::cat_love::file(),
        "4" => super::diving::file(),
        "5" => super::golf::file(),
        "6" => super::spider::file(),
        "7" => super::swimming::file(),
        "8" => super::flame_war::file(),
        "9" => super::aliens::file(),
        "10" => super::fire::file(),
        "11" => super::grind::file(),
        "12" => super::globe::file(),
        "13" => super::plane::file(),
        "14" => super::train::file(),
        "15" => super::under_sea::file(),
        "16" => super::monkey::file(),
        "e" => crate::exit(),
        "b" => crate::main_menu(),
        _ => movies_menu(),
    }
}