
use std::{thread, time::Duration};
use std::io::{self, Write};

pub fn file() {

    crate::clear_screen();

    let frames: Vec<&str> = ANIMATION_FRAMES.split("[br]").collect();

    for frame in frames {
        io::stdout().flush().expect("failed to flush stdout");
        println!("{}", frame);
        let sleep_duration = Duration::from_millis(350);
            thread::sleep(sleep_duration);
        crate::clear_screen();
    }

    println!("{}", FRAME);

    println!("
    ╔══════════════════════╗
    ║      (R) Replay      ║
    ╠══════════════════════╣
    ║(B) Go Back           ║
    ║                      ║
    ║(M) Main Menu         ║
    ║                      ║
    ║(E) Exit              ║
    ╚══════════════════════╝");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "b" => super::menu::movies_menu(),
        "m" => crate::main_menu(),
        "e" => crate::exit(),
        _ => file(),
    }
}

const ANIMATION_FRAMES: &str = r#"
[br]
                              
                              
O   O     O                   
|\  |    /\                   
|\  |\  /\                    
||  ||  | \                   
##%%##%%#                     
\/\/\/\/                      
/\/\/\/\ _|_/_____            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|\ | \|             
##########|_____|             
           \___/              
            % %               
             %                
            %                 
              %               
            % %               
[br]
                              
                              
 O   O      O                 
 |\  |\    /\                 
 |\  |\   /\                  
 ||  ||  /  \                 
%##%%##%%                     
\/\/\/\/                      
/\/\/\/\ _________            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|| \ ||             
##########|_____|             
           \___/              
              %               
            % %               
             %                
            %                 
              %               
[br]
                              
                              
  O   O                       
  |\  |\                      
  |\  |\                      
  ||  || ___                  
%%##%%##%_//O                 
\/\/\/\/                      
/\/\/\/\ _________            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|/ - /|             
##########|_____|             
           \___/              
            %                 
              %               
            % %               
             %                
            %                 
[br]
                              
                              
   O   O                      
   |\  |\                     
\  |\  |\                     
|  ||  ||                     
#%%##%%##                     
\/\/\/\/ \ |                  
/\/\/\/\ _\/______            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|- / -|             
##########|_____|             
           \___/              
             %                
            %                 
              %               
            % %               
             %                
[br]
                              
                              
O   O     O                   
|\  |    /\                   
|\  |\  /\                    
||  ||  | \                   
##%%##%%#                     
\/\/\/\/                      
/\/\/\/\ _|_/_____            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|\ | \|             
##########|_____|             
           \___/              
            % %               
             %                
            %                 
              %               
            % %               
[br]
                              
                              
 O   O      O                 
 |\  |\    /\                 
 |\  |\   /\                  
 ||  ||  /  \                 
%##%%##%%                     
\/\/\/\/                      
/\/\/\/\ _________            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|| \ ||             
##########|_____|             
           \___/              
              %               
            % %               
             %                
            %                 
              %               
[br]
                              
                              
  O   O                       
  |\  |\                      
  |\  |\                      
  ||  || ___                  
%%##%%##%_//O                 
\/\/\/\/                      
/\/\/\/\ _________            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|/ - /|             
##########|_____|             
           \___/              
            %                 
              %               
            % %               
             %                
            %                 
[br]
                              
                              
   O   O                      
   |\  |\                     
\  |\  |\                     
|  ||  ||                     
#%%##%%##                     
\/\/\/\/ \ |                  
/\/\/\/\ _\/______            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|- / -|             
##########|_____|             
           \___/              
             %                
            %                 
              %               
            % %               
             %                
[br]
                              
                              
O   O     O                   
|\  |    /\                   
|\  |\  /\                    
||  ||  | \                   
##%%##%%#                     
\/\/\/\/                      
/\/\/\/\ _|_/_____            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|\ | \|             
##########|_____|             
           \___/              
            % %               
             %                
            %                 
              %               
            % %               
[br]
                              
                              
 O   O      O                 
 |\  |\    /\                 
 |\  |\   /\                  
 ||  ||  /  \                 
%##%%##%%                     
\/\/\/\/                      
/\/\/\/\ _________            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|| \ ||             
##########|_____|             
           \___/              
              %               
            % %               
             %                
            %                 
              %               
[br]
                              
                              
  O   O                       
  |\  |\                      
  |\  |\                      
  ||  || ___                  
%%##%%##%_//O                 
\/\/\/\/                      
/\/\/\/\ _________            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|/ - /|             
##########|_____|             
           \___/              
            %                 
              %               
            % %               
             %                
            %                 
[br]
                              
                              
   O   O                      
   |\  |\                     
\  |\  |\                     
|  ||  ||                     
#%%##%%##                     
\/\/\/\/ \ |                  
/\/\/\/\ _\/______            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|- / -|             
##########|_____|             
           \___/              
             %                
            %                 
              %               
            % %               
             %                

"#;
const FRAME: &str = r#"
                              
                              
   O   O                      
   |\  |\                     
\  |\  |\                     
|  ||  ||                     
#%%##%%##                     
\/\/\/\/ \ |                  
/\/\/\/\ _\/______            
\/\/\/\/\\GRINDER/            
/\/\/\/\/\\     /             
##########|\___/|             
##########|- / -|             
##########|_____|             
           \___/              
             %                
            %                 
              %               
            % %               
             %  "#;