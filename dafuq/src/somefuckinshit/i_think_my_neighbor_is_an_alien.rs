    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"I Think My Neighbor Is An Alien"#, r#""#, r#""#, r#""#, r#""#, r#"Guys, my neighbors really freak me out. Sometimes at night, they will pull in to their garage and"#, r#"close the door with what looks like a female human. Then, during the night, they will leave, and I"#, r#"will never see the female human being again. I think that maybe they are aliens and the “female"#, r#"human” is their disguised ancestor."#, r#""#, r#"I say this because shortly after they pull into the garage, I hear the car doors close and then I hear a"#, r#"bunch of sounds like something hitting the car, and I hear screaming. So I think the “female human”"#, r#"disguised alien is one of their ancestors who can't adapt to the earth's atmosphere. After about"#, r#"fifteen minutes of the thumping noise, I hear the car doors close again and then they leave and the"#, r#"car comes back and parks for the night."#, r#""#, r#"I am really kind of freaked out about this. My neighbor is about 30, and “married” to another"#, r#"humanoid who is strangely absent during these rituals. He always walks around with a t-shirt on"#, r#"that has some strange scrawlings on it that look like alien language, he says it is something to do"#, r#"with a group of guys he hung out with in college but I think he's trying to lie to me."#, r#""#, r#"Anyone know how I can find out if these are really aliens??"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::sfs_2();
    }
