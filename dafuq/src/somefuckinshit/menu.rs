
use std::io;

pub fn sfs_menu() {
    crate::clear_screen();

    {
        let x = [r#""#, r#"                    .▄▄ ·       • ▌ ▄ ·. ▄▄▄ ."#, r#"                    ▐█ ▀. ▪     ·██ ▐███▪▀▄.▀·"#, r#"                    ▄▀▀▀█▄ ▄█▀▄ ▐█ ▌▐▌▐█·▐▀▀▪▄"#, r#"                    ▐█▄▪▐█▐█▌.▐▌██ ██▌▐█▌▐█▄▄▌"#, r#"                     ▀▀▀▀  ▀█▄▀▪▀▀  █▪▀▀▀ ▀▀▀"#, r#"                   ·▄▄▄▄• ▄▌ ▄▄· ▄ •▄ ▪   ▐ ▄"#, r#"                   ▐▄▄·█▪██▌▐█ ▌▪█▌▄▌▪██ •█▌▐█"#, r#"                   ██▪ █▌▐█▌██ ▄▄▐▀▀▄·▐█·▐█▐▐▌"#, r#"                   ██▌.▐█▄█▌▐███▌▐█.█▌▐█▌██▐█▌"#, r#"                   ▀▀▀  ▀▀▀ ·▀▀▀ ·▀  ▀▀▀▀▀▀ █▪"#, r#"                      .▄▄ ·  ▄ .▄▪  ▄▄▄▄▄"#, r#"                      ▐█ ▀. ██▪▐███ •██"#, r#"                      ▄▀▀▀█▄██▀▐█▐█· ▐█.▪"#, r#"                      ▐█▄▪▐███▌▐▀▐█▌ ▐█▌·"#, r#"                       ▀▀▀▀ ▀▀▀ ·▀▀▀ ▀▀▀"#, r#""#, r#""#, r#"  I picked up a hitchhiker last night. he seemed surprised that I'd"#, r#"  pick up a stranger and asked “thanks, but why would you pick me"#, r#"  up? how do you know I'm not a serial killer?” I told him the"#, r#"  chances of two serial killers in one car would be astronomical."#, r#""#, r#"    ╔════════════════════════════════════════════════════════╗"#, r#"    ║                                                        ║"#, r#"    ║ (1) I Think My Neighbor Is An Alien                    ║"#, r#"    ║                                                        ║"#, r#"    ║ (2) HOW TO ROB A BANK                                  ║"#, r#"    ║                                                        ║"#, r#"    ║ (3) The caffeine conspiracy                            ║"#, r#"    ║                                                        ║"#, r#"    ║ (4) The cat alien conspiracy                           ║"#, r#"    ║                                                        ║"#, r#"    ║ (5) Torlak and the Tale of the Space Mushrooms         ║"#, r#"    ║                                                        ║"#, r#"    ╚════════════════════════════════════════════════════════╝"#, r#""#, r#"         ENTER ANY NUMBER (1) TO (5) FROM THE ABOVE MENU."#, r#"             (E) TO EXIT OR (B) BACK TO MAIN MENU"#];

        for y in x.iter() {
            println!("  {}", y);
        }
    }

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "1" => super::i_think_my_neighbor_is_an_alien::file(),
        "2" => super::robbanks::file(),
        "3" => super::the_caffeine_conspiracy::file(),
        "4" => super::the_cat_alien_conspiracy::file(),
        "5" => super::torlak_and_the_tale_of_the_space_mushrooms::file(),
        "b" => crate::main_menu(),
        "e" => crate::exit(),
        _ => sfs_menu(),
    }
}

pub fn sfs_2() {

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");
    
    match input.trim() {
        "b" => sfs_menu(),
        "m" => crate::main_menu(),
        "e" => crate::exit(),
        _ => sfs_2(),
    }
}