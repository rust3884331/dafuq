
use std::io;

pub fn pyro_menu() {
    crate::clear_screen();

    {
        let x = [r#""#, r#"         ____  __ __  ____   ___      "#, r#"        |    \|  |  ||    \ /   \     "#, r#"        |  o  )  |  ||  D  )     |    "#, r#"        |   _/|  ~  ||    /|  O  |    "#, r#"        |  |  |___, ||    \|     |    "#, r#"        |  |  |     ||  .  \     |    "#, r#"        |__|  |____/ |__|\_|\___/     "#, r#"                              "#, r#"          __  __ __    ___  _____   "#, r#"         /  ]|  |  |  /  _]|     |  "#, r#"        /  / |  |  | /  [_ |   __|  "#, r#"       /  /  |  _  ||    _]|  |_    "#, r#"      /   \_ |  |  ||   [_ |   _]   "#, r#"      \     ||  |  ||     ||  |     "#, r#"       \____||__|__||_____||__| "#, r#""#, r#""#, r#"     if you want to find a needle"#, r#"   in a haystack, burn the haystack."#, r#""#, r#"    ╔════════════════════════════╗"#, r#"    ║                            ║"#, r#"    ║   (1) Brick Rocket Stove   ║"#, r#"    ║                            ║"#, r#"    ║   (2) The Hobo Stove       ║"#, r#"    ║                            ║"#, r#"    ╚════════════════════════════╝"#, r#""#, r#"   ENTER ANY NUMBER (1) TO (2) FROM"#, r#"     THE ABOVE MENU. (E) TO EXIT"#, r#"      OR (B) BACK TO MAIN MENU"#];

        for y in x.iter() {
            println!("  {}", y);
        }
    }

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "1" => super::brick_rocket_stove::file(),
        "2" => super::the_hobo_stove::file(),
        "b" => crate::main_menu(),
        "e" => crate::exit(),
        _ => pyro_menu(),
    }
}


pub fn pyro_2() {

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");
    
    match input.trim() {
        "b" => pyro_menu(),
        "m" => crate::main_menu(),
        "e" => crate::exit(),
        _ => pyro_2(),
    }
}