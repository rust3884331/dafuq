
use std::io;

pub fn weed_menu() {
    crate::clear_screen();

    {
        let x = [r#""#, r#"                    ███▄ ▄███▓ ▄▄▄       ██▀███   ██▓ ▄▄▄██▀▀▀█    ██  ▄▄▄       ███▄    █  ▄▄▄"#, r#"                   ▓██▒▀█▀ ██▒▒████▄    ▓██ ▒ ██▒▓██▒   ▒██   ██  ▓██▒▒████▄     ██ ▀█   █ ▒████▄"#, r#"                   ▓██    ▓██░▒██  ▀█▄  ▓██ ░▄█ ▒▒██▒   ░██  ▓██  ▒██░▒██  ▀█▄  ▓██  ▀█ ██▒▒██  ▀█▄"#, r#"                   ▒██    ▒██ ░██▄▄▄▄██ ▒██▀▀█▄  ░██░▓██▄██▓ ▓▓█  ░██░░██▄▄▄▄██ ▓██▒  ▐▌██▒░██▄▄▄▄██"#, r#"                   ▒██▒   ░██▒ ▓█   ▓██▒░██▓ ▒██▒░██░ ▓███▒  ▒▒█████▓  ▓█   ▓██▒▒██░   ▓██░ ▓█   ▓██▒"#, r#"                   ░ ▒░   ░  ░ ▒▒   ▓▒█░░ ▒▓ ░▒▓░░▓   ▒▓▒▒░  ░▒▓▒ ▒ ▒  ▒▒   ▓▒█░░ ▒░   ▒ ▒  ▒▒   ▓▒█░"#, r#"                   ░  ░      ░  ▒   ▒▒ ░  ░▒ ░ ▒░ ▒ ░ ▒ ░▒░  ░░▒░ ░ ░   ▒   ▒▒ ░░ ░░   ░ ▒░  ▒   ▒▒ ░"#, r#"                   ░      ░     ░   ▒     ░░   ░  ▒ ░ ░ ░ ░   ░░░ ░ ░   ░   ▒      ░   ░ ░   ░   ▒"#, r#"                          ░         ░  ░   ░      ░   ░   ░     ░           ░  ░         ░       ░  ░"#, r#""#, r#""#, r#""#, r#"            ...... And when the cop told me I smelled like marijuana, I told him he smelled like bacon...."#, r#""#, r#"                    I don't think he thought it was very funny, which is why I'm calling you collect."#, r#""#, r#"    ╔════════════════════════════╦═════════════════════════════════════════╦═════════════════════════════════════════╗"#, r#"    ║                            ║                                         ║                                         ║"#, r#"    ║ (1) Cooking with Pot       ║ (5) Cannabis Pancakes                   ║ (10) Weed French Toast                  ║"#, r#"    ║                            ║                                         ║                                         ║"#, r#"    ║ (2) Cannabis Canola Oil    ║ (6) One Pot Taco Weed Spaghetti         ║ (11) Weed Spaghetti Bolognese           ║"#, r#"    ║                            ║                                         ║                                         ║"#, r#"    ║ (3) Cannabis Butter        ║ (7) Christmas Cannabis Cookies          ║ (12) The Denver Weed Omelette           ║"#, r#"    ║                            ║                                         ║                                         ║"#, r#"    ║ (4) Decarboxylate Cannabis ║ (8) Quick Beef & Weed Stir Fry          ║ (13) Sausage Stuffed Peppers With Weed  ║"#, r#"    ║                            ║                                         ║                                         ║"#, r#"    ║                            ║ (9) Cannabis Winter Soup                ║ (14) Weed Brownies                      ║"#, r#"    ║                            ║                                         ║                                         ║"#, r#"    ╚════════════════════════════╩═════════════════════════════════════════╩═════════════════════════════════════════╝"#, r#""#, r#"                ENTER ANY NUMBER (1) TO (14) FROM THE ABOVE MENU. (E) TO EXIT OR (B) BACK TO MAIN MENU"#];

        for y in x.iter() {
            println!("  {}", y);
        }
    }

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "1" => super::cooking_with_pot::file(),
        "2" => super::cannabis_canola_oil::file(),
        "3" => super::cannabis_butter::file(),
        "4" => super::decarboxylate_cannabis::file(),
        "5" => super::cannabis_pancakes::file(),
        "6" => super::one_pot_taco_weed_spaghetti::file(),
        "7" => super::christmas_cannabis_cookies::file(),
        "8" => super::quick_beef_weed_stir_fry::file(),
        "9" => super::cannabis_winter_soup::file(),
        "10" => super::weed_french_toast::file(),
        "11" => super::weed_spaghetti_bolognese::file(),
        "12" => super::the_denver_weed_omelette::file(),
        "13" => super::sausage_stuffed_peppers_with_weed::file(),
        "14" => super::weed_brownies::file(),
        "b" => crate::main_menu(),
        "e" => crate::exit(),
         _ => weed_menu(),
    }
}


pub fn weed_2(){

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");
    
    match input.trim() {
        "b" => weed_menu(),
        "m" => crate::main_menu(),
        "e" => crate::exit(),
        "r" => crate::random(),
        _ => weed_2(),
    }
}