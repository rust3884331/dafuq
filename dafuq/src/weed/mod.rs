pub mod menu;
pub mod cannabis_butter;
pub mod cannabis_canola_oil;
pub mod cooking_with_pot;
pub mod decarboxylate_cannabis;
pub mod cannabis_pancakes;
pub mod cannabis_winter_soup;
pub mod christmas_cannabis_cookies;
pub mod one_pot_taco_weed_spaghetti;
pub mod quick_beef_weed_stir_fry;
pub mod sausage_stuffed_peppers_with_weed;
pub mod the_denver_weed_omelette;
pub mod weed_brownies;
pub mod weed_french_toast;
pub mod weed_spaghetti_bolognese;