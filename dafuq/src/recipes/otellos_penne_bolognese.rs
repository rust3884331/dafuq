    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Otello's Penne Bolognese"#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"olive oil"#, r#""#, r#"500g beef mince"#, r#""#, r#"one large onion"#, r#""#, r#"2 cups of tomato sauce"#, r#""#, r#"2 tablespoons of sugar"#, r#""#, r#"penne pasta"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"heat oil in a large frying pan. add onion and cook, stirring until"#, r#"softened. add the beef mince keep stirring until browned."#, r#""#, r#"add the sauce and bring to a simmer, then reduce to low heat."#, r#""#, r#"while the sauce is simmering bring a large saucepan of water to boil for the pasta."#, r#""#, r#"once the penne pasta is cooked strain the pasta then mix well with the sauce."#, r#""#, r#"enjoy."#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
