    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Stuffed Green Peppers"#, r#""#, r#""#, r#""#, r#""#, r#"These tasty stuffed peppers make a delicious and filling entree. Use brown"#, r#"rice to increase your whole grains."#, r#""#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#"    4 green pepper (large, washed)"#, r#"    1 pound turkey, ground, 85% lean"#, r#"    1 cup rice, uncooked"#, r#"    1/2 cup onion (peeled and chopped)"#, r#"    1 1/2 cups tomato sauce, unsalted"#, r#"    ground black pepper (to taste)"#, r#""#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#"1. Cut around the stem of the green peppers. Remove the seeds and the pulpy part of the peppers."#, r#""#, r#"2. Wash, and then cook green peppers in boiling water for five minutes. Drain well."#, r#""#, r#"3. In saucepan, brown turkey. Add rice, onion, 1/2 cup tomato sauce and black pepper."#, r#""#, r#"4. Stuff each pepper with the mixture and place in casserole dish."#, r#""#, r#"5. Pour the remaining tomato sauce over the green peppers."#, r#""#, r#"6. Cover and bake for 30 minutes at 350 degrees."#, r#""#, r#""#, r#""#, r#""#, r#" Source:"#, r#""#, r#"From Pyramid to the Plate: Healthy Eating by Timing, Combining, and Planning"#, r#"Adopted from: Eating Right is Basic"#, r#"Michigan State University Extension"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
