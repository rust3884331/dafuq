    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"ANZAC slice"#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#"1 1/4 cups plain flour"#, r#"1 1/4 cups rolled oats"#, r#"1 cup firmly packed brown sugar"#, r#"1 cup shredded coconut"#, r#"150g butter, chopped"#, r#"2 tablespoons golden syrup"#, r#"1/2 teaspoon bicarbonate of soda"#, r#"2 tablespoons boiling water"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#"Preheat oven to 180°C/160°C fan-forced. Grease and line a 3cm-deep, 19cm x 29cm (base) slice"#, r#"pan with baking paper, allowing a 2cm overhang at long ends."#, r#""#, r#"Combine flour, oats, sugar and coconut in a large bowl. Make a well in the centre."#, r#""#, r#"Place butter and syrup in a saucepan over low heat. Cook, stirring occasionally, for 8 to 10 minutes"#, r#"or until smooth. Combine bicarbonate of soda and boiling water in a jug. Remove butter mixture"#, r#"from heat. Stir in bicarbonate of soda mixture. Add to flour mixture. Stir to combine."#, r#""#, r#"Transfer to prepared pan. Using the back of a spoon, press mixture evenly into pan. Bake for 25 to"#, r#"30 minutes or until golden. Cool in pan. Cut into squares. Serve."#, r#""#, r#""#, r#""#, r#""#, r#"- Brett Ede"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
