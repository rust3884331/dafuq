    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Comforting Beef Enchilada Soup"#, r#""#, r#""#, r#""#, r#""#, r#"Like my Tortilla Soup II recipe but modified for people that like beef instead. Personally, I like the"#, r#"chicken version much better."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"8 ounces carrots, diced"#, r#""#, r#"8 ounces celery"#, r#""#, r#"8 ounces onions"#, r#""#, r#"1⁄2 teaspoon garlic powder"#, r#""#, r#"salt and pepper"#, r#""#, r#"2 tablespoons corn oil"#, r#""#, r#"4 (15 ounce) cans beef broth"#, r#""#, r#"1 (15 ounce) can petite diced tomatoes"#, r#""#, r#"1 (10 ounce) can Rotel diced tomatoes"#, r#""#, r#"1 (1 1/4-1 1/2 ounce) packet taco seasoning"#, r#""#, r#"1 (10 count) package corn tortillas, cut into small pieces"#, r#""#, r#"12 ounces shredded beef brisket (or shredded grilled beef fajita meat)"#, r#""#, r#"1 cup milk"#, r#""#, r#"corn tortilla chips, broken into small pieces"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Saute carrots, onions, celery in corn oil, garlic powder, salt and pepper until tender."#, r#""#, r#"Add beef broth and bring to boil."#, r#""#, r#"Add tomatoes, Rotel, taco seasoning, and shredded beef."#, r#""#, r#"Cut tortillas into small pieces and add to broth mixture."#, r#""#, r#"Let boil for 20 minutes or until tortillas are thoroughly incorporated into soup, stirring occasionally"#, r#"to keep from sticking."#, r#""#, r#"Reduce heat and simmer for additional 10 minutes."#, r#""#, r#"Add milk and simmer for additional 10 minutes."#, r#""#, r#"If thicker soup is desired, add more diced tortillas and let incorporate into soup."#, r#""#, r#"Garnish with shredded cheese and broken tortilla chips."#, r#""#, r#"Substitutions: 1 cup Masa Harina (masa flour) for 1 12 ct package of yellow corn tortillas."#, r#"Gradually add masa flour mixing into broth. If thicker soup is desired, add more masa flour."#, r#""#, r#"Can also add two seeded/sliced jalapeños and one sprig of fresh cilantro."#, r#""#, r#""#, r#""#, r#""#, r#"- Eddie Shipman"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
