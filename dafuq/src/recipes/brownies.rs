    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Brownies"#, r#""#, r#""#, r#""#, r#""#, r#"These brownies are not cake-like. They are more moist and gooey. I always undercook them a bit"#, r#"because we like them very moist."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1⁄2 cup vegetable oil"#, r#"1 cup sugar"#, r#"1 teaspoon vanilla"#, r#"2 large eggs"#, r#"1⁄4 teaspoon baking powder"#, r#"1⁄3 cup cocoa powder"#, r#"1⁄4 teaspoon salt"#, r#"1⁄2 cup flour"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Preheat oven to 350 degrees Fahrenheit or 180 degrees Celsius."#, r#""#, r#"Mix oil and sugar until well blended."#, r#""#, r#"Add eggs and vanilla; stir just until blended."#, r#""#, r#"Mix all dry ingredients in a separate bowl."#, r#""#, r#"Stir dry ingredients into the oil/sugar mixture."#, r#""#, r#"Pour into greased 9 x 9 square pan."#, r#""#, r#"Bake for 20 minutes or until sides just starts to pull away from the pan."#, r#""#, r#"Cool completely before cutting."#, r#""#, r#""#, r#"Note: I usually double the recipe and bake in a 9 x 13 pan. If you double the recipe, you will need to"#, r#"      cook longer than 20 minutes."#, r#""#, r#""#, r#""#, r#""#, r#"- Juenessa"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
