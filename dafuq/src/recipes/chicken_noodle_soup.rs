    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Chicken Noodle Soup"#, r#""#, r#""#, r#""#, r#""#, r#"This chicken noodle soup is great on a cool day."#, r#""#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#""#, r#"     2 teaspoons butter"#, r#""#, r#"     1 cup sliced celery"#, r#""#, r#"     1 cup chopped carrot"#, r#""#, r#"     1⁄2 cup chopped onion"#, r#""#, r#"     1⁄2 teaspoon thyme"#, r#""#, r#"     1 teaspoon poultry seasoning"#, r#""#, r#"     2 (32 ounce) containers and 1 can chicken broth (14 ounce)"#, r#""#, r#"     2 teaspoons chicken bouillon"#, r#""#, r#"     8 ounces egg noodles"#, r#""#, r#"     2 cups cooked chicken (3 frozen breasts)"#, r#""#, r#"     parsley"#, r#""#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#""#, r#"Melt butter in large pot."#, r#""#, r#"Sauté the celery, carrot and onion for 5 to 10 minutes."#, r#""#, r#"Add thyme, poultry seasoning, chicken broth and bouilion."#, r#""#, r#"Bring to a boil."#, r#""#, r#"Add noodles and chicken and cook on low for 20 minutes."#, r#""#, r#"Sprinkle with parsley."#, r#""#, r#""#, r#""#, r#""#, r#"- MizzNezz"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
