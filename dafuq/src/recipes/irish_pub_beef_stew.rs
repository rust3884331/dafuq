    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Irish Pub Beef Stew"#, r#""#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#""#, r#"     1 1⁄2 lbs beef, cut into chunks (splurge on the cut if you can)"#, r#""#, r#"     1⁄4 cup butter"#, r#""#, r#"     1 (10 1/2 ounce) can tomato soup"#, r#""#, r#"     1 (10 1/2 ounce) can water"#, r#""#, r#"     4 carrots, cut into chunks"#, r#""#, r#"     4 large potatoes, cut into chunks (I don't peel carrots or spuds but you can if you like)"#, r#""#, r#"     2 stalks celery, cut into chunks"#, r#""#, r#"     4 onions, cut into chunks"#, r#""#, r#"     2 teaspoons salt"#, r#""#, r#"     1 teaspoon black pepper"#, r#""#, r#"     1⁄4 cup fresh parsley, chopped fine"#, r#""#, r#"     1⁄4 cup good quality cooking sherry"#, r#""#, r#"     2 bay leaves"#, r#""#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#""#, r#"Preheat oven to 300F degrees."#, r#""#, r#"In a heavy skillet brown the beef in the butter over medium high heat."#, r#""#, r#"Add the soup and water and stir well."#, r#""#, r#"Add the rest of the ingredients and cook for about 5 minutes, stirring once."#, r#""#, r#"Transfer to a cast iron dutch oven or oven proof pot and cook in the oven,"#, r#"covered for 5 hours, stirring occasionally."#, r#""#, r#"Remove from oven, remove bay leaves and serve with Irish Soda Bread and butter."#, r#""#, r#""#, r#""#, r#""#, r#"- Diana Adcock"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
