    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Fruit Salad with Crystallized Ginger"#, r#""#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#""#, r#"2 cups Granny Smith apples"#, r#""#, r#"2 cups papaya cubes"#, r#""#, r#"1 cup kiwi slices"#, r#""#, r#"1-1/2 cups poppy seed dressing"#, r#""#, r#"1/4 cup crystallized ginger"#, r#""#, r#"1 cup raspberries"#, r#""#, r#"1/4 cup seedless grapes"#, r#""#, r#"2 tablespoons lime juice"#, r#""#, r#"Fresh mint leaves"#, r#""#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#""#, r#"toss ingredients in large bowl. ready to serve!"#, r#""#, r#"by Casey Ryback - Under Siege 2: Dark Territory"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
