    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Whole Wheat Pancakes"#, r#""#, r#""#, r#""#, r#"These are SO delicious - the honey & whole wheat give them a wonderful"#, r#"flavor. Hope you'll enjoy them as much as we do!"#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#""#, r#"     1 cup whole wheat flour"#, r#""#, r#"     2 teaspoons baking powder"#, r#""#, r#"     1⁄2 teaspoon salt"#, r#""#, r#"     1 tablespoon honey"#, r#""#, r#"     3 tablespoons oil"#, r#""#, r#"     1 cup buttermilk"#, r#""#, r#"     2 large eggs"#, r#""#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#""#, r#"Stir honey& oil together in a bowl."#, r#""#, r#"Add milk & eggs & beat well."#, r#""#, r#"Mix dry ingredients into the liquids until flour is moistened."#, r#""#, r#""#, r#""#, r#""#, r#"- WJKing"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
