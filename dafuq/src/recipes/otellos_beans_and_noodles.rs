    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Otello’s 4 Beans & noodles"#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1 Medium sized onion, diced"#, r#""#, r#"4 medium sized potatoes pealed chopped Into 8 chunks"#, r#""#, r#"400g tin of Four beans"#, r#""#, r#"400g tin of chick peas"#, r#""#, r#"1 1/2 cups of baby peas"#, r#""#, r#"Salt to taste."#, r#""#, r#"500g elbow pasta"#, r#""#, r#"1/4 cup of olive oil"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"using a large saucepan, add the onion, potato, and the chick peas, four beans, then"#, r#"add 750ml of water, bring to boil then reduce to low heat to a simmer till potatoes are cooked."#, r#""#, r#"bring back to full boil, add pasta, when the liquid starts boiling again, add baby peas."#, r#""#, r#"once pasta is cooked, add the olive oil."#, r#""#, r#"enjoy."#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }
        
        super::menu::recipes_2();
    }
