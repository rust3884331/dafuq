    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Grilled Chicken and Avocado Quinoa Pilaf"#, r#""#, r#""#, r#""#, r#""#, r#"This pilaf recipe pairs avocados and bell peppers with red quinoa and grilled chicken, for a"#, r#"satisfying, colorful meal."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"2 tablespoons fresh or bottled lemon juice"#, r#"1/4 cup fresh basil"#, r#"3/4 teaspoon ground black pepper (divided)"#, r#"1 avocado (cut into chunks)"#, r#"1 tablespoon olive oil (divided)"#, r#"1/4 teaspoon salt"#, r#"2 small boneless, skinless chicken breasts (about 1 lb)"#, r#"1 large red bell pepper"#, r#"1/2 medium onion (chopped)"#, r#"1 clove garlic (minced)"#, r#"3 cups water"#, r#"3 teaspoons sodium-free chicken bouillon"#, r#"1 1/2 cups red quinoa (uncooked/dry)"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Heat grill."#, r#""#, r#"Peel and cut avocado into chunks; place in a medium bowl."#, r#""#, r#"Mix lemon juice, basil, an 1/2 tsp black pepper. Drizzle over avocado chunks, toss, and set aside."#, r#""#, r#"Cut chicken breasts in half crosswise."#, r#""#, r#"Mix 1/2 tbsp olive oil, salt, and remaining black pepper. Brush mixture on chicken and red bell"#, r#"pepper."#, r#""#, r#"Grill chicken and pepper until done. Set chicken breasts aside. Cut pepper into thin strips."#, r#""#, r#"While chicken and peppers are grilling, heat remaining olive oil in a large pan, add garlic and onion,"#, r#"and cook until tender, about 5 minutes."#, r#""#, r#"Add water, bouillon, and quinoa to pan; bring to boil, cover, reduce heat, and simmer until liquid is"#, r#"absorbed and quinoa is cooked (about 15-20 minutes)."#, r#""#, r#"Place quinoa pilaf in a large bowl and add chicken, red peppers, and avocado. Toss gently."#, r#""#, r#""#, r#""#, r#""#, r#"Notes:  Serving Suggestions: Serve with non-fat milk and orange slices."#, r#""#, r#""#, r#""#, r#""#, r#"Source: Produce For Better Health Foundation"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
