
use std::io;

pub fn recipes_menu(){
    crate::clear_screen();

    {
        let x = [r#""#, r#"                    ▄████████    ▄████████  ▄████████  ▄█     ▄███████▄    ▄████████    ▄████████"#, r#"                   ███    ███   ███    ███ ███    ███ ███    ███    ███   ███    ███   ███    ███"#, r#"                   ███    ███   ███    █▀  ███    █▀  ███▌   ███    ███   ███    █▀    ███    █▀"#, r#"                  ▄███▄▄▄▄██▀  ▄███▄▄▄     ███        ███▌   ███    ███  ▄███▄▄▄       ███"#, r#"                 ▀▀███▀▀▀▀▀   ▀▀███▀▀▀     ███        ███▌ ▀█████████▀  ▀▀███▀▀▀     ▀███████████"#, r#"                 ▀███████████   ███    █▄  ███    █▄  ███    ███          ███    █▄           ███"#, r#"                   ███    ███   ███    ███ ███    ███ ███    ███          ███    ███    ▄█    ███"#, r#"                   ███    ███   ██████████ ████████▀  █▀    ▄████▀        ██████████  ▄████████▀"#, r#"                   ███    ███"#, r#""#, r#""#, r#"                  Dad cooks a deer and doesn't tell the kids what it is, he gives one clue."#, r#"                  it's what your mother calls me. the boy yells, it's a fucking dick, don't eat it..!!"#, r#""#, r#"    ╔══════════════════════════════════════╦══════════════════════════════════════╦══════════════════════════════════════╗"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ║ (1)  Any Day's a Picnic              ║ (12) Comforting Beef                 ║ (23) Grilled Chicken and             ║"#, r#"    ║      Chicken Salad                   ║      Enchilada Soup                  ║      Avocado Quinoa Pilaf            ║"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ║ (2)  ANZAC slice                     ║ (13) Grilled Pizza                   ║ (24) Fruit Salad with                ║"#, r#"    ║                                      ║                                      ║      Crystallized Ginger             ║"#, r#"    ║ (3)  Bean and Rice Burritos          ║ (14) Irish Pub Beef Stew             ║                                      ║"#, r#"    ║                                      ║                                      ║ (25) Pizza                           ║"#, r#"    ║ (4)  Beef and Broccoli Stir-Fry      ║ (15) Italian Cannelloni              ║                                      ║"#, r#"    ║                                      ║                                      ║ (26) Lentil-Veggie                   ║"#, r#"    ║ (5)  Belgian Waffles                 ║ (16) Lasagna                         ║      Soup (Crock Pot)                ║"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ║ (6)  Black Bean and Chicken Soup     ║ (17) Otello's Penne Bolognese        ║ (27) Spaghetti Sauce                 ║"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ║ (7)  Brownies                        ║ (18) Otello’s 4 Beans & noodles      ║ (28) Slow Cooker Chicken             ║"#, r#"    ║                                      ║                                      ║      Noodle Soup                     ║"#, r#"    ║ (8)  Buttermilk Waffles              ║ (19) The Best Chili Ever             ║                                      ║"#, r#"    ║                                      ║                                      ║ (29) Scott Hibb's Whiskey            ║"#, r#"    ║ (9)  Chicken Noodle Soup             ║ (20) Stuffed Green Peppers           ║      Grilled Baby Back Ribs          ║"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ║ (10) Chicken Tortilla Soup II        ║ (21) Mexican Chicken Chili Soup      ║ (30) Traditional Spaghetti Bolognese ║"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ║ (11) Chinese Fried Rice              ║ (22) Quick + Easy Pot Roast          ║ (31) Whole Wheat Pancakes            ║"#, r#"    ║                                      ║                                      ║                                      ║"#, r#"    ╚══════════════════════════════════════╩══════════════════════════════════════╩══════════════════════════════════════╝"#, r#""#, r#"                ENTER ANY NUMBER (1) TO (31) FROM THE ABOVE MENU. (E) TO EXIT OR (B) BACK TO MAIN MENU"#];

        for y in x.iter() {
            println!("  {}", y);
        }
    }

let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "1" => super::any_day_is_a_picnic_chicken_salad::file(),
        "2" => super::anzac_slice::file(),
        "3" => super::bean_and_rice_burritos::file(),
        "4" => super::beef_and_broccoli_stir_fry::file(),
        "5" => super::belgian_waffles::file(),
        "6" => super::black_bean_and_chicken_soup::file(),
        "7" => super::brownies::file(),
        "8" => super::buttermilk_waffles::file(),
        "9" => super::chicken_noodle_soup::file(),
        "10" => super::chicken_tortilla_soup_ii::file(),
        "11" => super::chinese_fried_rice::file(),
        "12" => super::comforting_beef_enchilada_soup::file(),
        "13" => super::grilled_pizza::file(),
        "14" => super::irish_pub_beef_stew::file(),
        "15" => super::italian_cannelloni::file(),
        "16" => super::lasagna::file(),
        "17" => super::otellos_penne_bolognese::file(),
        "18" => super::otellos_beans_and_noodles::file(),
        "19" => super::the_best_chili_ever::file(),
        "20" => super::stuffed_green_peppers::file(),
        "21" => super::mexican_chicken_chili_soup::file(),
        "22" => super::quick_easy_pot_roast::file(),
        "23" => super::grilled_chicken_and_avocado_quinoa_pilaf::file(),
        "24" => super::fruit_salad_with_crystallized_ginger::file(),
        "25" => super::pizza::file(),
        "26" => super::lentil_veggie_soup_crock_pot::file(),
        "27" => super::spaghetti_sauce::file(),
        "28" => super::slow_cooker_chicken_noodle_soup::file(),
        "29" => super::scott_hibb_whiskey_grilled_baby_back_ribs::file(),
        "30" => super::traditional_spaghetti_bolognese::file(),
        "31" => super::whole_wheat_pancakes::file(),
        "b" => crate::main_menu(),
        "e" => crate::exit(),
        _ => recipes_menu(),
    }
}


pub fn recipes_2(){

    let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read line");
    
    match input.trim() {
        "b" => recipes_menu(),
        "m" => crate::main_menu(),
        "e" => crate::exit(),
        "r" => crate::random(),
        _ => recipes_2(),
    }
}