    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Slow Cooker Chicken Noodle Soup"#, r#""#, r#""#, r#""#, r#""#, r#"This is a classic chicken noodle soup recipe from a recent issue of Good Housekeeping that I'd like"#, r#"to try. Update: I have made this soup and really like it. I did use 3-4 skinless chicken breasts in"#, r#"place of the whole chicken, and I substituted 8 cups of canned chicken broth for the water. If you do"#, r#"this, you might want to add a little less salt unless you use a low-sodium chicken broth. I'm not sure"#, r#"why some reviewers say that their egg noodles dissolve, unless they are using the very fine egg"#, r#"noodles. I use a wide noodle and they are just fine after 20 minutes in the Crock-Pot."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"8 cups water or 8 cups canned chicken broth"#, r#"1 cup carrot, cut into 1/4-inch slices"#, r#"1 cup celery, cut into 1/4-inch slices"#, r#"1 cup onion, chopped"#, r#"1 garlic clove, minced"#, r#"2 bay leaves"#, r#"1⁄2 teaspoon dried thyme"#, r#"4 teaspoons salt (to taste)"#, r#"1⁄2 teaspoon fresh ground black pepper, to taste"#, r#"1 (3 1/2 lb) roasting chickens"#, r#"3 cups wide egg noodles, uncooked"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"In 4-1/2 to 6-quart slow-cooker bowl, combine water, carrots, celery, onion, garlic, bay leaves,"#, r#"thyme, 4 t salt, 1/2 t pepper."#, r#""#, r#"Place whole chicken on top of vegetables."#, r#""#, r#"Cover slow cooker with lid and cook as manufacturer directs on low setting 8 to 10 hours or on high"#, r#"4-5 hours."#, r#""#, r#"Transfer chicken to cutting board. Discard bay leaves. Add noodles to slow-cooker; cover with lid"#, r#"and cook (on low or high) 20 minutes."#, r#""#, r#"While noodles cook, remove and discard skin, fat and bones from chicken; shred meat."#, r#""#, r#"Skim fat from soup and discard. Return chicken to soup to serve."#, r#""#, r#""#, r#""#, r#""#, r#"- CookingONTheSide"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
