    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Belgian Waffles"#, r#""#, r#""#, r#""#, r#""#, r#"This belgian waffle recipe is delicious! Top with your favorite fruit."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"2 cups all-purpose flour"#, r#"2 teaspoons baking powder"#, r#"2 tablespoons confectioners' sugar"#, r#"1 tablespoon vegetable oil"#, r#"2 cups milk"#, r#"3 eggs, separated"#, r#"2 teaspoons vanilla"#, r#"1 pinch salt"#, r#"whipped cream"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Combine the flour, baking powder, confectioners sugar, oil, milk and egg yolks."#, r#""#, r#"Beat the egg whites and salt until they stand in soft peaks, mix in the vanilla at this time and fold"#, r#"into the batter (do not over mix)."#, r#""#, r#"Pour 1/8 of the mixture into a hot waffle iron and bake for about 2 minutes."#, r#""#, r#"Repeat with the remaining batter."#, r#""#, r#"Top with your favorite fruit and whipped cream and serve hot."#, r#""#, r#""#, r#""#, r#""#, r#"- Charlotte J"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
