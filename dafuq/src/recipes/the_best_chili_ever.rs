    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"The Best Chili Ever"#, r#""#, r#""#, r#""#, r#""#, r#"This is the best chili recipe I have ever tried. I'm not sure where the"#, r#"recipe originated, but it is amazing! Sometimes, I don't bother adding all"#, r#"four cans of the kidney beans and it still turns out wonderful. Once"#, r#"anyone tastes this chili, they will be begging for the recipe!! Enjoy!"#, r#""#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#""#, r#"     2 teaspoons oil"#, r#""#, r#"     2 onions, chopped"#, r#""#, r#"     3 cloves garlic, minced"#, r#""#, r#"     1 lb lean ground beef"#, r#""#, r#"     3⁄4 lb beef sirloin, cubed"#, r#""#, r#"     1 (14 1/2 ounce) can diced tomatoes"#, r#""#, r#"     1 can dark beer"#, r#""#, r#"     1 cup strong coffee"#, r#""#, r#"     2 (6 ounce) cans tomato paste"#, r#""#, r#"     1 can beef broth"#, r#""#, r#"     1⁄2 cup brown sugar"#, r#""#, r#"     3 1⁄2 tablespoons chili sauce"#, r#""#, r#"     1 tablespoon cumin"#, r#""#, r#"     1 tablespoon cocoa"#, r#""#, r#"     1 teaspoon oregano"#, r#""#, r#"     1 teaspoon cayenne"#, r#""#, r#"     1 teaspoon coriander"#, r#""#, r#"     1 teaspoon salt"#, r#""#, r#"     4 (15 ounce) cans kidney beans"#, r#""#, r#"     4 chili peppers, chopped"#, r#""#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#""#, r#""#, r#"    Heat oil."#, r#""#, r#"    Cook onions, garlic and meat until brown."#, r#""#, r#"    Add tomatoes, beer, coffee, tomato paste and beef broth."#, r#""#, r#"    Add spices Stir in 2 cans of kidney beans and peppers."#, r#""#, r#"    Reduce heat and simmer for 1 1/2 hours."#, r#""#, r#"    Add 2 remaining cans of kidney beans and simmer for another 30 minutes."#, r#""#, r#""#, r#""#, r#""#, r#"- AmandaAOates"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
