    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Buttermilk Waffles"#, r#""#, r#""#, r#""#, r#""#, r#"Good waffle recipes are surprisingly hard to find. Most of them say to use “waffle mix”, or call for"#, r#"sugar to be dumped in. I consider this to be nothing short of blasphemy, so I decided to create my"#, r#"own recipe. It's very light and fluffy."#, r#""#, r#"This recipe makes approximately fourteen 10 cm * 10 cm waffles."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1.5 Cups flour"#, r#"1 Teaspoon baking powder"#, r#"1 Teaspoon baking soda"#, r#"Pinch of salt"#, r#"1.5 Cups buttermilk"#, r#"4 Eggs, separated"#, r#"0.5 Cups oil or applesauce"#, r#"2 Teaspoons vanilla extract"#, r#""#, r#""#, r#""#, r#""#, r#"    Preparation"#, r#""#, r#""#, r#"In a large bowl, mix the flour, baking powder, baking soda, and salt together. Add the buttermilk,"#, r#"egg yolks, oil (or applesauce), and vanilla."#, r#""#, r#"In a separate bowl, beat the egg whites until stiff peaks form."#, r#""#, r#"Beat the first batter mixture until smooth. Some cross contamination from the egg whites is"#, r#"acceptable."#, r#""#, r#"Gently stir the stiff egg whites into the rest of the batter. Do not overmix."#, r#""#, r#""#, r#""#, r#""#, r#"By Spatula Tzar"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
