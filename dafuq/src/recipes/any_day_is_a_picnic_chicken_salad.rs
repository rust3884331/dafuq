
pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Any Day's a Picnic Chicken Salad"#, r#""#, r#""#, r#""#, r#""#, r#""#, r#"Onion and pickle relish spice up a traditional chicken salad."#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#"    2 1/2 cups chicken breast (cooked, diced)"#, r#"    1/2 cup celery (chopped)"#, r#"    1/4 cup onion (chopped)"#, r#"    3 packages pickle relish (2/3 tablespoon)"#, r#"    1/2 cup mayonnaise (light)"#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#"    Combine all ingredients."#, r#"    Refrigerate until ready to serve."#, r#"    Use within 1-2 days. Chicken salad does not freeze well."#, r#""#, r#""#, r#""#, r#"How to use:"#, r#""#, r#"    Make chicken salad sandwiches."#, r#"    Make a pasta salad by mixing with 2 cups cooked pasta."#, r#"    Kids will love this salad served in a tomato or a cucumber boat."#, r#""#, r#""#, r#""#, r#" Source:"#, r#""#, r#"University of Wisconsin Cooperative Extension Service"#, r#"A Family Living Program."#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

            super::menu::recipes_2();
}
