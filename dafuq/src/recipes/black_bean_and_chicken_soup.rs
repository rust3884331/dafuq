    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Black Bean and Chicken Soup"#, r#""#, r#""#, r#""#, r#""#, r#"This is so tasty, and healthy too! We love black beans. Prep time does not include soaking and"#, r#"cooking the beans, but does include cooking the chicken. You can, of course, substitute canned"#, r#"beans, just drain them. The soup can also be frozen in a sturdy plastic container, or a ziploc"#, r#"bag. To serve, defrost and heat in microwave or on stovetop."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1 lb black beans, soaked, cooked, and drained"#, r#""#, r#"1 lb boneless skinless chicken breast, cut in bite size pieces"#, r#""#, r#"1⁄2 teaspoon cumin powder"#, r#""#, r#"1⁄2 teaspoon garlic powder"#, r#""#, r#"1⁄4 teaspoon black pepper"#, r#""#, r#"2 large onions, diced"#, r#""#, r#"2 (14 1/2 ounce) cans diced tomatoes, one can drained only"#, r#""#, r#"2 cups corn"#, r#""#, r#"2 (14 ounce) cans chicken broth (I use 99% fat free)"#, r#""#, r#"2 tablespoons cumin"#, r#""#, r#"2 tablespoons garlic powder"#, r#""#, r#"2 teaspoons black pepper"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Sprinkle chicken pieces with 1/2 tsp cumin, 1/2 tsp garlic powder, and 1/4 tsp black pepper."#, r#""#, r#"Cook on medium high heat in small about of oil, until chicken is cooked through and lightly"#, r#"browned, or about 5-7 minutes."#, r#""#, r#"Combine all ingredients and cooked chicken in crockpot."#, r#""#, r#"Stir well, and taste to make sure spices are how you like them."#, r#""#, r#"Cook on high heat one hour, then low for 3-4 hours, or for however long you need it to cook."#, r#""#, r#"It will be done when you are ready for it."#, r#""#, r#""#, r#""#, r#""#, r#"- Amanda Beth"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
