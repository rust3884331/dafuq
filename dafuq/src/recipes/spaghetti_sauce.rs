    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Spaghetti Sauce"#, r#""#, r#""#, r#""#, r#""#, r#"This is a recipe I have developed over a number of years. This is the only spaghetti sauce my family"#, r#"will request! Very easy to make. I hope your family enjoys it as much as mine does!"#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1 1⁄2 lbs ground beef"#, r#""#, r#"2 tablespoons olive oil"#, r#""#, r#"1 medium onion, chopped"#, r#""#, r#"2 cloves garlic, minced (to taste)"#, r#""#, r#"2 bay leaves"#, r#""#, r#"1 teaspoon oregano"#, r#""#, r#"1 teaspoon dried basil"#, r#""#, r#"1 teaspoon italian seasoning"#, r#""#, r#"1 teaspoon salt (, or to taste)"#, r#""#, r#"ground pepper"#, r#""#, r#"1 (6 ounce) can tomato paste"#, r#""#, r#"2 (16 ounce) cans tomato sauce"#, r#""#, r#"1 (28 ounce) can diced tomatoes"#, r#""#, r#"8 ounces fresh mushrooms, sliced and sauteed in butter (optional)"#, r#""#, r#"parmesan cheese, freshly grated (optional)"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Brown the ground beef, onion and garlic in olive oil with bay leaves, oregano, basil, Italian"#, r#"Seasoning, salt and pepper."#, r#""#, r#"Add tomato paste, tomato sauce and diced tomatoes."#, r#""#, r#"Stir well and bring to a simmer over medium heat."#, r#""#, r#"Cover and simmer for 1 1/2 hours."#, r#""#, r#"Use sauce to top your cooked spaghetti."#, r#""#, r#"Top with sauteed mushroom."#, r#""#, r#"Pass the Parmesan."#, r#""#, r#""#, r#""#, r#""#, r#"- Bev I Am"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
