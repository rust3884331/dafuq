    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Italian Cannelloni"#, r#""#, r#""#, r#""#, r#""#, r#"I've had this recipe for 20 years. It was given to me by my Brother in laws mother (Italian). I have"#, r#"on occassion substitued the ground beef and pork for ground chicken and/or turkey. Everyone in my"#, r#"family drools when they know I'm making it."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"2 tablespoons vegetable oil or 2 tablespoons olive oil"#, r#"1⁄4 cup finely chopped onion"#, r#"1 teaspoon finely chopped garlic (about 2 cloves)"#, r#"1 1⁄2 lbs ground beef"#, r#"1⁄2 lb ground pork"#, r#"1⁄2 cup grated parmesan cheese"#, r#"2 tablespoons cream or 2 tablespoons milk"#, r#"2 eggs, slightly beaten"#, r#"1 teaspoon oregano"#, r#"1 dash pepper"#, r#"200g cannelloni tubes or 200g manicotti, can be used"#, r#"796ml spaghetti sauce or 796ml homemade spaghetti sauce"#, r#"2 tablespoons butter or 2 tablespoons margarine"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"In oil in a large frypan, cook and stir onion, garlic until the onion is tender."#, r#""#, r#"Add beef and pork and continue cooking until beef is brown. Remove from heat and drain excess oil."#, r#""#, r#"Mix in half the parmesan cheese, cream, eggs and seasonings."#, r#""#, r#"When cool enough to handle, stuff uncooked cannelloni noodles with mixture. Spread a thin layer"#, r#"of sauce on the bottom of a 3.5 litre pan. Arrange stuffed cannelloni in a single layer and pour"#, r#"remaining sauce over, being certain to cover all the pasta."#, r#""#, r#"Sprinkle with the remaining parmesan and dot with butter."#, r#""#, r#"Cover with foil and bake at 375 F for 55 -60 minutes or until tender when pierced with a fork."#, r#"(during last 10 minutes shredded mozzeralla can be sprinkled on top and the foil removed if desired.)."#, r#""#, r#""#, r#""#, r#""#, r#"- tigra"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
