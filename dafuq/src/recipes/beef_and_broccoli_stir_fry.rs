    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Beef and Broccoli Stir-Fry"#, r#""#, r#""#, r#""#, r#""#, r#"I clipped this recipe from Taste of Home magazine several years ago and have found it to be the"#, r#"best-tasting easy beef and broccoli stir-fry. It is credited to Ruth Stahl. I often use charcoal chuck"#, r#"steak, which is very tender and lean. I also like that it doesn't call for any unusual ingredients."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"3 tablespoons cornstarch, divided"#, r#""#, r#"1⁄2 cup water, plus"#, r#""#, r#"2 tablespoons water, divided"#, r#""#, r#"1⁄2 teaspoon garlic powder"#, r#""#, r#"1 lb boneless round steak or 1 lb charcoal chuck steak, cut into thin 3-inch strips"#, r#""#, r#"2 tablespoons vegetable oil, divided"#, r#""#, r#"4 cups broccoli florets"#, r#""#, r#"1 small onion, cut into wedges"#, r#""#, r#"1⁄3 cup reduced sodium soy sauce"#, r#""#, r#"2 tablespoons brown sugar"#, r#""#, r#"1 teaspoon ground ginger"#, r#""#, r#"hot cooked rice, for serving"#, r#""#, r#"toasted sesame seeds, for serving (optional)"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"In a bowl, combine 2 tablespoons cornstarch, 2 tablespoons water and garlic powder until smooth."#, r#""#, r#"Add beef and toss."#, r#""#, r#"In a large skillet or wok over medium-high heat, stir-fry beef in 1 tablespoon oil until beef reaches"#, r#"desired doneness; remove and keep warm."#, r#""#, r#"Stir-fry onion in remaining oil for 4-5 minutes until softened. Add the broccoli and cook for 3"#, r#"minutes until the broccoli is tender but still crisp. Return beef to pan."#, r#""#, r#"Combine soy sauce, brown sugar, ginger and remaining 1 tablespoon cornstarch and 1/2 cup water"#, r#"until smooth; add to the pan."#, r#""#, r#"Cook and stir for 2 minutes."#, r#""#, r#"Serve over rice and garnish with toasted sesame seeds (optional)."#, r#""#, r#""#, r#""#, r#""#, r#"- Chris from Kansas"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
