    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Bean and Rice Burritos"#, r#""#, r#""#, r#""#, r#""#, r#"These baked burritos are a great way to use leftover cooked rice. Try them with brown rice for a"#, r#"whole grain boost."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"2 cups rice (cooked)"#, r#"1 onion (small, chopped)"#, r#"2 cups kidney beans (cooked, or one 15 ounce can, drained)"#, r#"8 flour tortillas (10 inch)"#, r#"1/2 cup salsa"#, r#"1/2 cup cheese (shredded)"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Preheat the oven to 300 °F."#, r#""#, r#"Peel the onion, and chop it into small pieces."#, r#""#, r#"Drain the liquid from the cooked (or canned) kidney beans."#, r#""#, r#"Mix the rice, chopped onion, and beans in a bowl."#, r#""#, r#"Put each tortilla on a flat surface."#, r#""#, r#"Put 1/2 cup of the rice and bean mix in the middle of each tortilla."#, r#""#, r#"Fold the sides of the tortilla to hold the rice and beans."#, r#""#, r#"Put each filled tortilla (burrito) in the baking pan."#, r#""#, r#"Bake for 15 minutes."#, r#""#, r#"While the burritos are baking, grate 1/2 cup cheese."#, r#""#, r#"Pour the salsa over the baked burritos. Add cheese."#, r#""#, r#"Serve the burritos warm."#, r#""#, r#""#, r#""#, r#""#, r#"Source:"#, r#""#, r#"Pennsylvania Nutrition Education Network"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
