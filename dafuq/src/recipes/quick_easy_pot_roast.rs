    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Quick + Easy Pot Roast"#, r#""#, r#""#, r#""#, r#""#, r#"Make pot roast fit for an elegant dinner party for a weeknight meal. This healthy dish is ready in"#, r#"about an hour."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"2 1⁄2 lbs beef chuck roast"#, r#"1 teaspoon kosher salt"#, r#"1⁄4 teaspoon fresh ground black pepper"#, r#"1 garlic clove, roughly chopped"#, r#"1 teaspoon Worcestershire sauce"#, r#"2 teaspoons dried thyme"#, r#"1 tablespoon tomato paste"#, r#"1 cup beef broth"#, r#"1 lb small potato"#, r#"3⁄4 lb carrot, peeled and roughly chopped"#, r#"1⁄4 cup chopped fresh parsley"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Set Instant Pot to sauté mode, add beef and sear for 2 to 3 minutes per side."#, r#""#, r#"Season with salt and pepper, then add garlic, Worcestershire sauce, thyme, tomato paste and beef"#, r#"broth."#, r#""#, r#"Top beef with potatoes and carrots."#, r#""#, r#"Cover and lock the lid in place. Place the vent to the sealing position and set to manual on high"#, r#"pressure for 40 minutes. When the cook time is done, allow the pressure to naturally release for 15"#, r#"minutes."#, r#""#, r#"Release remaining pressure and remove the lid. Using a slotted spoon, transfer carrots and potatoes"#, r#"to a serving dish. Transfer pot roast to same dish and garnish with parsley."#, r#""#, r#"If a gravy is desired, set the pot to saute mode and bring cooking liquid to a simmer. Mix a"#, r#"tablespoon of cornstarch with 3 tablespoons of cooking liquid. Add the cornstarch mixture back to"#, r#"the simmering liquid; cook and stir until thickened. Spoon some of the gravy over the pot roast"#, r#"before serving."#, r#""#, r#"Serves: 6; Calories: 342; Total Fat: 8 grams; Saturated Fat: 3 grams; Total Carbohydrate: 19 grams;"#, r#"Sugars: 4 grams; Protein: 45 grams; Sodium: 538 milligrams; Cholesterol: 123 milligrams; Fiber: 4"#, r#"gram."#, r#""#, r#""#, r#""#, r#""#, r#"- DanaAngeloWhite"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
