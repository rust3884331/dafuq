    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Traditional Spaghetti Bolognese"#, r#""#, r#""#, r#""#, r#""#, r#"I got this recipe from an italian friend on a trip to the spanish isle of mallorca quite a few years ago"#, r#"and cannot find another to touch it."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1⁄2 ounce butter"#, r#""#, r#"1⁄2 tablespoon olive oil"#, r#""#, r#"1⁄2 carrot"#, r#""#, r#"1 stalk celery"#, r#""#, r#"1⁄2 onion"#, r#""#, r#"2 ounces streaky bacon"#, r#""#, r#"1⁄2 lb hamburger or 1/2 lb ground beef"#, r#""#, r#"1 (7 ounce) can chopped tomatoes"#, r#""#, r#"1 bay leaves"#, r#""#, r#"1⁄2 to taste salt"#, r#""#, r#"1⁄2 to taste fresh ground black pepper"#, r#""#, r#"1 clove chopped garlic"#, r#""#, r#"2 ounces mushrooms"#, r#""#, r#"1⁄8 pint beef stock"#, r#""#, r#"1⁄2 glass red wine"#, r#""#, r#"1 tablespoon double cream"#, r#""#, r#"3⁄8 tablespoon tomato puree"#, r#""#, r#"1⁄2 to taste thyme"#, r#""#, r#"1⁄2 to taste oregano"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Gently melt the butter & oil in a large pan which can be covered."#, r#""#, r#"Add chopped carrot, onion, celery, bacon & bay leaves, gently cook until golden."#, r#""#, r#"Add minced steak & garlic, season well with salt & pepper, cook until meat is no longer pink."#, r#""#, r#"Add wine,cook until liquid reduces a little, add mushrooms thyme& oregano."#, r#""#, r#"Blend the tomato puree with the beef stock,then add to pan along with the tinned tomatoes, stir well"#, r#"then cover and cook on lowest possible heat for a couple of hours. The secret to this dish is long,"#, r#"slow cooking as to allow the flavors to meld."#, r#""#, r#"As this dish slowly simmers you will need to add more liquid; use either wine or a little water.I find"#, r#"a little wine does best."#, r#""#, r#"After two hours or so remove from heat, remove bay leaves, add 2 tbsp of cream, stir well, serve"#, r#"with hot pasta or spaghetti with fresh baked garlic bread and grated cheese."#, r#""#, r#""#, r#""#, r#""#, r#"- fireball"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
