    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Lasagna"#, r#""#, r#""#, r#""#, r#""#, r#"My family's favorite. This is a very meaty lasagna. We prefer it with cottage cheese, but you can"#, r#"easily substitute ricotta."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1 1⁄2 lbs lean ground beef"#, r#""#, r#"1⁄2 lb Italian sausage or 1/2 lb ground veal"#, r#""#, r#"1 large onion, chopped"#, r#""#, r#"2 -3 garlic cloves, minced"#, r#""#, r#"1 teaspoon salt"#, r#""#, r#"1 teaspoon fresh coarse ground black pepper"#, r#""#, r#"1 tablespoon dried parsley flakes"#, r#""#, r#"1 tablespoon dried oregano"#, r#""#, r#"1 tablespoon dried basil"#, r#""#, r#"2 (14 1/2 ounce) cans whole tomatoes, undrained and chopped (or canned chopped tomatoes)"#, r#""#, r#"12 ounces tomato paste"#, r#""#, r#"24 ounces ricotta cheese"#, r#""#, r#"2 eggs, beaten"#, r#""#, r#"1⁄2 teaspoon pepper"#, r#""#, r#"2 tablespoons parsley"#, r#""#, r#"1⁄2 cup grated parmesan cheese"#, r#""#, r#"1 lb mozzarella cheese, divided"#, r#""#, r#"12 -15 lasagna noodles"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Brown ground meat, onion and garlic."#, r#""#, r#"Add salt, pepper, parsley, oregano, basil, chopped tomatoes with juice, tomato paste; stirring until"#, r#"well mixed."#, r#""#, r#"Cover and simmer 1 hour (or longer,but watch for getting too dry)."#, r#""#, r#"Cook lasagna noodles according to package directions; drain and set aside."#, r#""#, r#"Spray a 13 x 9 baking pan with cooking spray."#, r#""#, r#"Combine riccota cheese, eggs, pepper, 2 tablespoons parsley, Parmesan cheese and 1/2 1lb of"#, r#"mozzarella cheese; In a lasagna pan, layer noodles, meat sauce, and cheese mixture; repeat."#, r#""#, r#"Top off with layer of noodles; sprinkle evenly with remaining mozzarella cheese; make sure to"#, r#"cover noodles completely."#, r#""#, r#"Bake at 375F for 40-60 minutes, or until cheese mixture is thoroughly melted. (I cover w/ foil for"#, r#"about 40 minutes, then uncover for 15-20 minutes."#, r#""#, r#"Let sit for 15-20 minutes before cutting and serving."#, r#""#, r#""#, r#""#, r#""#, r#"- ratherbeswimmin"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
