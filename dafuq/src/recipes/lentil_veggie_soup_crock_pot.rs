    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Lentil-Veggie Soup (Crock Pot)"#, r#""#, r#""#, r#""#, r#""#, r#"This soup is easy to put together with basic food items that most of us have around. I found the"#, r#"recipe in Better Homes and Gardens Biggest Book of Slow Cooker Recipes, and I did some"#, r#"adjustments with the vegetables and herbs used. I was surprised at how good this very simple soup"#, r#"turned out."#, r#""#, r#""#, r#""#, r#""#, r#"    Ingredients"#, r#""#, r#""#, r#"1 cup dry lentils"#, r#"1 1⁄2 cups carrots, chopped"#, r#"1 1⁄2 cups celery, chopped"#, r#"1 1⁄2 cups onions, chopped"#, r#"3 garlic cloves, minced"#, r#"1 teaspoon dried basil"#, r#"1 teaspoon dried oregano"#, r#"1⁄2 teaspoon dried thyme"#, r#"1 tablespoon dried parsley"#, r#"2 bay leaves"#, r#"3 1⁄2 cups vegetable broth (2 cans)"#, r#"1 1⁄2 cups water"#, r#"1 (14 1/2 ounce) can diced tomatoes"#, r#"fresh ground black pepper, to taste"#, r#""#, r#""#, r#""#, r#""#, r#"    Directions"#, r#""#, r#""#, r#"Rinse lentils."#, r#""#, r#"Place all ingredients except the pepper into a 4-6 quart slow cooker."#, r#""#, r#"Cover and cook on low for at least 12 hours, or high for at least 5 hours."#, r#""#, r#"Season with pepper and remove the bay leaves before serving."#, r#""#, r#""#, r#""#, r#""#, r#"- Vino Girl"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
