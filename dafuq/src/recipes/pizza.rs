    
    pub fn file() {
        crate::clear_screen();

        {
            let x = [r#""#, r#"Pizza"#, r#""#, r#""#, r#""#, r#"Make your own pizza topped with green peppers, mushrooms, or other vegetables."#, r#""#, r#""#, r#""#, r#"Ingredients"#, r#""#, r#"    1/4 mini baguette or Italian bread (split lengthwise, or 2 split English muffins)"#, r#""#, r#"    1/2 cup pizza sauce"#, r#""#, r#"    1/2 cup mozzarella or cheddar cheese (part-skim, shredded)"#, r#""#, r#"    1/4 cup green pepper (chopped)"#, r#""#, r#"    1/4 cup mushrooms (fresh or canned, sliced)"#, r#""#, r#"    vegetable toppings (others, as desired, optional)"#, r#""#, r#"    Italian seasoning (optional)"#, r#""#, r#""#, r#""#, r#"Directions"#, r#""#, r#"Toast the bread or English muffin until slightly brown."#, r#""#, r#"Top bread or muffin with pizza sauce, vegetables and low-fat cheese."#, r#""#, r#"Sprinkle with Italian seasonings as desired."#, r#""#, r#"Return bread to toaster oven (or regular oven preheated to 350 °F)."#, r#""#, r#"Heat until cheese melts."#, r#""#, r#""#, r#""#, r#" Source:"#, r#""#, r#"Pumpkin Post and Banana Beat Newsletters"#, r#"University of Massachusetts Extension"#, r#"Nutrition Education Program"#, r#""#, r#""#, r#""#, r#"╔══════════════════════╗"#, r#"║(R) Random            ║"#, r#"║                      ║"#, r#"║(B) Go Back           ║"#, r#"║                      ║"#, r#"║(M) Main Menu         ║"#, r#"║                      ║"#, r#"║(E) Exit              ║"#, r#"╚══════════════════════╝"#];
    
            for y in x.iter() {
                println!("  {}", y);
            }
        }

        super::menu::recipes_2();
    }
