
use std::io;
use std::{thread, time::Duration};

pub fn file_menu(submenu_path: &str, submenu: &str) {

    crate::clear_screen();
    
    println!("

                     __                                      __           __    __ __
    .----.-----.----|__.-----.-----.-----.   .---.-.-----.--|  |   .-----|  |--|__|  |_
    |   _|  -__|  __|  |  _  |  -__|__ --|   |  _  |     |  _  |   |__ --|     |  |   _|
    |__| |_____|____|__|   __|_____|_____|   |___._|__|__|_____|   |_____|__|__|__|____|
                       |__|

                                        {}

", submenu);

    {
        let read_file = super::read_write::read(submenu_path).unwrap();
        let sections: Vec<&str> = read_file.split("[body-V]-title>").collect();

        for (index, section) in sections.iter().enumerate().skip(1) {
            let list: Vec<&str> = section.lines().collect();
            println!("              ({}) {}", index, list[0]);
        }
    }

    println!("
    
    
    ENTER ANY NUMBER FROM THE ABOVE MENU. (E) TO EXIT OR (B) BACK (M) MAIN MENU
    
    ");

    let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();

    if input.trim() == "b" {
        super::menu::print_menu();
    } else if input.trim() == "m" {
        crate::main_menu();
    } else if input.trim() == "e" {
        crate::exit();
    } else {
        match input.trim().parse::<usize>() {
            Ok(input_num) => {
                index(input_num, submenu_path, submenu);
            },
            Err(_) => {
                error(submenu_path, submenu);
            }
        }
    }
}

pub fn index(input_num: usize, submenu_path: &str, submenu: &str) {

    {
        let read_file = super::read_write::read(submenu_path).unwrap();
        let sections: Vec<&str> = read_file.split("[body-V]-title>").collect();

        if let Some(section) = sections.get(input_num) {
            crate::clear_screen();
            println!(""); println!(""); println!("");
            for line in section.lines() {
                println!("    {}", line)
            }
        } else {
            error(submenu_path, submenu);
        }

        println!("


        ╔══════════════════════╗
        ║(B) Go Back           ║
        ║                      ║
        ║(C) Categories Menu   ║
        ║                      ║
        ║(M) Main Menu         ║
        ║                      ║
        ║(E) Exit              ║
        ╚══════════════════════╝");
    }

    let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();

    if input.trim() == "b" {
        file_menu(submenu_path, submenu);
    } else if input.trim() == "c" {
        super::menu::print_menu();
    } else if input.trim() == "m" {
        crate::main_menu();
    } else if input.trim() == "e" {
        crate::exit();
    } else {
        index(input_num, submenu_path, submenu);
    }
}

fn error(submenu_path: &str, submenu: &str) {

    println!("\n    FUCK!");

    let sleep_duration = Duration::from_millis(2000);
        thread::sleep(sleep_duration);

    file_menu(submenu_path, submenu);
}
