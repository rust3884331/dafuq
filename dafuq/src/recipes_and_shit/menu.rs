
use std::io;
use std::{thread, time::Duration};

pub fn print_menu() {
    crate::clear_screen();

    println!("

                     __                                      __           __    __ __
    .----.-----.----|__.-----.-----.-----.   .---.-.-----.--|  |   .-----|  |--|__|  |_
    |   _|  -__|  __|  |  _  |  -__|__ --|   |  _  |     |  _  |   |__ --|     |  |   _|
    |__| |_____|____|__|   __|_____|_____|   |___._|__|__|_____|   |_____|__|__|__|____|
                       |__|


    ");

    {
        let category = super::read_write::read("categories").unwrap();
        let categories_tag: Vec<&str> = category.split("[CATEGORIES]").collect();
        let categories_list: Vec<&str> = categories_tag[1].lines().collect();

        for (index, list) in categories_list.iter().enumerate().skip(1) {
            println!("              ({}) {}", index, list);
        }
    }

    println!("
    
    
    ENTER ANY NUMBER FROM THE ABOVE MENU. (E) EXIT (B) BACK TO MAIN MENU (H) HELP
    
    ");

    let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();

    if input.trim() == "b" {
        crate::main_menu();
    } else if input.trim() == "h" {
        super::information::help();
    } else if input.trim() == "e" {
        crate::exit();
    } else {
        match input.trim().parse::<usize>() {
            Ok(input_num) => {
                index(input_num);
            },
            Err(_) => {
                error();
            },
        }
    }
}

pub fn index(input_num: usize) {

    let category = super::read_write::read("categories").unwrap();
    let categories_tag: Vec<&str> = category.split("[CATEGORIES]").collect();
    let categories_list: Vec<&str> = categories_tag[1].lines().collect();

    if let Some(submenu) = categories_list.get(input_num) {
        let submenu_path = format!("files/{}", submenu);
        super::file::file_menu(&submenu_path, submenu);
    } else {
        error();
    }
}

fn error() {

    println!("\n    FUCK!");
    
    let sleep_duration = Duration::from_millis(2000);
        thread::sleep(sleep_duration);

        super::read_write::write_files().unwrap();
}