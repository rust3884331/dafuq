
use std::path::Path;
use std::fs::File;
use std::io::Read;
use std::fs;
use whoami;

pub fn read(x: &str) -> std::io::Result<String> {

    let user = whoami::username();
        let y = format!("/home/{}/dafuq/{}.txt", user, x);

    let mut file = File::open(y)?;
    let mut read_file = String::new();
    file.read_to_string(&mut read_file)?;
    Ok(read_file)
}

pub fn write_path() -> std::io::Result<()> {

    let user = whoami::username();

    let path1 = format!("/home/{}/dafuq", user);
        fs::create_dir(path1)?;

    let path2 = format!("/home/{}/dafuq/files", user);
        fs::create_dir(path2)?;

    let file_path = format!("/home/{}/dafuq/categories.txt", user);
        fs::write(file_path, r#"
here you can add/remove your own sub-menu categories below.

removeing a category will remove it from dafuq,
but it won't delete the text file from "files"
allowing you to add it back.

[CATEGORIES]    <-- don't remove this tag!
Breakfast
Desserts
Soups
Stews
One-Pot Meals
Bakeing
Pasta and Noodles
Rice and Grains
Grilling and BBQ
Vegetarian
Salads
Healthy and Light
Slow Cooker and Instant Pot
Comfort Foods
Breads"#)?;

write_files().unwrap();

Ok(())
}

pub fn write_files() -> std::io::Result<()> {

    let category = read("categories").unwrap();
    let section: Vec<&str> = category.split("[CATEGORIES]").collect();
    let output: Vec<&str> = section[1].lines().collect();

    for list in output.iter().skip(1) {

        let user = whoami::username();
            let file_path2 = format!("/home/{}/dafuq/files/{}.txt", user, list);

        let path = Path::new(&file_path2);

        if path.exists() {
            ();
        } else {
            fs::write(file_path2, "[body-V]-title>Your Recipe Title Goes Here

    recipe goes here


[body-V]-title>Recipe2

    _____________
    _____________
    _____________
    _____________


    __________________________
    __________________________
    __________________________")?;
        }
    }

    super::menu::print_menu();
    
    Ok(())
}