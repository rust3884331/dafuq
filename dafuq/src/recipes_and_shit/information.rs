
use std::io;
use std::path::Path;

pub fn control() {

    let user = whoami::username();
        let file_path = format!("/home/{}/dafuq", user);

    let path = Path::new(&file_path);

    if path.exists() {
        super::read_write::write_files().unwrap();
    } else {
        crate::clear_screen();
        confirm();
    }
}

pub fn confirm() {

    let x = r#"


==========================================================CATEGORIES===========================================================

        /home/username/dafuq/categories.txt


    You can add or remove categories from the list of menu items.

    to add a category simply type the name of the new category
    you want to add below the "[CATEGORIES]" tag, then reload the
    page, it will automatically appear in the menu and generate a
    text file.

    to remove a category, you delete it from the list.
    This will remove it from the menu options. however, removing
    a category does not delete the associated text file from the
    "files" directory. This means the text file is still in
    the "files" directory and can be re-added back to the
    categories list later if needed.







============================================================FILES==============================================================

      /home/username/dafuq/files


 [body-V]-title>    <--  This tag is used to identify sections of each document in /files, specifically where titles begin
                         and new sections start. It’s a placeholder or marker to help with organizing content.




 How to Use It:

    1.
      Start a New Section:
        - Use `[body-V]-title>` followed by your section title to denote the beginning of a new section.
    2.
      Insert Content:
        - After the title marker, provide the relevant content for that section.
    3.
      Follow the Format:
        - Continue using `[body-V]-title>` for each new section title and format your content accordingly.




 Example:

    [body-V]-title>Your Recipe Title Goes Here

        recipe goes here


    [body-V]-title>Recipe2

      _____________
      _____________
      _____________
      _____________


      __________________________
      __________________________
      __________________________
      
      
      "#;

    println!("

    read files not found. this action will write following to your user home directory:

               dafuq
                 ├── categories.txt
                 └── files
                       ├── Bakeing.txt
                       ├── Breads.txt
                       ├── Breakfast.txt
                       ├── Comfort Foods.txt
                       ├── Desserts.txt
                       ├── Grilling and BBQ.txt
                       ├── Healthy and Light.txt
                       ├── One-Pot Meals.txt
                       ├── Pasta and Noodles.txt
                       ├── Rice and Grains.txt
                       ├── Salads.txt
                       ├── Slow Cooker and Instant Pot.txt
                       ├── Soups.txt
                       ├── Stews.txt
                       └── Vegetarian.txt



                       Do you wish to continue? [y/n]

                       (W) What the fuck is this shit
");

    let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();

    if input.trim() == "y" {
        super::read_write::write_path().unwrap();
    } else if input.trim() == "n" {
        crate::main_menu();
    } else if input.trim() == "w" {
        crate::clear_screen();
        println!("{}", x);
        confirm();
    } else {
        crate::clear_screen();
        confirm();
    }
}

pub fn help() {

    crate::clear_screen();
    
    println!(r#"


==========================================================CATEGORIES===========================================================

        /home/username/dafuq/categories.txt


    You can add or remove categories from the list of menu items.

    to add a category simply type the name of the new category
    you want to add below the "[CATEGORIES]" tag, then reload the
    page, it will automatically appear in the menu and generate a
    text file.

    to remove a category, you delete it from the list.
    This will remove it from the menu options. however, removing
    a category does not delete the associated text file from the
    "files" directory. This means the text file is still in
    the "files" directory and can be re-added back to the
    categories list later if needed.







============================================================FILES==============================================================

      /home/username/dafuq/files


 [body-V]-title>    <--  This tag is used to identify sections of each document in /files, specifically where titles begin
                         and new sections start. It’s a placeholder or marker to help with organizing content.




 How to Use It:

    1.
      Start a New Section:
        - Use `[body-V]-title>` followed by your section title to denote the beginning of a new section.
    2.
      Insert Content:
        - After the title marker, provide the relevant content for that section.
    3.
      Follow the Format:
        - Continue using `[body-V]-title>` for each new section title and format your content accordingly.




 Example:

    [body-V]-title>Your Recipe Title Goes Here

        recipe goes here


    [body-V]-title>Recipe2

      _____________
      _____________
      _____________
      _____________


      __________________________
      __________________________
      __________________________


    PRESS ENTER TO GO BACK

    
"#);

    let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();

    if input.trim() == "b" {
        super::menu::print_menu();
    } else {
        super::menu::print_menu();
    }
}