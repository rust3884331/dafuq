# Dafuq

dafuq is a mixtape of internet oddities in the form of a TUI program for the terminal. it's a repository of recipes, jokes, and internet oddities including: conspiracy theories, cooking with marijuana, ascii animations, and more. you can create your own categories and store your own recipes. the basic interface with it's retro ascii graphics is simple to use, reminiscent of old computers. only compatible with linux.


**Versions published**

`dafuq` published 24-AUG-2024

`dafuq.rs` published 2023

## Compiling and Install

first install Rust: (if you haven't already)

    https://www.rust-lang.org/tools/install


Download the source code.

extract contents.

open the "dafuq" folder in the terminal.

RUN:

    cargo build --release


to install the Program just move the dafuq binary file to /usr/local/bin/

you can do this by:

    sudo cp target/release/dafuq /usr/local/bin/


## Usage

**Options:**

`dafuq` menu

`dafuq -r` randomly select a recipe from hard coded list.

`dafuq name` add your name as an argument & get an answer. 
> If you’re masochistic enough to run your name through a program that insults you for a laugh, or you are sadistic enough to run someone else’s name through such a program, this might just be the thing for you :)

**navigateing the Program**

to navigate the Program Type the number of the option you want and press **Enter**

**Default files are automatically generated**

when you first open **Personal recipes and shit** it will ask permission to add a new directory with files to your user home directory.

**adding or removing a category**

to add a category: simply type the name of the new category you want in categories.txt below the "[CATEGORIES]" tag
to remove a category: just remove the name from the list in categories.txt. however, removing a category does not delete the associated text file from the "files" directory. This means the text file is still in the "files" directory and can be re-added back to the categories list later if needed.

**how to add your own recipes**

find or create the relevant category.

using: `[body-V]-title>`

follow this format:
```
[body-V]-title>Your Recipe Title Goes Here

recipe goes here


[body-V]-title>Recipe2

_____________
_____________
_____________
_____________


__________________________
__________________________
__________________________
```

## License

GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007

